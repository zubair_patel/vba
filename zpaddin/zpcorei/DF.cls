VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Interface DataFilter


Public Sub SetFields( _
    ByRef Fields As GD)
End Sub

Public Function GetFields() As GD
End Function

Public Property Get AllCriteria() As Dictionary ' map<Long columnIndex, GD columnFilters{Operator|Value}>
End Property

Public Property Get ColumnCriteria( _
    ByVal clmIndex As Long) As GD ' {Operator|Value}
End Property

Public Property Get FieldCriteria( _
    ByVal fieldName As String) As GD ' {Operator|Value}
End Property

Public Sub RemoveColumnCriteria( _
    ByVal clmIndex As Long)
End Sub

Public Sub RemoveFieldCriteria( _
    ByVal fieldName As String)
End Sub

Public Sub ClearAllCriteria()
End Sub

Public Sub AddColumnCriteria( _
    ByVal clmIndex As Long, _
    ByVal op As Operator, _
    ByVal val As Variant)
End Sub

Public Sub AddFieldCriteria( _
    ByVal fieldName As String, _
    ByVal op As Operator, _
    ByVal val As Variant)
End Sub

Public Sub AddStringCriteria( _
    ByVal clause As String, _
    Optional valType As ZPType = ZPVariant)
End Sub

Public Function Equals( _
    ByRef compareWith As DF) As Boolean
End Function

Public Function MatchesCriteria( _
    ByVal sourceValue As Variant, _
    ByVal op As Operator, _
    ByVal filterValue As Variant) As Boolean
End Function
    
Public Sub DebugPrint()
End Sub
