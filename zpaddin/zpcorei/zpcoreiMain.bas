Attribute VB_Name = "zpcoreiMain"
Option Explicit

Public Const ZPLIB_VERSION As String = "1.7"

Private Const SOURCE_ROOT = "D:\dev\zpaddin\v"

Private Const ZPLIB_NAME = "zpcorei"
'

Public Sub ProcessComponents()
    ExportComponents xlbookThis, ZPLIB_NAME
    'zplibi.RemoveComponents xlbookThis
    'zplibi.AddZPReferences xlbookThis
    'zplibi.RemoveZPReferences xlbookThis
End Sub




' MS VBS extensibility ref
Public Sub ExportComponents( _
    ByRef wbk As Workbook, _
    ByVal libname As String, _
    Optional version As String = ZPLIB_VERSION, _
    Optional sourceRoot As String = SOURCE_ROOT)
    
    Dim outDir As String
    If sourceRoot = SOURCE_ROOT Then
        outDir = sourceRoot & version & "\" & libname & "\"
    Else
        outDir = sourceRoot
        If Right(outDir, 1) <> "\" Then outDir = outDir & "\"
    End If

    Dim vbc As VBComponent

    For Each vbc In wbk.VBProject.VBComponents
        Dim ext As String
        Select Case vbc.Type
            Case 1: ext = ".bas"
            Case 2: ext = ".cls"
            Case 100: ext = ".cls"
        End Select

        Dim fname As String: fname = vbc.Name & ext

        If vbc.CodeModule.CountOfLines > 1 Then
            vbc.Export outDir & fname
        End If
    Next
    
End Sub

Public Sub RemoveComponents( _
    ByRef wbk As Workbook)

    Dim vbc As VBComponent

    For Each vbc In wbk.VBProject.VBComponents
        Dim ext As String
        Select Case vbc.Type
            Case 1: ext = ".bas"
            Case 2: ext = ".cls"
            Case 100: ext = ".cls"
        End Select

        Dim fname As String: fname = vbc.Name & ext

        If Left(fname, Len("xlbook")) = "xlbook" Or _
            Left(fname, Len("xlsheet")) = "xlsheet" Then
            
            ' Don't Delete
        Else
            wbk.VBProject.VBComponents.Remove vbc
        End If
    Next
    
End Sub

Public Sub AddZPReferences( _
    ByRef wbk As Workbook, _
    Optional ByVal binRoot As String, _
    Optional version As String = "")

    On Error Resume Next
    
    If version = "" Then
        version = ZPLIB_VERSION
    Else
        version = version
    End If
    
    Application.EnableCancelKey = xlDisabled
    
    If binRoot = "" Then
        binRoot = wbk.Path & "\"
    End If
    
    wbk.VBProject.References.AddFromFile binRoot & "zpcorei-" & version & ".xla"
    wbk.VBProject.References.AddFromFile binRoot & "zpcore-" & version & ".xla"
    wbk.VBProject.References.AddFromFile binRoot & "zploadi-" & version & ".xla"
    wbk.VBProject.References.AddFromFile binRoot & "zpload-" & version & ".xla"
    wbk.VBProject.References.AddFromFile binRoot & "zpwki-" & version & ".xla"
    wbk.VBProject.References.AddFromFile binRoot & "zpwk-" & version & ".xla"
    wbk.VBProject.References.AddFromFile binRoot & "zptooli-" & version & ".xla"
    wbk.VBProject.References.AddFromFile binRoot & "zptool-" & version & ".xla"

End Sub

Public Sub RemoveZPReferences( _
    ByRef wbk As Workbook)

    Dim vbr As VBIDE.Reference


    Debug.Print "IsBroken,Name, BuiltIn, FullPath, GUID, Major, Minor, Type, VBE.Version"

    For Each vbr In wbk.VBProject.References
        If vbr.Type = vbext_rk_Project And _
            vbr.GUID = vbNullString And _
            InStr(1, vbr.Name, "zp") > 0 Then
        
            Debug.Print _
                vbr.IsBroken & ", " & _
                vbr.Name & ", " & _
                vbr.BuiltIn & ", " & _
                vbr.FullPath & ", " & _
                vbr.GUID & ", " & _
                vbr.Major & ", " & _
                vbr.Minor & ", " & _
                vbr.Type & ", " & _
                vbr.VBE.version
                
            wbk.VBProject.References.Remove vbr
        End If
    Next
    
End Sub


Public Function CreateDefaultLogger() As LG
    Set CreateDefaultLogger = New LG
End Function




























