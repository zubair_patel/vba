VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "GD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Option Base 0

' Interface for a grid of data

Public Sub Initialize( _
    ByVal clms As Long, _
    ByVal rows As Long, _
    Optional ByVal containsHeader As Tristate = TristateMixed)
End Sub

Public Sub InitializeWithString( _
    ByVal s As String, _
    Optional ByVal delim As String = "|", _
    Optional ByVal createColumnData As Boolean = False, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1, _
    Optional ByVal containsHeader As Tristate = TristateFalse)
End Sub

Public Sub InitializeWithRange( _
    ByRef rge As Range, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1, _
    Optional ByVal containsHeader As Tristate = TristateFalse)
End Sub

Public Sub InitializeWithDictionary( _
    ByRef dict As Dictionary, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1)
End Sub

Public Sub InitializeWithADORS( _
    ByRef rs As ADODB.Recordset, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1)
End Sub

Public Function Clone() As GD
End Function

Public Sub SetStringToDateConvertor( _
    ByVal clm As Long, _
    ByRef conv As Str2Dt)
End Sub

Public Property Let FieldType( _
    ByVal fieldName As String, _
    ByVal clmType As ZPType)
End Property

Public Property Get FieldType( _
    ByVal fieldName As String) As ZPType
End Property

Public Property Let ColumnType( _
    ByVal clm As Long, _
    ByVal clmType As ZPType)
End Property

Public Property Get ColumnType( _
    ByVal clm As Long) As ZPType
End Property

Public Property Let HasHeader( _
    ByVal containsHeader As Tristate)
End Property

Public Property Get HasHeader() As Tristate
End Property

Public Function SortedGrid( _
    ByVal clm As Long) As GD
End Function

' Make this the default property [Attribute Value.VB_UserMemId = 0]
Public Property Let Value( _
    ByVal clm As Long, _
    ByVal row As Long, _
    datum As Variant)
Attribute Value.VB_UserMemId = 0
End Property

' Make this the default property [Attribute Value.VB_UserMemId = 0]
Public Property Get Value( _
    ByVal clm As Long, _
    ByVal row As Long) As Variant
Attribute Value.VB_UserMemId = 0
End Property

Public Sub InsertExpand( _
    ByVal clm As Long, _
    ByVal row As Long, _
    datum As Variant)
End Sub

Public Property Let FieldValue( _
    ByVal heading As String, _
    ByVal row As Long, _
    datum As Variant)
End Property

Public Property Get FieldValue( _
    ByVal heading As String, _
    ByVal row As Long) As Variant
End Property

Public Function FindFieldValue( _
    ByVal what As Variant, _
    ByVal fieldName As String, _
    Optional ByRef foundClm As Long, _
    Optional ByRef foundRow As Long) As Boolean
End Function

Public Function FindValue( _
    ByVal what As Variant, _
    Optional ByRef foundClm As Long, _
    Optional ByRef foundRow As Long, _
    Optional ByVal clmLookup As Long = -1, _
    Optional ByVal rowLookup As Long = -1) As Boolean
    
    ' Also populates foundClm and foundRow
End Function

Public Property Get ColumnIndex( _
    ByVal heading As String) As Long
    ' ret 0 if not found
End Property

Public Property Get RowAsString( _
    ByVal row As Long, _
    Optional ByVal delim As String = "|")
End Property

Public Property Get ColumnAsString( _
    ByVal clm As Long, _
    Optional ByVal delim As String = "|", _
    Optional ByVal skipHeader As Boolean = True)
End Property

Public Sub AddFilterCriteria( _
    ByVal heading As String, _
    ByVal op As Operator, _
    ByVal val As Variant, _
    ByRef inoutFilter As DF)
End Sub

Public Property Get Filtered( _
    ByRef dataFilter As DF) As GD
End Property

Public Property Get Transposed() As GD
End Property

Public Property Get Tail( _
    ByVal rows As Long) As GD
End Property

Public Property Get Subset( _
    Optional ByVal startColumn As Long = 1, _
    Optional ByVal startRow As Long = 1, _
    Optional ByVal Columns As Long = -1, _
    Optional ByVal rows As Long = -1) As GD
End Property

Public Function Equal( _
    ByRef compareWith As GD) As Boolean
End Function

Public Function VLookup( _
    ByVal key As String, _
    ByVal valueColumn As Long, _
    ByRef foundValue As Variant, _
    Optional ByVal keyColumn As Long = 1, _
    Optional ByVal caseSensitive As Boolean = False) As Boolean
    
    ' Finds the first occurrence of key in the keyColumn
    ' And populates foundValue with it
    ' Returns true if value is found
    ' Leaves passed-in foundValue unchanged if not found
    
End Function

Public Function Lookup( _
    ByVal key As String, _
    ByVal keyField As String, _
    ByVal valueField As String, _
    ByRef foundValue As Variant, _
    Optional ByVal caseSensitive As Boolean = False) As Boolean
    
    ' Finds the first occurrence of key in the keyColumn
    ' And populates foundValue with it
    ' Returns true if value is found
    ' Leaves passed-in foundValue unchanged if not found
    
End Function

Public Sub UpdateColumn( _
    ByRef clmData As GD, _
    ByVal clmNbr As Long)
End Sub

Public Sub UpdateRow( _
    ByRef rowData As GD, _
    ByVal rowNbr As Long)
End Sub

Public Sub InsertColumn( _
    ByRef clmData As GD, _
    ByVal clmNbr As Long)
End Sub

Public Sub InsertRow( _
    ByRef rowData As GD, _
    ByVal rowNbr As Long)
End Sub

Public Sub SwapColumns( _
    ByVal clm1 As Long, _
    ByVal clm2 As Long)
End Sub

Public Sub SwapRows( _
    ByVal row1 As Long, _
    ByVal row2 As Long)
End Sub

Public Sub Expand( _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1)

    ' Preserves data

End Sub

Public Sub ExpandRows( _
    ByVal rows As Long)
    ' Preserves data
End Sub

Public Property Get NbrOfDimensions() As Long
End Property

Public Sub Append( _
    ByRef toAppend As GD)
    ' Preserves data.  Will not expand columns
End Sub

Public Sub AppendStringRow( _
    ByVal rowStr As String, _
    Optional ByVal delim As String = "|")
    ' Preserves data.  Will not expand columns
End Sub

Public Property Get StartIndex() As Long
End Property

Public Property Get HasData() As Boolean
End Property

Public Property Get EntireRow( _
    ByVal row As Long) As GD
End Property

Public Property Get HeaderRow() As GD
End Property

Public Property Get EntireColumn( _
    ByVal clm As Long) As GD
End Property

Public Property Get UniqueColumn( _
    ByVal clm As Long) As GD
End Property

Public Function SortedColumn( _
    ByVal clm As Long) As GD
End Function

Public Property Get NbrOfColumns() As Long
End Property

Public Property Get NbrOfRows() As Long
End Property

Public Function GetDictionary( _
    ByVal keyColumn As Long, _
    ByVal valueColumn As Long) As Dictionary
End Function

' Diagnostic

Public Sub DebugPrint()
End Sub













