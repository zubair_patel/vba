VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DSD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Interface for the DataService Definition

Public Property Get Keys() As GD
End Property

Public Property Get Fields() As GD
End Property

Public Property Get Table() As String
End Property

Public Property Get DefaultOrderBy() As String
End Property

