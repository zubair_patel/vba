VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "OBSVBL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Interface

Public Property Get Identifier() As String
End Property

Public Sub Attach( _
    ByRef obsrvr As OBSVR)
End Sub

Public Sub Detach( _
    ByRef obsrvr As OBSVR)
End Sub

Public Sub Notify()
End Sub

Public Property Get State() As Object
End Property

