VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CoreFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function CreateType( _
    Optional ByRef str2DtConv As Str2Dt = Nothing) As VTYP
End Function


' UI

Public Function CreateUI() As UI
End Function
    
    
' GridData

Public Function CreateGD( _
    Optional HeaderRow As String = "", _
    Optional delim As String = "|", _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1) As GD
End Function

Public Function CreateArithmeticOp( _
    ByVal op As String) As BinaryOp
End Function

Public Function CreateGridBinaryProcess( _
    ) As GDBinaryProcess
End Function

' Data Filter

Public Function CreateDataFilter( _
    Optional Fields As GD = Nothing) As DF
End Function


' Log


Public Function CreateLogger( _
    infoSheet As String, _
    Optional ByRef warningSheet As String = "", _
    Optional ByRef errorSheet As String = "", _
    Optional ByRef debugSheet As String = "", _
    Optional ByRef logFilePath As String = "", _
    Optional showErrorsOnce As Boolean = False, _
    Optional showWarningsOnce As Boolean = False) As LG
End Function


' Cursor

Public Function CreateCursor( _
    ByVal sheetName As String, _
    Optional ByVal topLeft As String, _
    Optional ByRef wbk As Workbook = Nothing) As CSR
End Function


' Exception

Public Function CreateExceptionObject() As EXCPT
End Function



' Observable

Public Function CreateObservableImpl( _
    parentId As String, _
    parent As OBSVBL) As OBSVBL
End Function


' Config

Public Function CreateConfig( _
    Optional ByVal configSheet As String = "Config", _
    Optional ByVal topLeft As String = "T_Config") As CFG
End Function

Public Function CreateConfigGD(ByRef dat As GD) As CFG
End Function


