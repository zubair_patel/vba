VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ToolFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Shell

Public Function CreateShell( _
    executablePath As String, _
    arguments As String) As RSH
End Function


' Web Downloader

Public Function CreateWebDownload() As WDL
End Function


' ProcesssInterrupt

Public Function CreateProcessInterrupt( _
    ByVal batchSz As Long, _
    ByVal pauseSeconds As Long) As ProcessInterrupt
End Function


' ProcessKill

Public Function CreateProcessKiller( _
    ByVal windowCaption As String) As ProcessKill
End Function

' Email

Public Function CreateEmail( _
    ByRef attachments As GD) As Email
End Function


' Zip / Unzip tool
Public Function CreateZipper( _
    ByVal pathToExe As String) As Zip
End Function





