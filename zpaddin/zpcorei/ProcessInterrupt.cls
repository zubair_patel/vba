VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ProcessInterrupt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Sub Reset()
End Sub

Public Sub StopRun()
End Sub

Public Property Get BatchSize() As Long
End Property

Public Function ShouldProcessStop() As Boolean
End Function



