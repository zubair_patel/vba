VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CFG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Interface for Config

Public Property Get IsProd() As Boolean
End Property

Public Sub AddDefaultItem( _
    ByVal section As String, _
    ByVal Field As String, _
    ByVal val As Variant)
End Sub

Public Property Get FieldExists( _
    ByVal section As String, _
    ByVal Field As String) As Boolean
End Property

Public Property Get Value( _
    ByVal section As String, _
    ByVal Field As String) As String
End Property

Public Property Get DatedValue( _
    ByVal section As String, _
    ByVal Field As String, _
    ByVal dt As Date) As String
End Property

Public Property Get data() As GD  ' {Section|Field|Detail|Description|DisplayOrder}
End Property

Public Function ExpandVariable( _
    ByVal origStr As String, _
    ByVal var As String, _
    ByVal val As String) As String
End Function

