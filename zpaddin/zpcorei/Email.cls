VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Email"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Interface for generating emails

Public Sub SendEmail( _
    ByVal toList As String, _
    ByVal subjectText As String, _
    ByVal messageBody As String, _
    Optional autoSend As Boolean = False, _
    Optional ccList As String = "", _
    Optional bccList As String = "", _
    Optional bodyIsHtml As Boolean = False)
End Sub

Public Sub AttachFile(ByVal filePath As String)
End Sub

