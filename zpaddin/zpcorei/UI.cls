VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "UI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Property Get ColumnsInRow( _
    ByRef crsr As CSR) As Long
End Property

Public Property Get RowsInColumn( _
    ByRef crsr As CSR) As Long
End Property

Public Property Get BlockRange( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1 _
    ) As Range
End Property

Public Property Get MaxColumnsInStaggeredBlock( _
    ByRef crsr As CSR) As Long
End Property

Public Property Get MaxRowsInStaggeredBlock( _
    ByRef crsr As CSR) As Long
End Property

Public Property Get ColumnOffset( _
    ByRef crsr As CSR, _
    ByVal heading As String) As Long
End Property

Public Function LoadGrid( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1) As GD
End Function

Public Function LoadTable( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1, _
    Optional ByVal fieldName As String, _
    Optional ByVal op As Operator, _
    Optional ByVal val As Variant) As GD
End Function

Public Sub OutputGrid( _
    ByRef crsr As CSR, _
    ByRef dat As GD, _
    Optional ByVal gridStartColumn As Long = 1, _
    Optional ByVal gridStartRow As Long = 1, _
    Optional ByVal gridColumns As Long = -1, _
    Optional ByVal gridRows As Long = -1 _
    )
End Sub

Public Sub Clear( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1 _
    )
End Sub

Public Sub InsertRowsInBlock( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = 1, _
    Optional ByVal rows As Long = -1)
End Sub

Public Sub DeleteRowsFromBlock( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = 1, _
    Optional ByVal rows As Long = -1)
End Sub

Public Sub DeleteBlock( _
    ByRef crsr As CSR)
End Sub
    
Public Sub SetBlockName( _
    crsr As CSR, _
    ByVal RangeName As String, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1)
End Sub

Public Sub SetDataForAutocomplete( _
    ByRef crsr As CSR, _
    ByRef gdata As GD)
End Sub

Public Function Overlaps( _
    ByRef crsr As CSR, _
    ByRef cellSelected As Range, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1) As Boolean
End Function

Public Function BlockAsHtml( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1) As String
End Function

Public Property Get ActiveCursor() As CSR
End Property

Public Property Get RangeNames( _
    ByRef crsr As CSR) As GD
End Property

' Table UI

Public Property Get TableTopLeft( _
    ByRef crsr As CSR) As String
End Property

Public Property Get FilteredRecords( _
    ByRef crsr As CSR, _
    Optional ByVal topLeftAddr As String = "") As GD
End Property

Public Property Get FilteredRecordsRange( _
    ByRef crsr As CSR, _
    Optional ByVal topLeftAddr As String = "") As Range
End Property

Public Property Get TableColumnData( _
    ByRef crsr As CSR, _
    Optional ByVal topLeftAddr As String = "") As GD
End Property

Public Property Get TableColumnDataRange( _
    ByRef crsr As CSR, _
    Optional ByVal topLeftAddr As String = "") As Range
End Property





























