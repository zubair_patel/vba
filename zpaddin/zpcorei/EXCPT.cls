VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "EXCPT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Interface for Exceptions

Public Enum ExceptionCode

    ' VB Errors
    FileNotFound = 53
    FileAlreadyExists = 58
    FilePermissionDenied = 70
    PathNotFound = 76
    NoRecordsFound = 3021
    ObjectNotSet = 91
    
    AccessOdbcTableDoesntExist = -2147217865
    AccessOdbcConstraintDoesntExist = -2147467259
    AccessOdbcFieldDoesntExist = -2147217900
    AccessOdbcUniqueKeyIndexViolation = -2147217900
    AccessOdbcReferentialIntegrityViolation = -2147217913

    ' User defined library errors
    GeneralSystem = 10000
    Argument = 10100
    ArgumentOutOfRange = 10200
    ArrayTypeMismatch = 10300
    FieldAccess = 10400
    IndexOutOfRange = 10500
    InvalidOperation = 10600
    FailedOperation = 10650
    MissingField = 10700
    MissingValue = 10750
    NotImplemented = 10800
    NotSupported = 10900
    NullReference = 11000
    ObjectNotInitialised = 11100
    InvalidObject = 11200
    InvalidResult = 11300
    
    ' Application Errors
    Business = 20000
End Enum
    
Public Sub HandleException( _
    Optional caughtLocation As String = "", _
    Optional showmsg As Boolean = True)
    
    Dim msg As String
    msg = Err.source & "() - " & Err.Description
    
    If showmsg Then
        MsgBox msg
    End If
    
    Debug.Print msg & " caught in " & caughtLocation & "()"
End Sub

Public Property Get ExceptionName(ExceptionCode As Long) As String
    Err.Raise NotImplemented, "EXCPT.ExceptionName"
End Property

Public Sub RecordException()
    Err.Raise NotImplemented, "EXCPT.RecordException"
End Sub

Public Sub ClearRecorded()
    Err.Raise NotImplemented, "EXCPT.ClearRecorded"
End Sub

Public Sub RethrowRecorded()
    Err.Raise NotImplemented, "EXCPT.RethrowRecorded"
End Sub


