VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WkDisp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Interface to Display Data

Public Sub Display() ' Display Data on Sheet
End Sub

Public Sub DisplayNext()
End Sub

Public Sub DisplayPrevious()
End Sub

Public Sub DisplayLast()
End Sub

'Public Property Set Filter(ByRef df As DataFilter)
'End Property
'
'Public Property Get Filter() As DataFilter
'End Property

