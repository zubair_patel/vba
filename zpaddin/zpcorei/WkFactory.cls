VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WkFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' WkDisp

Public Function CreateWkDispGD( _
    ByVal sheetName As String, _
    ByVal topLeft As String, _
    ByRef dat As GD) As WkDisp
End Function

Public Function CreateWkDisp( _
    ByVal sheetName As String, _
    ByRef data As Dictionary) As WkDisp
End Function


' WkProtect

Public Function CreateWkProtect( _
    ByRef crsr As CSR) As WkProtect
End Function

' WkClr

Public Function CreateWkClrDeleteRows( _
    ByVal sheetName As String, _
    Optional ByVal startRow As Long = 1, _
    Optional ByVal clearButDontDeleteFirstRow As Boolean = False) As WkClr
End Function

Public Function CreateWkClrBlocks( _
    ByVal sheetName As String, _
    ByRef topLeftNames As GD, _
    Optional ByVal deleteRows As Boolean = True) As WkClr
End Function

' WkExport

Public Function CreateWkExportValues() As WkExport
End Function

Public Function CreateWkExportPivot() As WkExport
End Function

' WkSheet

Public Function CreateWkSheet( _
    ByRef wks As Worksheet, _
    Optional ByRef clr As WkClr, _
    Optional ByRef disp As WkDisp, _
    Optional ByRef dataservices As Dictionary = Nothing) As WkSheet
End Function


