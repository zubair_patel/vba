VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "LG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum LogType
    LG_INFO
    LG_WARNING
    LG_ERROR
    LG_DEBUG
End Enum


' Interface LG

Public Sub CloseLog()
End Sub

Public Sub AddToDebugList( _
    ByVal source As String)
End Sub

Public Sub PauseDebug()
End Sub

Public Sub ResumeDebug()
End Sub

Public Sub ForceDebug()
End Sub

Public Sub ResetDebug()
End Sub

Public Property Get Console(lgType As LogType) As Worksheet
    Err.Raise ExceptionCode.NotSupported, "LG", _
        "Only basic logging is supported with the base LG class"
End Property


Public Sub Clear()
End Sub

' TODO - test

Public Sub log(ByVal s As String)
    Debug.Print s
End Sub

Public Sub logI(ByVal s As String)
    log "INFO: " & s
End Sub

Public Sub logW(ByVal s As String)
    log "WARNING: " & s
End Sub

Public Sub logE(ByVal s As String)
    log "ERROR: " & s
End Sub

Public Sub logProgress() ' Used for showing progress
End Sub

Public Sub logD(s As String, source As String)
    log "DEBUG: " & s
End Sub









