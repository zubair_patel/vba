VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "LoadFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' DB Schema

Public Function CreateDBSchema( _
    ByRef db As DBH) As DbSchema
End Function

Public Function CreateDBAccess( _
    ByVal connString As String, _
    Optional ByVal userId As String = "", _
    Optional ByVal password As String = "") As DbAccess
End Function

Public Function CreateDBHelper( _
    ByVal dbPath As String, _
    Optional ByVal userId As String = "", _
    Optional ByVal password As String = "", _
    Optional ByRef auditlog As LG = Nothing, _
    Optional ByVal dbType As String = "") As DBH
End Function

' DB Clause

Public Function CreateInsertClause( _
    ByRef Fields As GD, _
    ByVal tableName As String) As DBClause
End Function

Public Function CreateSelectClause( _
    ByRef Fields As GD, _
    Optional ByVal count As Long = -1) As DBClause
End Function

Public Function CreateUpdateClause( _
    ByRef Fields As GD, _
    ByRef FieldTypes As GD, _
    ByRef values As GD, _
    ByVal tableName As String, _
    ByRef dbHelper As DBH) As DBClause
End Function

Public Function CreateValuesClause( _
    ByRef FieldTypes As GD, _
    ByRef fieldValues As GD, _
    ByRef dbHelper As DBH) As DBClause
End Function

Public Function CreateWhereClause( _
    ByRef Fields As GD, _
    ByRef filter As DF, _
    ByVal tblName As String, _
    ByRef db As DBH) As DBClause
End Function


' DB Connection Strings

Public Function CreateCSVConnStr( _
    ByVal filePath As String) As DBConnStr
End Function

Public Function CreateXLSConnStr( _
    ByVal filePath As String) As DBConnStr
End Function

Public Function CreateAccessConnStr( _
    ByVal filePath As String) As DBConnStr
End Function

Public Function CreateODBCConnStr( _
    ByVal dsnName As String, _
    Optional ByVal userId As String = "", _
    Optional ByVal password As String = "") As DBConnStr
End Function


' Data Source and Definition

Public Function CreateDataService( _
    ByVal tableName As String, _
    ByRef db As DBH, _
    ByRef allTablesConfig As GD) As DS
        
    ' allTablesConfig -> {Table|Field|IsKeyField|IsSortField}

End Function

Public Function CreateDataDefinition( _
    ByVal tableName As String, _
    ByRef allTablesConfig As GD) As DSD
    
        
    ' allTablesConfig -> {Table|Field|IsKeyField|IsSortField}

End Function

    
' Query Creator
    
Public Function CreateQuery( _
    ByVal qryName As String, _
    ByRef db As DBH) As Query
End Function


' File Feeds

Public Function CreateFeedActiveRange( _
    ByVal workbookPath As String, _
    ByVal sheetName As String) As Feed
End Function

Public Function CreateFeedImpl( _
    ByVal tableName As String, _
    ByRef dbHelper As DBH, _
    ByRef selectClause As DBClause, _
    ByRef whereClause As DBClause) As Feed
End Function

Public Function CreateFeedTrimData( _
    ByVal workbookPath As String, _
    ByVal sheetName As String) As Feed
End Function




