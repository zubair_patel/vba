VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DbSchema"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Sub DropTable( _
    ByVal tableName As String)
End Sub

Public Sub CreateTable( _
    ByVal tableName As String, _
    ByRef tableDef As GD, _
    Optional ByVal identityField As String = "")
    'tableDef -> {Field|FieldType|FieldSize|IsKeyField|IsRequired}
End Sub

Public Sub AddFieldDefinition( _
    ByVal fldName As String, _
    ByVal fldType As ZPType, _
    Optional ByVal fldSize As Long = 50, _
    Optional ByVal isKey As Boolean = False, _
    Optional ByVal isRqd As Boolean = False, _
    Optional ByRef inOutDefn As GD = Nothing)
    
    'inOutDefn-> {Field|FieldType|FieldSize|IsKeyField|IsRequired}
    
End Sub

Public Sub AddField( _
    ByVal tableName As String, _
    ByVal fieldName As String, _
    ByVal typ As ZPType, _
    Optional ByVal fieldSize As Long = 50, _
    Optional ByVal isRqd As Boolean = False)
End Sub

Public Sub DeleteField( _
    ByVal tableName As String, _
    ByVal fieldName As String)
End Sub
    
Public Sub DropKey( _
    ByVal tableName As String)
End Sub

Public Sub CreateKey( _
    ByVal tableName As String, _
    ByVal keyFlds As GD)
    ' keyflds is single header row
End Sub

Public Sub DropIndex( _
    ByVal tableName As String, _
    ByVal indexName As String)
End Sub

Public Sub CreateIndex( _
    ByVal tableName As String, _
    ByVal indexName As String, _
    ByRef indexFields As GD, _
    ByVal dupsAllowed As Boolean)
    
    ' http://www.w3schools.com/sql/sql_create_index.asp
    
    ' indexFields -> single row of fields
End Sub
    
Public Function GetTableSchema( _
    ByVal tableName As String) As GD
    ' {FieldOrder|Field|IsKeyField|IsSortField|IsNullAllowed}
End Function

Public Function GetDbType( _
    ByVal typ As ZPType, _
    Optional ByVal sz As Long = 50, _
    Optional autoIncr As Boolean = False) As String
End Function

