VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CSR"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Property Get StartAddress() As String
End Property

Public Property Get Ref() As Range
End Property

Public Property Get Sheet() As Worksheet
End Property

Public Property Set Ref(ByRef r As Range)
End Property

Public Property Set Sheet(ByRef wk As Worksheet)
End Property

Public Property Get RefNameOrAddress() As String
End Property

Public Function LocateNameData( _
    Name As String, _
    Optional dx As Long = 0, _
    Optional dy As Long = 0) As Range
End Function

Public Sub Left(Optional dx As Long = 1)
End Sub

Public Sub Right(Optional dx As Long = 1)
End Sub

Public Sub Up(Optional dy As Long = 1)
End Sub

Public Sub Down(Optional dy As Long = 1)
End Sub

Public Sub Move(Optional dx As Long = 0, Optional dy As Long = 0)
End Sub

Public Function GetRelativeRange( _
    Optional dx As Long = 0, _
    Optional dy As Long = 0) As Range
End Function

Public Sub Reset()
End Sub

Public Sub Mark()
End Sub

Public Sub Restore()
End Sub


