VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "GDBinaryProcess"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function ProcessTwoGrids( _
    ByRef fn As BinaryOp, _
    ByRef gd1 As GD, _
    ByRef gd2 As GD, _
    ByRef checkFields As GD, _
    ByRef operateFields As GD) As GD ' {<gd1CheckFields>,<gd1OperatedFields>}
    
    ' checkFieldsMap -> {GD1|GD2}
    ' operateFieldsMap -> {GD1|GD2}
    
    ' Iterates gd1
    ' For Each field in checkFields, validates the data is the same
    ' For Each field in operateFields, operates on the data using the operand
    
End Function
