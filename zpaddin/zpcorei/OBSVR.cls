VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "OBSVR"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' This is assigned by the Observable when you call out to 'Attach'
Public Property Let Identifier( _
    ByVal id As String)
End Property

' This is required to be able to id when 'Detach'ing myself from the observable
Public Property Get Identifier() As String
End Property

Public Sub Update( _
    ByRef subject As OBSVBL)
End Sub
