VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WkProtect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Interface for sheet protection

Public Sub Mark()
End Sub

Public Sub Restore()
End Sub

Public Sub ForceProtect()
End Sub

Public Sub ForceUnProtect()
End Sub
