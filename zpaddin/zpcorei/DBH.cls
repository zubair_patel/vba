VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DBH"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Interface DBHelpr
' Wraps the DBAccess Class
' Facilitates qry exec and adds logging + error handling

Public Sub CloseDb()
End Sub

Public Property Get Conn() As ADODB.Connection
End Property

Public Property Get IsTypeText(adType As Long) As Boolean
End Property

Public Property Get IsTypeDate(adType As Long) As Boolean
End Property

Public Property Get IsTypeNumeric(adType As Long) As Boolean
End Property

Public Property Get FieldTypes(tbl As String, ByRef Fields As GD) As GD
End Property

Public Property Get IsAutoInrement(tbl As String, fieldName As String) As Boolean
End Property

' No need for recordset as no results.
Public Function Run(sql As String, Optional auditSql As Boolean = False)
End Function

' RunGD returns data in a GD object
Public Function RunGD(sql As String, Optional auditSql As Boolean = False) As GD
End Function

' runS returns results as string - useful when we have only one row result
Public Function RunS(sql As String, Optional auditSql As Boolean = False) As String
End Function

Public Function RunSProc(procName As String, ByRef args As GD) As GD
    ' args -> { String argName, ZPType typ, ParameterDirectionEnum inOrOutType }
End Function

