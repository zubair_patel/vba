VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "VTYP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private stringToDateConvertor_ As Str2Dt
'

Public Enum ZPType
    ZPBool
    ZPString
    ZPFloat
    ZPDouble
    ZPInteger
    ZPLong
    ZPDate
    ZPVariant
    ZPObject
End Enum

Public Function TypeToString( _
    ByVal typ As ZPType) As String
End Function

Public Function GetTypedValue( _
    ByVal orig As Variant, _
    ByVal typ As ZPType) As Variant
End Function
