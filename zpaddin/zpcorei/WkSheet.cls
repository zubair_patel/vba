VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WkSheet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Sub Terminate()
End Sub

Public Property Get Sheet() As Worksheet
End Property

Public Property Get AutoRefresh() As Boolean
End Property

Public Property Let AutoRefresh( _
    ByVal b As Boolean)
End Property

Public Sub Clear()
End Sub

Public Sub Refresh()
End Sub

Public Sub RunEvent( _
    ByVal subName As String, _
    Optional ByRef triggerComponent As Object = Nothing, _
    Optional ByVal Target As Range = Nothing, _
    Optional ByRef Cancel As Boolean = False)
End Sub

