VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Interface for the DataServices Layer

Public Property Get Definition() As DSD
End Property

Public Property Set Definition(ByRef def As DSD)
End Property

Public Sub ClearCache()
End Sub

Public Sub Synchronize(ByRef toSynch As GD)
End Sub

Public Sub Save( _
    ByRef toSave As GD, _
    Optional ByVal updateIfInsertFails As Boolean = True)
End Sub

Public Sub Delete(ByRef toDelete As GD)
End Sub

Public Function Load( _
        Optional ByRef filter As DF, _
        Optional ByVal orderBy As String = "", _
        Optional ByVal sortAscending As Boolean = True, _
        Optional ByVal count As Long = -1, _
        Optional ByRef selectFields As GD) As GD    ' Contains Table Header

'     Works like the data filter in Excel so if the same field appears twice, we use an 'Or'
'     Select name, description, val From t_data'
'     Where  name = 'zubair'
'     And val < 5
'     And description like '%Surname%'
'     Or name like '%olly'
    
'       filter Grid:
'       [Field] , [Operator], [Value]
'       filter(1, 1) = "Field":         filter(2, 1) = "Operator":  filter(3, 1) = "Value"
'       filter(1, 2) = "name":          filter(2, 2) = "=":         filter(3, 2) = "zubair"
'       filter(1, 2) = "name":          filter(2, 2) = "=":         filter(3, 2) = "zubair"
'       filter(1, 3) = "val":           filter(2, 3) = "<":         filter(3, 3) = 5
'       filter(1, 4) = "description":   filter(2, 4) = "Like":      filter(3, 4) = "%Surname%"
'       filter(1, 5) = "name":          filter(2, 5) = "Like":      filter(3, 5) = "%olly"
    
End Function



