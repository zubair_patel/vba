VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "VariableSubstituter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Sub Initialize()
End Sub

Public Function Substitute( _
    ByVal fullOriginal As String, _
    ByVal variableToSubsitute As String, _
    ByVal substituteWith As String) As String
    
    ' fullOriginal is the full orig str with no substitutions
    ' variableToSubstitue will be replace, eg '%APPROOT%'.
    ' substituteWith will be the replacement eg 'H:\dev\'
    
    Dim ret As String
    ret = fullOriginal
        
    Dim startPosn As Long: startPosn = 1
    startPosn = InStr(startPosn, ret, variableToSubsitute)
    If startPosn <> 0 Then
        ret = Replace(ret, variableToSubsitute, substituteWith, 1) ' makes all susbstitutions
    End If
    
    Substitute = ret
    
End Function

Public Function SubstituteDate( _
    ByVal val As String, _
    ByVal dt As Date) As String
    
    Dim ret As String
    ret = val
    
    Dim startPosn As Long
    startPosn = InStr(1, ret, "<", vbTextCompare)
    
    While startPosn <> 0
    
        Dim endPosn As Long: endPosn = 0
        endPosn = InStr(startPosn, ret, ">", vbTextCompare)
        
        Dim formatString As String
        formatString = Mid(ret, startPosn + 1, endPosn - startPosn - 1)
        
        Dim toReplace As String
        toReplace = Mid(ret, startPosn, endPosn - startPosn + 1)
        
        Dim replaceWith As String
        replaceWith = Format(dt, formatString)
        
        ret = Replace(ret, toReplace, replaceWith, 1)
        
        startPosn = InStr(1, ret, "<", vbTextCompare)
        
    Wend
    
    SubstituteDate = ret
    
End Function
















