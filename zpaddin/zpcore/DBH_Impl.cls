VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DBH_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Option Base 0

Implements DBH

Private db_ As DbAccess
Private auditLog_ As LG
'

' Wraps the DBAccess Class
' Facilitates qry exec and adds logging + error handling

' Dep on AuditLogSheet
' Tests have dependency on TestUtil, and FileOperation
'

Public Sub Initialize( _
    connString As String, _
    Optional userId As String = "", _
    Optional password As String = "", _
    Optional auditlog As LG = Nothing)
    
    
    DBH_CloseDb
    Set db_ = DataLoadFactory.CreateDBAccess(connString, userId, password)
    
    If auditlog Is Nothing Then
        Set auditLog_ = AppLog
    Else
        Set auditLog_ = auditlog
    End If
    
End Sub

Public Sub Class_Terminate()
    Me.DBH_CloseDb
    
    If Err.Number <> 0 Then
    AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "DBH_Impl.Class_Terminate"
    End If
End Sub

Public Sub DBH_CloseDb()
    If Not db_ Is Nothing Then db_.CloseConnection
End Sub

Public Property Get DBH_Conn() As ADODB.Connection
    Set DBH_Conn = db_.Conn
End Property

Public Property Get DBH_IsTypeText(adType As Long) As Boolean

    ' Lookup:  http://www.carlprothman.net/Default.aspx?tabid=97

    Select Case adType
        Case adChar, adBSTR, adTypeText, adVarChar, _
               adVarWChar, adWChar, adLongVarChar, adLongVarWChar
            DBH_IsTypeText = True
        Case Else
            DBH_IsTypeText = False
    End Select
End Property

Public Property Get DBH_IsTypeDate(adType As Long) As Boolean

    ' Lookup:  http://www.carlprothman.net/Default.aspx?tabid=97

    Select Case adType
        Case adDBDate, adDBTime, adDBTimeStamp
            DBH_IsTypeDate = True
        Case Else
            DBH_IsTypeDate = False
    End Select
End Property

Public Property Get DBH_IsTypeNumeric(adType As Long) As Boolean

    ' Lookup:  http://www.carlprothman.net/Default.aspx?tabid=97

    Select Case adType
        Case adBigInt, adCurrency, adDecimal, adDouble, adInteger, _
            adLongVarBinary, adLongVarChar, adLongVarWChar, adNumeric, _
            adSingle, adSmallInt, adTinyInt, adUnsignedBigInt, _
            adUnsignedInt, adUnsignedSmallInt, adUnsignedTinyInt, _
            adVarBinary, adVarNumeric
            
            DBH_IsTypeNumeric = True
        Case Else
            DBH_IsTypeNumeric = False
    End Select
    
End Property

Public Property Get DBH_FieldTypes(tbl As String, ByRef flds As GD) As GD
    
    ' Expect fields to contain the table fields in row 1
    ' Leaves fields unchanged - returns a new griddata with two rows
    ' One with the fields and one with the types
    
    Dim ret As GD
    
    Dim sql As String: sql = "Select Top 1 * from " & tbl
    Dim rs As New ADODB.Recordset
    On Error Resume Next
    Set rs = DBH_Conn.Execute(sql)
    If Err.Number <> 0 Then
        On Error GoTo 0
        sql = "Select * from " & tbl
        Set rs = DBH_Conn.Execute(sql)
    End If
    On Error GoTo 0
        
    Set ret = CoreFactory.CreateGD
    ret.Initialize flds.NbrOfColumns, 2, TristateTrue
    
    Dim c As Long
    For c = 1 To flds.NbrOfColumns
        Dim found As Boolean: found = False
        Dim fld As ADODB.Field
        For Each fld In rs.Fields
            Dim currentFieldName As String: currentFieldName = flds.Value(c, 1)
            If currentFieldName = fld.Name Or currentFieldName = "[" & fld.Name & "]" Then
                ret(c, 1) = currentFieldName
                ret(c, 2) = fld.Type
                found = True
                Exit For
            End If
        Next
                
        If found = False Then
            rs.Close
            Err.Raise ExceptionCode.MissingField, "DBH_Impl.FieldTypes", _
                " Table='" & tbl & "' Field='" & flds.Value(c, 1) & "'"
        End If
    Next c
    
    rs.Close
    Set DBH_FieldTypes = ret
End Property

Public Property Get DBH_IsAutoInrement(tbl As String, fieldName As String) As Boolean
    
    Dim sql As String: sql = "Select Top 1 * from " & tbl
    Dim rs As New ADODB.Recordset
    On Error Resume Next
    Set rs = DBH_Conn.Execute(sql)
    If Err.Number <> 0 Then
        On Error GoTo ERRHANDLER
        sql = "Select * from " & tbl
        Set rs = DBH_Conn.Execute(sql)
    End If
    On Error GoTo ERRHANDLER
    
    Dim ret As Boolean
    ret = rs.Fields(fieldName).Properties("ISAUTOINCREMENT").Value
    
    rs.Close
    
    DBH_IsAutoInrement = ret
    Exit Property
    
ERRHANDLER:
    AppEx.RecordException
    rs.Close
    AppEx.RethrowRecorded
End Property

' No need for recordset as no results.
Public Function DBH_Run(sql As String, Optional auditSql As Boolean = False)
    On Error GoTo ERRHANDLER
    
    AppLog.LogD "qry='" & sql & "'", "DBH_Impl.Run"
    
    DBH_Conn.Execute sql
    
    On Error GoTo AUDITERRHANDLER
    If auditSql = True Then auditLog_.log sql

EXITSUB:
    Exit Function
    
ERRHANDLER:
    Err.Raise Err.Number, "DBH_Impl.Run", Err.Description
    
    Exit Function
    
AUDITERRHANDLER:
    Err.Raise Err.Number, "DBH_Impl.RunGD", Err.Description
    
End Function


' RunGD returns data in a GD object
Public Function DBH_RunGD(sql As String, Optional auditSql As Boolean = False) As GD

    On Error GoTo QRYERRHANDLER
    
    AppLog.LogD "qry='" & sql & "'", "DBH_Impl.RunGD"
    
    Dim rs As ADODB.Recordset
    Set rs = DBH_Conn.Execute(sql)
    
    On Error GoTo AUDITERRHANDLER
    If auditSql = True Then auditLog_.log sql
    On Error GoTo QRYERRHANDLER
    
    Dim ret As GD
    Set ret = CoreFactory.CreateGD()
    ret.InitializeWithADORS rs
    
    Set DBH_RunGD = ret
    
EXITFUNCTION:
    On Error Resume Next
    If Not (rs Is Nothing) Then rs.Close
    Exit Function
   
QRYERRHANDLER:
    If Not (rs Is Nothing) Then rs.Close

    If Err.Number = ExceptionCode.NoRecordsFound Then
        Set DBH_RunGD = CoreFactory.CreateGD()
    Else
        Err.Raise Err.Number, "DBH_Impl.RunGD", Err.Description
    End If
    Exit Function
    
AUDITERRHANDLER:
    If Not (rs Is Nothing) Then rs.Close
    Err.Raise Err.Number, "DBH_Impl.RunGD", Err.Description
    Exit Function
    
End Function

' RunS returns results as string - useful when we have only one row result
Public Function DBH_RunS(sql As String, Optional auditSql As Boolean = False) As String

    On Error GoTo ERRHANDLER
    
    AppLog.LogD "qry='" & sql & "'", "DBH_Impl.RunS"
    
    Dim rs As ADODB.Recordset
    Set rs = DBH_Conn.Execute(sql)
    
    DBH_RunS = rs.GetString
    
    If Asc(Right(DBH_RunS, 1)) = Asc(vbNewLine) Then
        ' Queried results contains a newline char at end for some reason so we remove it
        DBH_RunS = Left(DBH_RunS, Len(DBH_RunS) - 1)
    End If
    
    On Error GoTo AUDITERRHANDLER
    If auditSql = True Then auditLog_.log sql
    
EXITSUB:
    On Error Resume Next
    rs.Close
    Exit Function
    
ERRHANDLER:
    If Not (rs Is Nothing) Then rs.Close
    
    If Err.Number = ExceptionCode.NoRecordsFound Then
        DBH_RunS = ""
    Else
        Err.Raise Err.Number, "DBH_Impl.RunS", Err.Description
    End If
    Exit Function
    
AUDITERRHANDLER:
    If Not (rs Is Nothing) Then rs.Close
    Err.Raise Err.Number, "DBH_Impl.RunS", Err.Description
    
End Function

Public Function DBH_RunSProc(procName As String, ByRef args As GD) As GD
    
    ' args -> { String ArgName, ZPType Type, ParameterDirectionEnum InOrOutType, ADO_LONGPRT Size, Variant Value }
    
    On Error GoTo QRYERRHANDLER
    
    Dim cmd As New ADODB.Command
    Set cmd.ActiveConnection = Me.DBH_Conn
    cmd.CommandText = procName
    
    Dim r As Long
    For r = 2 To args.NbrOfRows
        Dim argName As String
        Dim argType As ADODB.DataTypeEnum
        Dim inOrOutType As ADODB.ParameterDirectionEnum
        Dim sz As ADO_LONGPTR
        Dim val As Variant
        
        argName = args.FieldValue("ArgName", r)
        argType = getADOType(args.FieldValue("Type", r))
        inOrOutType = args.FieldValue("InOrOutType", r)
        val = args.FieldValue("Value", r)
        
        If args.FieldValue("Type", r) = ZPType.ZPString Then
            sz = args.FieldValue("Size", r)
            If sz <= 0 Then sz = Len(val) ' for memo types
        Else
            sz = 0
        End If
        
        cmd.Parameters.Append cmd.CreateParameter( _
            argName, _
            argType, _
            inOrOutType, _
            sz, _
            val)
    Next r
    
    Dim rs As ADODB.Recordset
    Set rs = cmd.Execute
    
    Dim ret As GD
    Set ret = CoreFactory.CreateGD
    If rs.State = adStateOpen Then
        ret.InitializeWithADORS rs
    Else
        Set rs = Nothing
    End If
    
    Set DBH_RunSProc = ret
    
EXITFUNCTION:
    If Not (rs Is Nothing) Then rs.Close
    Exit Function
    
QRYERRHANDLER:
    If Not (rs Is Nothing) Then rs.Close

    If Err.Number = ExceptionCode.NoRecordsFound Then
        Set DBH_RunSProc = CoreFactory.CreateGD
    Else
        Err.Raise Err.Number, "DBH_Impl.RunSProc", Err.Description
    End If
    
End Function

Private Function getADOType(typ As ZPType) As ADODB.DataTypeEnum
    
    ' INFO: http://msdn.microsoft.com/en-us/library/ms675318(VS.85).aspx
    
    Dim ret As ADODB.DataTypeEnum
    
    Select Case typ
    Case ZPType.ZPDate
        ret = adDBTimeStamp
        'ret = adDate
    Case ZPType.ZPFloat
        ret = adSingle
    Case ZPType.ZPDouble
        ret = adDouble
    Case ZPType.ZPInteger
        ret = adInteger
    Case ZPType.ZPLong
        ret = adBigInt
    Case ZPType.ZPString
        ret = adChar
    Case Else
        Err.Raise ExceptionCode.Argument, _
            "DBH_Impl.getADOType", _
            "Type Variant or Object is invalid"
    End Select
    
    getADOType = ret
    
End Function





















