VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WkDisp_MultipleGDs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements WkDisp

Private crsr_ As CSR
Private protector_ As WkProtect
Private data_ As Dictionary ' map<String topLeft, GD dat>
'


Public Sub Initialize( _
    sheetName As String, _
    ByRef data As Dictionary)
    
    Set crsr_ = CoreFactory.CreateCursor(sheetName)
    Set protector_ = WorksheetFactory.CreateWkProtect(crsr_)
    Set data_ = data
End Sub

Public Sub Class_Terminate()
End Sub


Public Sub WkDisp_DisplayLast()

    Dim v As Variant
    For Each v In data_
    
        Dim topLeft As String
        topLeft = v
        
        Dim dat As GD
        Set dat = data_(topLeft)
        
        crsr_.LocateNameData topLeft, -1, 1
        crsr_.Ref.Value = Max(1, dat.NbrOfRows - maxRows(crsr_))
    Next
    
    WkDisp_Display
    
End Sub

Public Sub WkDisp_DisplayNext()
    
    Dim v As Variant
    For Each v In data_
        
        Dim topLeft As String
        topLeft = v
    
        Dim dat As GD
        Set dat = data_(topLeft)
        
        crsr_.LocateNameData topLeft, -1, 1
        Dim maxR As Long
        maxR = maxRows(crsr_)
        crsr_.Ref.Value = Min(recordStart(topLeft) + maxR, dat.NbrOfRows - maxR)
        
    Next
        
    WkDisp_Display
End Sub

Public Sub WkDisp_DisplayPrevious()

    Dim v As Variant
    For Each v In data_
    
        Dim topLeft As String
        topLeft = v
        
        Dim dat As GD
        Set dat = data_(topLeft)
        
        crsr_.LocateNameData topLeft, -1, 1
        Dim maxR As Long
        maxR = maxRows(crsr_)
        crsr_.Ref.Value = Max(1, Min(1, recordStart(topLeft) - maxRows(crsr_)))

    Next
    
    WkDisp_Display
End Sub

Public Sub WkDisp_Display()

    On Error GoTo ERRHANDLER
    
    ' Remove any autofilters as these can cause problems
    crsr_.Sheet.AutoFilterMode = False
    
    protector_.Mark
    protector_.ForceUnProtect
    
    Dim v As Variant
    For Each v In data_
    
        Dim topLeft As String
        topLeft = v
        
        crsr_.LocateNameData topLeft
        
        Dim outputStartRow As Long
        outputStartRow = recordStart(topLeft)
        
        Dim dat As GD
        Set dat = data_(topLeft)
    
        
        ' Output the table header
        If dat.HasHeader = TristateTrue Then
            AppGui.OutputGrid crsr_, dat, 1, 1, maxColumns(crsr_), 1
            outputStartRow = outputStartRow + 1 ' Add one to handle header row
            crsr_.Down
        End If
        
        ' Output the data.
        AppGui.OutputGrid crsr_, dat, 1, outputStartRow, maxColumns(crsr_), maxRows(crsr_)
        
        ' Fill down the record counts
        Dim recordNum As Long
        recordNum = recordStart(topLeft)
        While crsr_.Ref <> ""
            crsr_.GetRelativeRange(-1) = recordNum
            recordNum = recordNum + 1
            crsr_.Down
        Wend
    
    Next
    
    protector_.Restore
    Exit Sub
    
ERRHANDLER:
    AppEx.HandleException "WkDisp_MultipleGDs.Display"
    On Error Resume Next
    protector_.Restore

End Sub

Private Property Get recordStart(ByVal topLeft As String) As Long
    ' Extract the first record number that we wish to output
    
    crsr_.Mark
    
    crsr_.LocateNameData topLeft, -1, 1
    Dim ret As Long
    ret = Max(CLng(crsr_.Ref.Value2), 1)
    crsr_.Restore
    
    recordStart = ret
    recordStart = 1
    
End Property

Private Property Get maxColumns(ByRef crsr As CSR) As Long
    Dim clmsOnSheet As Long
    clmsOnSheet = crsr.Sheet.Columns.count
    
    Dim ret As Long
    ret = clmsOnSheet - crsr.Ref.Column
    
    maxColumns = ret
End Property

Private Property Get maxRows(ByRef crsr As CSR) As Long
    Dim rowsOnSheet As Long
    rowsOnSheet = crsr.Sheet.rows.count
    
    Dim ret As Long
    ret = rowsOnSheet - crsr.Ref.row
    
    maxRows = ret
End Property




















