VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DBConnStr_Csv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements DBConnStr

Private filePath_ As String
'

Public Sub Class_Terminate()
    If Err.Number <> 0 Then
        AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "DBConnStr_Csv.Class_Terminate"
    End If
End Sub

Public Sub DBConnStr_Initialize(filePath As String)
    filePath_ = filePath
End Sub


Public Property Get DBConnStr_ConnectionString() As String

    Dim fso As New FileSystemObject

    If fso.FileExists(filePath_) = False Then
        Err.Raise ExceptionCode.FileNotFound, _
            "DBConnStr_Csv.ConnectionString", _
            "File='" & filePath_ & "'"
    End If

    Dim connstr As String
    connstr = connstr & "Driver={Microsoft Text Driver (*.txt; *.csv)};"
    connstr = connstr & "DBQ=" & fso.GetParentFolderName(filePath_) & ";"
    connstr = connstr & "ReadOnly=True;"
    connstr = connstr & "Extensions=csv;"
    
    Set fso = Nothing
    
    DBConnStr_ConnectionString = connstr

End Property

