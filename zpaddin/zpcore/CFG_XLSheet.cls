VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CFG_XLSheet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Implements OBSVR
Implements CFG

'Private obsvrId_ As String
Private crsr_ As CSR
Private topLeft_ As String
Private underlying_ As CFG_Impl
'

Public Sub Initialize( _
    ByVal sheetName As String, _
    ByVal topLeft As String)

    topLeft_ = topLeft
    Set crsr_ = CoreFactory.CreateCursor(sheetName, topLeft)

    'obsvrId_ = sheetname
        
    Dim dat As GD
    Set dat = AppGui.LoadGrid(crsr_)
    dat.HasHeader = TristateTrue
    
    Set underlying_ = New CFG_Impl
    underlying_.Initialize dat
    
End Sub

Public Sub Class_Terminate()
End Sub

Public Property Get CFG_IsProd() As Boolean
    CFG_IsProd = underlying_.CFG_IsProd
End Property

Public Sub CFG_AddDefaultItem( _
    ByVal section As String, _
    ByVal Field As String, _
    ByVal val As Variant)

    underlying_.CFG_AddDefaultItem section, Field, val
End Sub

Public Property Get CFG_Data() As GD

    Dim ret As GD
    updateUnderlying
    Set ret = underlying_.CFG_Data
    Set CFG_Data = ret
End Property


Public Property Get CFG_FieldExists( _
    ByVal section As String, _
    ByVal Field As String) As Boolean
    
    updateUnderlying
    CFG_FieldExists = underlying_.CFG_FieldExists(section, Field)

End Property

Public Property Get CFG_Value( _
    ByVal section As String, _
    ByVal Field As String) As String

    Dim ret As String
    updateUnderlying
    ret = underlying_.CFG_Value(section, Field)
    CFG_Value = ret

End Property

Public Property Get CFG_DatedValue( _
    ByVal section As String, _
    ByVal Field As String, _
    ByVal dt As Date) As String

    Dim ret As String
    updateUnderlying
    ret = underlying_.CFG_DatedValue(section, Field, dt)
    CFG_DatedValue = ret
    
End Property

Public Function CFG_ExpandVariable( _
    ByVal origStr As String, _
    ByVal var As String, _
    ByVal val As String) As String
    
    Dim ret As String
    updateUnderlying
    ret = underlying_.CFG_ExpandVariable(origStr, var, val)
    CFG_ExpandVariable = ret
    
End Function

Private Sub updateUnderlying()
    crsr_.LocateNameData topLeft_
    Dim dat As GD
    Set dat = AppGui.LoadGrid(crsr_)
    dat.HasHeader = TristateTrue
    If dat.NbrOfRows > 0 Then
        underlying_.setData dat
    End If
End Sub

'' This is assigned by the OBSVBL when you call out to 'Attach'
'Public Property Let OBSVR_Identifier( _
'    ByVal id As String)
'    obsvrId_ = id
'End Property
'
'' This is required to be able to id when 'Detach'ing myself from the observable
'Public Property Get OBSVR_Identifier() As String
'    OBSVR_Identifier = obsvrId_
'End Property
'
'Public Sub OBSVR_Update( _
'    ByRef subject As OBSVBL)
'
'    Dim updateDat As GD
'    Set updateDat = subject.State
'
'    Dim headerRowStr As String
'    headerRowStr = updateDat.RowAsString(1)
'
'    Select Case headerRowStr
'
'    Case "Section|Field|Detail|Description|DisplayOrder"
'        underlying_.setData updateDat
'    Case "AppName|Section|Field|Detail|Description|DisplayOrder"
'        Set updateDat = updateDat.Subset(2)
'        updateDat.HasHeader = TristateTrue
'        underlying_.setData updateDat
'    End Select
'
'End Sub
'
'
'
'
'
'
