VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Feed_TrimData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Feed

Private sourcePath_ As String
Private sourceSheetName_ As String

Private sourceWorkbook_ As Workbook
'


Public Sub Initialize(workbookPath As String, sheetName As String)
    sourcePath_ = workbookPath
    sourceSheetName_ = sheetName
End Sub

Public Sub Class_Terminate()
    Me.Feed_CloseFeed
End Sub

Public Sub Feed_CloseFeed()
    On Error Resume Next
    If Not (sourceWorkbook_ Is Nothing) Then
        sourceWorkbook_.Close
    End If
    Set sourceWorkbook_ = Nothing
End Sub

Public Function Feed_Load() As GD
        
    Application.ScreenUpdating = False
    Application.DisplayAlerts = False
    
    On Error GoTo ERRHANDLER
    Feed_CloseFeed
    
    Set sourceWorkbook_ = Workbooks.Open( _
        sourcePath_, _
        False, True, , , , , , , , , , False)
    
    Dim WS As Worksheet
    Set WS = sourceWorkbook_.Sheets( _
        sourceSheetName_)
    
    Dim ret As GD
    Set ret = CoreFactory.CreateGD
    ret.InitializeWithRange WS.UsedRange
    
    Dim r As Long
    Dim c As Long
    For r = 1 To ret.NbrOfRows
        For c = 1 To ret.NbrOfColumns
            Dim val As Variant
            val = ret(c, r)
            If IsDate(val) Or IsNull(val) Or IsNumeric(val) Or IsEmpty(val) Or IsObject(val) Then
                ' do nothing
            Else
                ' trim
                ret(c, r) = Trim(val)
            End If
        Next c
    Next r
        
    Set Feed_Load = ret
    
    Exit Function
    
ERRHANDLER:

    AppEx.RecordException
    Feed_CloseFeed
    AppEx.RethrowRecorded

End Function













