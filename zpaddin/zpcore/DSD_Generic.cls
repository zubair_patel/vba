VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DSD_Generic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private tableName_ As String
Private fields_ As GD
Private keys_ As GD
Private sortField_ As String

Implements DSD
'

Public Sub Initialize( _
    ByVal tableName As String, _
    ByRef allTablesConfig As GD)
    
    ' allTablesConfig -> {Table|Field|IsKeyField|IsSortField}
    
    tableName_ = tableName
    
    Dim fltr As DF
    allTablesConfig.AddFilterCriteria "Table", Equal, tableName, fltr
    
    Dim tableConfig As GD
    Set tableConfig = allTablesConfig.Filtered(fltr)
    
    If tableConfig.NbrOfRows <= 1 Then
        Err.Raise ExceptionCode.MissingValue, "DSD_Generic.Initialize", _
            "No table definition found for '" & tableName & "'"
    End If
    
    Dim tableClm As Long
    Dim fieldClm As Long
    Dim isKeyClm As Long
    Dim isSortFieldClm As Long

    tableClm = tableConfig.ColumnIndex("Table")
    fieldClm = tableConfig.ColumnIndex("Field")
    isKeyClm = tableConfig.ColumnIndex("IsKeyField")
    isSortFieldClm = tableConfig.ColumnIndex("IsSortField")

    Set fields_ = tableConfig.Subset(fieldClm, 2, 1).Transposed
    fields_.HasHeader = TristateTrue
    
    
    Set fltr = Nothing
    tableConfig.AddFilterCriteria "IsKeyField", Equal, True, fltr
    
    Dim tmp As GD
    Set tmp = tableConfig.Filtered(fltr)
    
    If tmp.NbrOfRows > 1 Then
        Set keys_ = tmp.Subset(fieldClm, 2, 1).Transposed
        keys_.HasHeader = TristateTrue
    Else
        Set keys_ = CoreFactory.CreateGD
    End If
    
    Set fltr = Nothing
    Set tmp = Nothing
    tableConfig.AddFilterCriteria "IsSortField", Equal, True, fltr
    Set tmp = tableConfig.Filtered(fltr)
    
    If tmp.NbrOfRows > 1 Then
        sortField_ = tmp(fieldClm, 2)
    End If
    
End Sub

Public Sub Class_Terminate()
End Sub

Public Property Get DSD_Keys() As GD
    Set DSD_Keys = keys_
End Property

Public Property Get DSD_Fields() As GD
    Set DSD_Fields = fields_
End Property

Public Property Get DSD_Table() As String
    DSD_Table = tableName_
End Property

Public Property Get DSD_DefaultOrderBy() As String
    DSD_DefaultOrderBy = sortField_
End Property








