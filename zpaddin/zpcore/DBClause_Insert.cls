VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DBClause_Insert"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements DBClause

Private fields_ As GD
Private tableName_ As String
'

Public Sub Initialize( _
    ByRef Fields As GD, _
    tableName As String)

    Set fields_ = Fields
    tableName_ = tableName
End Sub

Public Sub Class_Terminate()
    If Err.Number <> 0 Then
    AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "DBClause_Insert.Class_Terminate"
    End If
End Sub

Public Function DBClause_GenerateString() As String

    ' Eg :  "Insert into t_test([Source], [Title], [Val]) "
    
    Dim sql As String
    
    sql = "Insert into " & tableName_ & "("
    
    Dim c As Long
    
    For c = 1 To fields_.NbrOfColumns
        sql = sql & "[" & fields_(c, 1) & "], "
    Next c
    
    sql = Left(sql, Len(sql) - Len(", "))
    sql = sql & ") "
    
    DBClause_GenerateString = sql
    
End Function



