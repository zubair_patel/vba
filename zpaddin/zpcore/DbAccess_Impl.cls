VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DbAccess_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements DbAccess

Private conn_ As ADODB.Connection
'

Public Sub Initialize( _
    ConnectionString As String, _
    Optional userId As String = "", _
    Optional password As String = "")
    
    DbAccess_CloseConnection
    
    On Error GoTo ERRHANDLER
    Set conn_ = New ADODB.Connection
    
    If userId = "" Then
        conn_.Open ConnectionString
    ElseIf password = "" Then
        ' userId <> ""
        conn_.Open ConnectionString, userId
    Else
        ' userId <> "", password <> ""
        conn_.Open ConnectionString, userId, password
    End If
    
    Exit Sub
    
ERRHANDLER:
    Dim errMsg As String
    errMsg = Err.Description & ", Conn='" & ConnectionString & "'"
    DbAccess_CloseConnection
    
    Err.Raise ExceptionCode.ObjectNotInitialised, "DbAccess.Initialize", errMsg
    
End Sub

Public Sub Class_Terminate()
    Me.DbAccess_CloseConnection
    If Err.Number <> 0 Then
        AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "DbAccess_Impl.Class_Terminate"
    End If
End Sub

Public Property Get DbAccess_conn() As ADODB.Connection
    If conn_ Is Nothing Then
        AppLog.LogW "DB connectivity refused.  Please Ensure you have logged in first"
    End If
    
    Set DbAccess_conn = conn_
End Property

Public Sub DbAccess_CloseConnection()
    On Error Resume Next
    conn_.Close
    Set conn_ = Nothing
End Sub
        

