VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Zip_7za"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Zip

Private exePath_ As String
'

Public Sub Initialize( _
    ByVal pathTo7zaExe As String)
    
    exePath_ = pathTo7zaExe
    
End Sub


Public Sub Zip_Unzip( _
    ByVal zippedFilePath As String, _
    ByVal outputDirPath As String)
    
    Dim runner As New RSH_Wait
    runner.Initialize exePath_, "e -o""" & outputDirPath & """ """ & zippedFilePath & """"
    runner.RSH_Run
    Set runner = Nothing
    
End Sub

Public Sub Zip_Zip( _
    ByVal unzippedFilePath As String, _
    ByVal outputFilePath As String)

    Dim runner As New RSH_Wait
    runner.Initialize exePath_, "a -tzip """ & outputFilePath & """ """ & unzippedFilePath & """"
    runner.RSH_Run
    Set runner = Nothing
    
End Sub

