VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DBClause_Select"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements DBClause

Private fields_ As GD
Private count_ As Long
'

Public Sub Initialize( _
    ByRef Fields As GD, _
    Optional count As Long = -1)
    
    Set fields_ = Fields
    count_ = count
End Sub

Public Sub Class_Terminate()
    If Err.Number <> 0 Then
    AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "DBClause_Select.Class_Terminate"
    End If
End Sub

Public Function DBClause_GenerateString() As String
    Dim ret As String
    
    ret = "Select Distinct "
    
    If count_ > 0 Then
        ret = ret & "Top " & count_ & " "
    End If
    
    If fields_ Is Nothing Then
        ret = ret & "* "
    ElseIf fields_.NbrOfColumns = 0 Then
        ret = ret & "* "
    Else
        Dim c As Long
        For c = 1 To fields_.NbrOfColumns
            ret = ret & "[" & fields_(c, 1) & "], "
        Next c
        
        ' Remove the last (trailing) comma
        ret = Left(ret, Len(ret) - Len(", ")) & " "
    End If
    
    DBClause_GenerateString = ret
End Function

