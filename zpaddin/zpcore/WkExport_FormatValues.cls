VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WkExport_FormatValues"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements WkExport
'

Public Sub Initialize()
End Sub

Public Sub WkExport_Export( _
    ByVal tabName As String, _
    ByVal destinationPath As String, _
    Optional beforeTab As String = "move to end", _
    Optional destinationTabName As String = "")
    
    Dim srcWk As Worksheet
    Dim srcWbk As Workbook
    Dim destWk As Worksheet
    Dim destWbk As Workbook
    
    Set srcWk = Sheets(tabName)
    Set srcWbk = srcWk.parent
    Set destWk = createSheetCopy(srcWk)
    Set destWbk = destinationBook(destinationPath)
    
    If destinationTabName <> "" Then
        destWk.Name = destinationTabName
    Else
        destWk.Name = tabName
    End If
    
    On Error Resume Next
    Application.DisplayAlerts = False
    destWbk.Sheets(destWk.Name).Delete
    Application.DisplayAlerts = True
    On Error GoTo 0
    
    If LCase(beforeTab) = "move to end" Then
        destWk.Move , destWbk.Sheets(destWbk.Sheets.count)
    Else
        destWk.Move destWbk.Sheets(beforeTab)
    End If
    Set destWk = ActiveSheet
    
    srcWbk.Activate
End Sub

Private Function createSheetCopy( _
    ByRef sourceWk As Worksheet) As Worksheet
    
    Dim ret As Worksheet
    sourceWk.Copy sourceWk
    Set ret = ActiveSheet
    
    ret.Unprotect
    ret.Cells.Copy
    ret.Cells.PasteSpecial xlPasteValues
    ret.Cells.Hyperlinks.Delete
    ret.Range("A1").Select
    Application.CutCopyMode = False
    
    ' We have to remove src code before moving to a new blank book
    removeSourceCode ret
    removeObjects ret
    ret.Move
    Set ret = ActiveSheet
        
    sourceWk.parent.Activate
        
    Set createSheetCopy = ret
    
End Function

Private Sub removeSourceCode( _
    ByRef wk As Worksheet)

    On Error Resume Next
    Dim cm As CodeModule
    
    Set cm = wk.parent.VBProject.VBComponents(wk.CodeName).CodeModule
    cm.DeleteLines 1, cm.CountOfLines
    On Error GoTo 0
    
End Sub

Private Sub removeObjects( _
    ByRef wk As Worksheet)
    
    Dim obj As Shape
    
    For Each obj In wk.Shapes
        obj.Delete
    Next
    
End Sub

Private Property Get destinationBook( _
    ByVal wbkPath As String) As Workbook
    
    Dim ret As Workbook
    
    Dim wbkName As String
    Dim fso As New Scripting.FileSystemObject
    wbkName = fso.GetFileName(wbkPath)
    
    On Error Resume Next
    Set ret = Workbooks(wbkName)
    On Error GoTo 0
    
    If ret Is Nothing Then
        If fso.FileExists(wbkPath) Then
            Set ret = Workbooks.Open(wbkPath, False)
        Else
            Set ret = Workbooks.Add
            ret.SaveAs wbkPath
        End If
    End If
    
    Set fso = Nothing
    Set destinationBook = ret
    
End Property




