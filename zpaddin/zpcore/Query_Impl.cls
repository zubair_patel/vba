VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Query_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Query

Private qryName_ As String
Private qryArgs_ As GD
Private db_ As DBH
'

Public Sub Initialize( _
    ByVal qryName As String, _
    ByRef db As DBH)
    
    Set qryArgs_ = CoreFactory.CreateGD
    qryArgs_.InitializeWithString "ArgName|Type|InOrOutType|Size|Value", , False, , , TristateTrue
    
    Set db_ = db
    
    qryName_ = qryName
End Sub

Public Sub Query_AddArgument( _
    ByVal argName As String, _
    ByVal argType As ZPType, _
    ByVal argValue As Variant, _
    Optional ByVal argSize As ADO_LONGPTR = 0)
    
    Dim inOrOutTyp As ParameterDirectionEnum
    inOrOutTyp = adParamInput
    
    qryArgs_.AppendStringRow argName & "|" & argType & "|" & inOrOutTyp & "|" & argSize & "|" & argValue

End Sub

Public Function Query_RunGD() As GD
    Dim ret As GD
    Set ret = db_.RunSProc(qryName_, qryArgs_)
    Set Query_RunGD = ret
End Function


Public Function Query_RunS() As String
    Dim ret As String
    
    On Error Resume Next
    ret = Me.Query_RunGD(1, 2)
    On Error GoTo 0
    
    Query_RunS = ret
End Function
