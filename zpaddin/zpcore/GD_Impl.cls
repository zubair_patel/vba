VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "GD_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Option Base 0

Implements GD ' Implements Interface for a grid of data_

Private Const XDimension = 1
Private Const YDimension = 2

'Private Const DOUBLE_TOLERANCE = 0.00000000001
Private Const DOUBLE_TOLERANCE = 0.00000001
Private Const FLOAT_TOLERANCE = 0.000001

Private hasHeader_ As Tristate
Private data_() As Variant
Private columnTypes_ As Dictionary ' map<Long clm, ZPType typ>
Private columnIndices_ As Dictionary ' map<String hdr, Long clm>
Private columnStringToDateConvertor_ As Dictionary ' map<Long clm, Str2Dt convertor>
'

Public Sub Class_Initialize()
    hasHeader_ = TristateMixed
    Set columnTypes_ = New Dictionary
    Set columnIndices_ = New Dictionary
    Set columnStringToDateConvertor_ = New Dictionary
End Sub

Public Sub Class_Terminate()
    On Error Resume Next
    Set columnTypes_ = Nothing
    Set columnIndices_ = Nothing
    Set columnStringToDateConvertor_ = Nothing
    clearData
End Sub


Friend Property Get ColumnTypes() As Dictionary
    Set ColumnTypes = columnTypes_
End Property

Friend Property Set ColumnTypes(ByRef d As Dictionary)
    Set columnTypes_ = d
End Property

Friend Property Get StringToDateConvertors() As Dictionary
    Set StringToDateConvertors = columnStringToDateConvertor_
End Property

Friend Property Set StringToDateConvertors(ByRef d As Dictionary)
    Set columnStringToDateConvertor_ = StringToDateConvertors
End Property


Private Sub clearData()
    Dim emptyVar() As Variant
    data_ = emptyVar
End Sub

Public Sub GD_Initialize( _
    ByVal clms As Long, _
    ByVal rows As Long, _
    Optional ByVal containsHeader As Tristate = TristateMixed)

    If clms = 0 Or rows = 0 Then
        Dim tmp() As Variant
        data_ = tmp
    Else
        ReDim data_(0 To clms - 1, 0 To rows - 1)
    End If
    hasHeader_ = containsHeader

End Sub

Public Sub GD_InitializeWithString( _
    ByVal s As String, _
    Optional ByVal delim As String = "|", _
    Optional ByVal createColumnData As Boolean = False, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1, _
    Optional ByVal containsHeader As Tristate = TristateFalse)

    If s = "" Then
        clearData
        Exit Sub
    End If

    If createColumnData = True Then
        Dim v As Variant
        v = Split(s, delim)
        Me.GD_Initialize 1, varArrayLength(v), containsHeader
        insertVarArray v, True, 1, 1
    Else
        Me.GD_AppendStringRow s, delim
        Me.GD_HasHeader = containsHeader ' Must be called after the AppendStringRow
    End If

    Me.GD_Expand clms, rows

End Sub

Public Sub GD_InitializeWithRange( _
    ByRef rge As Range, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1, _
    Optional ByVal containsHeader As Tristate = TristateFalse)

    Dim v As Variant
    v = rge.Value

    If IsEmpty(v) = True Then
        Exit Sub
    End If

    Me.GD_Initialize _
        Max(rge.Columns.count, clms), _
        Max(rge.rows.count, rows), _
        containsHeader

    If IsArray(v) = False Then
        Me.GD_Value(1, 1) = v
        Exit Sub
    End If

    Dim rowOffset As Long
    Dim clmOffset As Long

    rowOffset = 1 - LBound(v, 1)  ' could be either 0 or 1
    clmOffset = 1 - LBound(v, 2)   ' could be either 0 or 1

    Dim c As Long
    Dim r As Long

    For r = 1 To rge.rows.count
        For c = 1 To rge.Columns.count
            ' Note: Access is transposed between range array and grid array
            Me.GD_Value(c, r) = v(r - rowOffset, c - clmOffset)
        Next c
    Next r

End Sub

Public Sub GD_InitializeWithDictionary( _
    ByRef dict As Dictionary, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1)

    clms = Max(2, clms)
    rows = Max(dict.count, rows)

    Me.GD_Initialize clms, rows, TristateFalse

    Dim r As Long
    r = 0

    Dim v As Variant
    For Each v In dict
        r = r + 1
        Me.GD_Value(1, r) = v
        Me.GD_Value(2, r) = dict.Item(v)
    Next

End Sub

Public Sub GD_InitializeWithADORS( _
    ByRef rs As ADODB.Recordset, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1)

    If rs.Fields.count > 0 Then
        Dim var() As Variant: var = rs.GetRows
        clms = Max(rs.Fields.count, clms)

        Me.GD_Initialize clms, 1, TristateTrue ' Add one row for the heading

        Dim fld As ADODB.Field
        Dim c As Long: c = 0
        For Each fld In rs.Fields
            c = c + 1
            Me.GD_Value(c, 1) = fld.Name
        Next

        Dim gd2 As New GD_Impl
        gd2.Grid = var
        Me.GD_Append gd2
        Me.GD_ExpandRows rows
    End If

End Sub

Public Function GD_Clone() As GD
    Dim ret As GD
    Set ret = New GD_Impl
    ret.Initialize Me.GD_NbrOfColumns, Me.GD_NbrOfRows, Me.GD_HasHeader
    
    Dim v As Variant
    For Each v In columnTypes_
        ret.ColumnType(v) = columnTypes_(v)
    Next
    
    Dim v2 As Variant
    For Each v2 In columnStringToDateConvertor_
        ret.SetStringToDateConvertor v, columnStringToDateConvertor_(v2)
    Next v2
    
    Dim r As Long
    For r = 1 To Me.GD_NbrOfRows
        Dim c As Long
        For c = 1 To Me.GD_NbrOfColumns
            ret(c, r) = Me.GD_Value(c, r)
        Next c
    Next r
    
    Set GD_Clone = ret
    
End Function

Public Sub GD_SetStringToDateConvertor( _
    ByVal clm As Long, _
    ByRef conv As Str2Dt)

    If columnStringToDateConvertor_.Exists(clm) Then
        columnStringToDateConvertor_.Remove (clm)
    End If

    Set columnStringToDateConvertor_(clm) = conv
End Sub

Public Property Let GD_HasHeader( _
    ByVal containsHeader As Tristate)

    hasHeader_ = containsHeader
End Property

Public Property Get GD_StartIndex() As Long
    Dim ret As Long
    If hasHeader_ = TristateTrue Then
        ret = 2
    Else
        ret = 1
    End If
    GD_StartIndex = ret
End Property

Public Property Get GD_HasData() As Boolean
    Dim ret As Boolean
    
    ret = Me.GD_NbrOfRows >= Me.GD_StartIndex
    
    GD_HasData = ret
End Property

Public Property Get GD_HasHeader() As Tristate
    GD_HasHeader = hasHeader_
End Property

Public Property Let GD_FieldType( _
    ByVal fieldName As String, _
    ByVal clmType As ZPType)

    Me.GD_ColumnType(Me.GD_ColumnIndex(fieldName)) = clmType
End Property

Public Property Get GD_FieldType( _
    ByVal fieldName As String) As ZPType

    Dim ret As ZPType
    ret = Me.GD_ColumnType(Me.GD_ColumnIndex(fieldName))
    GD_FieldType = ret
End Property

Public Property Let GD_ColumnType( _
    ByVal clm As Long, _
    ByVal clmType As ZPType)

    If columnTypes_.Exists(clm) = True Then
        columnTypes_.Remove (clm)
    End If
    columnTypes_.Add clm, clmType
End Property

Public Property Get GD_ColumnType( _
    ByVal clm As Long) As ZPType

    Dim ret As ZPType
    If columnTypes_.Exists(clm) Then
        ret = columnTypes_.Item(clm)
    Else
        ret = ZPVariant
    End If

    GD_ColumnType = ret
End Property

Public Function GD_SortedGrid( _
    ByVal clm As Long) As GD

    ' Uses Insertion Sort algorithm
    ' http://en.wikipedia.org/wiki/Insertion_sort

    Dim ret As GD
    Set ret = Me.GD_Clone

    Dim startRow As Long

    If ret.HasHeader = TristateTrue Then
        startRow = 2
    Else
        startRow = 1
    End If

    Dim r As Long
    Dim q As Long
    Dim val As Variant
    Dim valRow As GD
    Dim done As Boolean

    For r = startRow + 1 To ret.NbrOfRows
        val = ret(clm, r)
        Set valRow = ret.EntireRow(r)
        q = r - 1
        done = False

        While done = False
            If ret(clm, q) > val Then
                ret.UpdateRow ret.EntireRow(q), q + 1
                q = q - 1
                If q < startRow Then
                    done = True
                End If
            Else
                done = True
            End If
        Wend
        ret.UpdateRow valRow, q + 1

    Next r

    Set GD_SortedGrid = ret
End Function

Private Sub validateBounds( _
    ByVal clm As Long, _
    ByVal row As Long, _
    ByVal methodName As String)

    If clm < 1 Or row < 1 Then
        Err.Raise ExceptionCode.IndexOutOfRange, "GD_Impl." & methodName, _
        "Indexed clm=" & clm & ", row=" & row
    End If

    Dim clms As Long: clms = Me.GD_NbrOfColumns

    If clm > clms Then
        Err.Raise ExceptionCode.IndexOutOfRange, "GD_Impl." & methodName, _
        "Indexed clm=" & clm & ", total clms=" & clms
    End If

    Dim rows As Long: rows = Me.GD_NbrOfRows

    If row > rows Then
        Err.Raise ExceptionCode.IndexOutOfRange, "GD_Impl." & methodName, _
        "Indexed row=" & row & ", total rows=" & rows
    End If
End Sub


' Make this the default property [Attribute Value.VB_UserMemId = 0]
Public Property Let GD_Value( _
    ByVal clm As Long, _
    ByVal row As Long, _
    datum As Variant)

    validateBounds clm, row, "Value"
    setValue clm, row, datum

End Property

' Make this the default property [Attribute Value.VB_UserMemId = 0]
Public Property Get GD_Value( _
    ByVal clm As Long, _
    ByVal row As Long) As Variant

    validateBounds clm, row, "Value"

    Dim clmOffset As Long
    Dim rowOffset As Long

    clmOffset = LBound(data_, XDimension)
    rowOffset = LBound(data_, YDimension)

    If IsObject(data_(clm - 1 + clmOffset, row - 1 + rowOffset)) Then
        Set GD_Value = data_(clm - 1 + clmOffset, row - 1 + rowOffset)
    Else
        GD_Value = data_(clm - 1 + clmOffset, row - 1 + rowOffset)
    End If

End Property

Public Sub GD_InsertExpand( _
    ByVal clm As Long, _
    ByVal row As Long, _
    datum As Variant)

    Me.GD_Expand clm, row
    Me.GD_Value(clm, row) = datum

End Sub

Public Property Let GD_FieldValue( _
    ByVal heading As String, _
    ByVal row As Long, _
    datum As Variant)

    Dim clmIdx As Long
    clmIdx = Me.GD_ColumnIndex(heading)

    If clmIdx = 0 Then
        Err.Raise ExceptionCode.Argument, _
            "GD_Impl.FieldValue", _
            "Heading='" & heading & "', " & _
            "Row=" & row
    End If

    Me.GD_Value(clmIdx, row) = datum
End Property

Public Property Get GD_FieldValue( _
    ByVal heading As String, _
    ByVal row As Long) As Variant

    Dim clmIdx As Long
    clmIdx = Me.GD_ColumnIndex(heading)

    If clmIdx = 0 Then
        Err.Raise ExceptionCode.Argument, _
            "GD_Impl.FieldValue", _
            "Heading='" & heading & "', " & _
            "Row=" & row
    End If

    GD_FieldValue = Me.GD_Value(clmIdx, row)
End Property

Public Function GD_FindFieldValue( _
    ByVal what As Variant, _
    ByVal fieldName As String, _
    Optional ByRef foundClm As Long, _
    Optional ByRef foundRow As Long) As Boolean

    Dim ret As Boolean
    ret = Me.GD_FindValue( _
        what, _
        foundClm, _
        foundRow, _
        Me.GD_ColumnIndex("fieldName"))

    GD_FindFieldValue = ret

End Function

Public Function GD_FindValue( _
    ByVal what As Variant, _
    Optional ByRef foundClm As Long, _
    Optional ByRef foundRow As Long, _
    Optional ByVal clmLookup As Long = -1, _
    Optional ByVal rowLookup As Long = -1) As Boolean

    ' Also populates foundClm and foundRow

    GD_FindValue = False

    Dim startC As Long, endC As Long
    Dim startR As Long, endR As Long

    If clmLookup > 0 Then
        startC = clmLookup
        endC = clmLookup
    Else
        startC = 1
        endC = Me.GD_NbrOfColumns
    End If

    If rowLookup > 0 Then
        startR = rowLookup
        endR = rowLookup
    Else
        startR = 1
        endR = Me.GD_NbrOfRows
    End If

    Dim c As Long, r As Long

    For c = startC To endC
        For r = startR To endR

            If Me.GD_Value(c, r) = what Then
                foundClm = c
                foundRow = r
                GD_FindValue = True
                Exit Function
            End If
        Next r
    Next c

End Function

Public Property Get GD_ColumnIndex( _
    ByVal heading As String) As Long

    If columnIndices_.Exists(heading) = False Then

        Dim idx As Long
        idx = 0 ' ret 0 if not found

        Dim c As Long
        For c = 1 To Me.GD_NbrOfColumns
            If Me.GD_Value(c, 1) = heading Then
                idx = c
                Exit For
            End If
        Next c

        columnIndices_(heading) = idx

    End If

    GD_ColumnIndex = columnIndices_(heading)

End Property

Public Property Get GD_RowAsString( _
    ByVal row As Long, _
    Optional ByVal delim As String = "|")
    
    Dim ret As String

    Dim c As Long
    For c = 1 To Me.GD_NbrOfColumns
        ret = ret & CStr(Me.GD_Value(c, row)) & delim
    Next c
    ret = Left(ret, Len(ret) - Len(delim))

    GD_RowAsString = ret
End Property

Public Property Get GD_ColumnAsString( _
    ByVal clm As Long, _
    Optional ByVal delim As String = "|", _
    Optional ByVal skipHeader As Boolean = True)
    
    Dim ret As String
    
    Dim strt As Long
    
    If skipHeader = True Then
        strt = Me.GD_StartIndex
    Else
        strt = 1
    End If

    Dim r As Long
    For r = strt To Me.GD_NbrOfRows
        ret = ret & CStr(Me.GD_Value(clm, r)) & delim
    Next r
    ret = Left(ret, Len(ret) - Len(delim))

    GD_ColumnAsString = ret
End Property

Public Sub GD_AddFilterCriteria( _
    ByVal heading As String, _
    ByVal op As Operator, _
    ByVal val As Variant, _
    ByRef fltr As DF)

    If fltr Is Nothing Then
        Set fltr = CoreFactory.CreateDataFilter(Me.GD_HeaderRow)
    End If

    fltr.AddFieldCriteria heading, op, val

End Sub


Public Property Get GD_Filtered( _
    ByRef dataFilter As DF) As GD

    ' TODO - Test all operators

    '   Filter dictionary :
    '
    '   fieldFilter Key = "description"
    '   filter(1,1) = "Operator"    :   filter(2,1) = "Value"
    '   filter(1,2) = "Contains"    :   filter(2,2) = "Surname"
    '
    '   fieldFilter Key = "name"
    '   filter(1,1) = "Operator"    :   filter(2,1) = "Value"
    '   filter(1,2) = "="           :   filter(2,2) = "zubair"
    '   filter(1,3) = "<"           :   filter(2,3) = "ba"
    '   filter(1,4) = ">"           :   filter(2,4) = "ba"
    '   filter(1,5) = "BeginsWith"  :   filter(2,5) = "Sam"
    '   filter(1,6) = "EndsWith"    :   filter(2,6) = "ntha"
    '
    '   fieldFilter Key = "val"
    '   filter(1,1) = "Operator"    :   filter(2,1) = "Value"
    '   filter(1,2) = "<>"          :   filter(2,2) = 5

    Dim fltrd As GD
    Set fltrd = Me.GD_Clone

    Dim clmIndex As Variant
    For Each clmIndex In dataFilter.AllCriteria
        Set fltrd = applyColumnFilter( _
            CLng(clmIndex), _
            dataFilter, _
            fltrd)
    Next

    Set GD_Filtered = fltrd
End Property

Private Function applyColumnFilter( _
    ByVal clmIdx As Long, _
    ByRef dataFilter As DF, _
    ByRef sourceData As GD) As GD

    Dim columnFilter As GD
    Set columnFilter = dataFilter.ColumnCriteria(clmIdx)

    Dim ret As GD
    
    If columnFilter.NbrOfRows < 2 Then    ' Don't process as the filter is empty
        Set ret = sourceData
        GoTo EXITFUNCTION
    ElseIf sourceData.HasHeader = TristateTrue Then
        Set ret = sourceData.HeaderRow
    Else
        Set ret = New GD_Impl
    End If

    Dim r As Long ' sourceData Rows
    For r = sourceData.StartIndex To sourceData.NbrOfRows

        Dim fr As Long  ' filter Rows

        Dim sourceValue As Variant
        sourceValue = sourceData(clmIdx, r) ' Note use r, not fr for source row index

        For fr = columnFilter.StartIndex To columnFilter.NbrOfRows ' fr is filter row index

            Dim op As Operator
            op = columnFilter.FieldValue("Operator", fr)
            
            Dim filterValue As Variant
            filterValue = columnFilter.FieldValue("Value", fr)

            If dataFilter.MatchesCriteria(sourceValue, op, filterValue) = True Then
                ret.Append sourceData.EntireRow(r)
                Exit For
            End If

        Next fr

    Next r

    Set applyColumnFilter = ret
    
EXITFUNCTION:

End Function

Public Property Get GD_Transposed() As GD

    ' HasHeader will be false for returned object

    Dim ret As GD
    Set ret = New GD_Impl

    ret.Initialize Me.GD_NbrOfRows, Me.GD_NbrOfColumns, TristateFalse

    Dim c As Long, r As Long
    For c = 1 To Me.GD_NbrOfColumns
        For r = 1 To Me.GD_NbrOfRows
            ret(r, c) = Me.GD_Value(c, r)
        Next r
    Next c

    Set GD_Transposed = ret

End Property

Public Property Get GD_Tail( _
    ByVal rows As Long) As GD

    ' case
    ' rows = 5, maxrows = 10, start = 6     maxrows-rows+1
    ' rows = 10, maxrows = 12, start = 3    maxrows-rows+1
    ' rows = 10, maxrows = 100, start = 91  maxrows-rows+1
    ' rows = 100, maxrows = 10, start = 1
    ' rows = 10, maxrows = 10, start = 1
    ' rows = 12, maxrows = 10, start = 1

    Dim startRow As Long

    If rows > Me.GD_NbrOfRows Then
        startRow = 1
    Else
        startRow = Me.GD_NbrOfRows - rows + 1
    End If

    Set GD_Tail = Me.GD_Subset(1, startRow)
End Property

Public Property Get GD_Subset( _
    Optional ByVal startColumn As Long = 1, _
    Optional ByVal startRow As Long = 1, _
    Optional ByVal Columns As Long = -1, _
    Optional ByVal rows As Long = -1) As GD

    If startColumn > Me.GD_NbrOfColumns Or startRow > Me.GD_NbrOfRows Then
        Set GD_Subset = New GD_Impl
        GD_Subset.HasHeader = TristateFalse
        Exit Property
    End If

    If Columns < 0 Then Columns = Me.GD_NbrOfColumns - startColumn + 1
    If rows < 0 Then rows = Me.GD_NbrOfRows - startRow + 1

    Dim endColumn As Long: endColumn = startColumn + Columns - 1
    Dim endRow As Long: endRow = startRow + rows - 1

    Dim ret As GD
    Set ret = New GD_Impl

    ret.Initialize Columns, rows, TristateFalse

    Dim r As Long
    Dim c As Long

    Dim retR As Long: retR = 1
    Dim retC As Long: retC = 1

    For r = startRow To endRow
        retC = 1
        For c = startColumn To endColumn
            ret(retC, retR) = Me.GD_Value(c, r)
            retC = retC + 1
        Next c
        retR = retR + 1
    Next r

    Set GD_Subset = ret

End Property


Public Function GD_Equal( _
    ByRef compareWith As GD) As Boolean

    Dim ret As Boolean
    ret = True

    Dim rows As Long: rows = Me.GD_NbrOfRows
    Dim clms As Long: clms = Me.GD_NbrOfColumns

    If compareWith Is Nothing Then
        ret = False
        GoTo EXITFUNCTION
    ElseIf compareWith.HasHeader <> Me.GD_HasHeader Then
        ret = False
    ElseIf compareWith.NbrOfColumns <> clms Then
        ret = False
    ElseIf compareWith.NbrOfRows <> rows Then
        ret = False
    Else
        Dim r As Long
        Dim c As Long

        For r = 1 To rows
            For c = 1 To clms
                If compareWith(c, r) <> Me.GD_Value(c, r) Then
                    If Me.GD_ColumnType(c) = ZPDouble Then
                        If Abs(compareWith(c, r) - Me.GD_Value(c, r)) > DOUBLE_TOLERANCE Then
                            ret = False
                            Exit For
                        End If
                    ElseIf Me.GD_ColumnType(c) = ZPFloat Then
                        If Abs(compareWith(c, r) - Me.GD_Value(c, r)) > FLOAT_TOLERANCE Then
                            ret = False
                            Exit For
                        End If
                    ElseIf IsNumeric(Me.GD_Value(c, r)) And IsNumeric(compareWith(c, r)) Then
                        If Abs(compareWith(c, r) - Me.GD_Value(c, r)) > DOUBLE_TOLERANCE Then
                            ret = False
                            Exit For
                        End If
                    Else
                        ret = False
                        Exit For
                    End If
                End If
            Next c
        Next r
    End If

EXITFUNCTION:
    GD_Equal = ret
End Function

Public Function GD_Lookup( _
    ByVal key As String, _
    ByVal keyField As String, _
    ByVal valueField As String, _
    ByRef foundValue As Variant, _
    Optional ByVal caseSensitive As Boolean = False) As Boolean

    ' Finds the first occurrence of key in the keyColumn
    ' And populates foundValue with it
    ' Returns true if value is found
    ' Leaves passed-in foundValue unchanged if not found

    Dim ret As Boolean
    ret = GD_VLookup( _
        key, _
        GD_ColumnIndex(valueField), _
        foundValue, _
        GD_ColumnIndex(keyField), _
        caseSensitive)

    GD_Lookup = ret

End Function

Public Function GD_VLookup( _
    ByVal key As String, _
    ByVal valueColumn As Long, _
    ByRef foundValue As Variant, _
    Optional ByVal keyColumn As Long = 1, _
    Optional ByVal caseSensitive As Boolean = False) As Boolean

    ' Finds the first occurrence of key in the keyColumn
    ' And populates foundValue with it
    ' Returns true if value is found
    ' Leaves passed-in foundValue unchanged if not found

    Dim r As Long
    Dim c As Long

    For r = 1 To Me.GD_NbrOfRows

        Dim current As String

        If caseSensitive = False Then
            current = UCase(Me.GD_Value(keyColumn, r))
            key = UCase(key)
        Else
            current = Me.GD_Value(keyColumn, r)
        End If

        If current = key Then
            foundValue = Me.GD_Value(valueColumn, r)
            GD_VLookup = True
            Exit Function
        End If
    Next r

    GD_VLookup = False

End Function

Public Sub GD_UpdateColumn( _
    ByRef clmData As GD, _
    ByVal clmNbr As Long)

    ' NOTE: if clmData has fewer rows than this, no error is raised

    Dim r As Long
    For r = 1 To clmData.NbrOfRows
        Me.GD_Value(clmNbr, r) = clmData(1, r)
    Next r
End Sub

Public Sub GD_UpdateRow( _
    ByRef rowData As GD, _
    ByVal rowNbr As Long)

    ' NOTE: if rowData has fewer clms than this, no error is raised

    Dim c As Long
    For c = 1 To rowData.NbrOfColumns
        Me.GD_Value(c, rowNbr) = rowData(c, 1)
    Next c
End Sub

Private Function blankCopy( _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1, _
    Optional ByVal hasHdr As Tristate = TristateMixed _
    ) As GD

    clms = Max(clms, Me.GD_NbrOfColumns)
    rows = Max(rows, Me.GD_NbrOfRows)

    If hasHdr = TristateMixed Then
        hasHdr = Me.GD_HasHeader
    End If

    Dim tmp As GD
    Set tmp = New GD_Impl
    tmp.Initialize clms, rows, hasHdr

    Dim v1 As Variant
    For Each v1 In columnTypes_
        tmp.ColumnType(CLng(v1)) = columnTypes_(v1)
    Next

    Dim v2 As Variant
    For Each v2 In columnStringToDateConvertor_
        tmp.SetStringToDateConvertor CLng(v2), columnStringToDateConvertor_(v2)
    Next

    Set blankCopy = tmp

End Function

Public Sub GD_InsertColumn( _
    ByRef clmData As GD, _
    ByVal clmNbr As Long)
    
    columnIndices_.RemoveAll

    Dim tmp As GD
    Set tmp = blankCopy(Me.GD_NbrOfColumns + 1)

    Dim r As Long
    Dim c As Long

    For r = 1 To tmp.NbrOfRows
        For c = 1 To clmNbr - 1
            tmp(c, r) = Me.GD_Value(c, r)
        Next c
    Next r

    c = clmNbr
    For r = 1 To tmp.NbrOfRows
        tmp(c, r) = clmData(1, r)
    Next r

    For r = 1 To tmp.NbrOfRows
        For c = (clmNbr + 1) To tmp.NbrOfColumns
            tmp(c, r) = Me.GD_Value(c - 1, r)
        Next c
    Next r

    Dim tmp2 As GD_Impl
    Set tmp2 = tmp
    Me.Grid = tmp2.Grid
End Sub

Public Sub GD_InsertRow( _
    ByRef rowData As GD, _
    ByVal rowNbr As Long)

    Dim tmp As GD
    Set tmp = blankCopy(, Me.GD_NbrOfRows + 1)

    Dim r As Long
    Dim c As Long

    For r = 1 To rowNbr - 1
        For c = 1 To tmp.NbrOfColumns
            tmp(c, r) = Me.GD_Value(c, r)
        Next c
    Next r

    r = rowNbr
    For c = 1 To tmp.NbrOfColumns
        tmp(c, r) = rowData(c, 1)
    Next c

    For r = (rowNbr + 1) To tmp.NbrOfRows
        For c = 1 To tmp.NbrOfColumns
            tmp(c, r) = Me.GD_Value(c, r - 1)
        Next c
    Next r

    Dim tmp2 As GD_Impl
    Set tmp2 = tmp
    Me.Grid = tmp2.Grid
End Sub

Public Sub GD_SwapColumns( _
    ByVal clm1 As Long, _
    ByVal clm2 As Long)

    columnIndices_.RemoveAll

    Dim gdClm1 As GD
    Set gdClm1 = Me.GD_EntireColumn(clm1)

    Dim gdClm2 As GD
    Set gdClm2 = Me.GD_EntireColumn(clm2)

    Me.GD_UpdateColumn gdClm2, clm1
    Me.GD_UpdateColumn gdClm1, clm2
End Sub

Public Sub GD_SwapRows( _
    ByVal row1 As Long, _
    ByVal row2 As Long)

    Dim gdRow1 As GD
    Set gdRow1 = Me.GD_EntireRow(row1)

    Dim gdRow2 As GD
    Set gdRow2 = Me.GD_EntireRow(row2)

    Me.GD_UpdateRow gdRow2, row1
    Me.GD_UpdateRow gdRow1, row2
End Sub

Public Sub GD_Expand( _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1)


    ' NOTE : Expanding the nbr of colums is very expensive
    '  as it involves creating a new array and then
    '  copying the contents from the orig to the new

    ' Preserves data

    Dim oldClms, oldRows  As Long
    oldClms = Me.GD_NbrOfColumns
    oldRows = Me.GD_NbrOfRows

    rows = Max(rows, oldRows)

    If clms > oldClms Then
        Dim tmp() As Variant
        ReDim tmp(0 To clms - 1, 0 To rows - 1)

        Dim c, r As Long
        For c = 0 To oldClms - 1
            For r = 0 To oldRows - 1
                tmp(c, r) = data_(c, r)
            Next r
        Next c

        data_ = tmp
    Else
        Me.GD_ExpandRows rows
    End If

End Sub

Public Sub GD_ExpandRows( _
    ByVal rows As Long)
    ' Preserves data_

    Dim clms As Long
    clms = Me.GD_NbrOfColumns

    If rows > Me.GD_NbrOfRows Then
        If clms < 1 Then Err.Raise ExceptionCode.IndexOutOfRange, _
            "GD_Impl.ExpandRows", "Incorrect number of columns " & clms

        ReDim Preserve data_(0 To clms - 1, 0 To rows - 1)
    End If
End Sub

Public Property Get GD_NbrOfDimensions() As Long

    Dim ret As Long

    If Me.GD_NbrOfRows = 1 Or Me.GD_NbrOfColumns = 1 Then
        ret = 1
    Else
        ret = arrayDimensions(data_)
    End If

    GD_NbrOfDimensions = ret
End Property

Public Sub GD_Append( _
    ByRef toAppend As GD)
    
    ' Preserves data_.  Will not expand columns

    Dim clms As Long: clms = Me.GD_NbrOfColumns
    Dim toAppendClms As Long: toAppendClms = toAppend.NbrOfColumns

    If clms > 0 And clms <> toAppendClms Then
        Err.Raise ExceptionCode.IndexOutOfRange, "GD_Impl.Append", _
        "Expected " & clms & " columns but received " & toAppendClms
    End If

    Dim rows As Long: rows = Me.GD_NbrOfRows
    Dim toAppendRows As Long: toAppendRows = toAppend.NbrOfRows

    Me.GD_Expand toAppendClms, rows + toAppendRows

    Dim c As Long, r As Long

    For c = 1 To toAppendClms
        For r = 1 To toAppendRows
            Me.GD_Value(c, r + rows) = toAppend.Value(c, r)
        Next r
    Next c

End Sub

Public Sub GD_AppendStringRow( _
    ByVal rowStr As String, _
    Optional ByVal delim As String = "|")

    ' Preserves data_.  Will expand columns only if data_ is empty

    Dim v As Variant
    v = Split(rowStr, delim)

    Dim c As Long: c = 1
    Dim r As Long: r = 1

    If Me.GD_NbrOfColumns = 0 Then
        Me.GD_Initialize varArrayLength(v), r, TristateFalse
    Else
        r = Me.GD_NbrOfRows + 1
        Me.GD_ExpandRows r
    End If

    insertVarArray v, False, c, r
End Sub

Private Sub insertVarArray( _
    ByRef v As Variant, _
    ByVal InsertColumn As Boolean, _
    ByVal startC As Long, _
    ByVal startR As Long)

    Dim r As Long
    Dim c As Long
    r = startR
    c = startC

    Dim ii As Long
    For ii = LBound(v) To UBound(v)
        Me.GD_Value(c, r) = v(ii)

        If InsertColumn = True Then
            r = r + 1
        Else
            c = c + 1
        End If
    Next ii

End Sub


Public Property Get GD_EntireRow( _
    ByVal row As Long) As GD

    Dim clms As Long
    clms = Me.GD_NbrOfColumns

    Dim ret As GD
    Set ret = New GD_Impl
    ret.Initialize clms, 1, TristateFalse

    Dim ii As Long
    For ii = 1 To clms
        ret(ii, 1) = Me.GD_Value(ii, row)
    Next ii

    Set GD_EntireRow = ret
End Property

Public Property Get GD_HeaderRow() As GD

    Dim ret As GD_Impl

    If Me.GD_HasHeader = TristateFalse Then
        Err.Raise ExceptionCode.FailedOperation, "GD_Impl.HeaderRow", _
            "The Grid Data does not have a marked header row"
    End If

    Set ret = Me.GD_EntireRow(1)
    ret.GD_HasHeader = TristateTrue
    Set ret.ColumnTypes = Me.ColumnTypes

    Set GD_HeaderRow = ret

End Property

Public Property Get GD_EntireColumn( _
    ByVal clm As Long) As GD

    Dim rows As Long
    rows = Me.GD_NbrOfRows

    Dim ret As GD
    Set ret = New GD_Impl
    ret.Initialize 1, rows, Me.GD_HasHeader

    Dim ii As Long
    For ii = 1 To rows
        ret(1, ii) = Me.GD_Value(clm, ii)
    Next ii

    Set GD_EntireColumn = ret

End Property

Public Property Get GD_UniqueColumn( _
    ByVal clm As Long) As GD

    Dim dataColumn As GD
    Set dataColumn = Me.GD_EntireColumn(clm)

    ' we used the dict to get the unique data

    Dim dict As New Dictionary
    Dim dummy As Byte

    Dim val As Variant

    Dim r As Long
    For r = 1 To dataColumn.NbrOfRows
        val = dataColumn(1, r)

        If dict.Exists(val) = False Then
            dict.Add val, dummy
        End If
    Next r

    Dim ret As GD
    Set ret = New GD_Impl
    ret.Initialize 1, dict.count

    Dim v As Variant
    Dim ii As Long: ii = 0
    For Each v In dict
        ii = ii + 1
        ret(1, ii) = v
    Next v
    
    If Me.GD_HasHeader = TristateTrue And Me.GD_Value(clm, 1) = ret(1, 1) Then
        ret.HasHeader = TristateTrue
    End If

    Set GD_UniqueColumn = ret

End Property

Public Function GD_SortedColumn( _
    ByVal clm As Long) As GD

    ' Uses Insertion Sort algorithm
    ' http://en.wikipedia.org/wiki/Insertion_sort

    Dim ret As GD
    Set ret = Me.GD_EntireColumn(clm)

    Dim startRow As Long

    If ret.HasHeader = TristateTrue Then
        startRow = 2
    Else
        startRow = 1
    End If

    Dim r As Long
    Dim q As Long
    Dim val As Variant
    Dim done As Boolean

    For r = startRow + 1 To ret.NbrOfRows
        val = ret(1, r)
        q = r - 1
        done = False

        While done = False
            If ret(1, q) > val Then
                ret(1, q + 1) = ret(1, q)
                q = q - 1
                If q < startRow Then
                    done = True
                End If
            Else
                done = True
            End If
        Wend
        ret(1, q + 1) = val

    Next r

    Set GD_SortedColumn = ret

End Function

Public Property Get GD_NbrOfColumns() As Long
    Dim ret As Long

    If arrayDimensions(data_) <> 0 Then
        ret = dimensionSize(data_, XDimension)
    Else
        ret = 0
    End If

    GD_NbrOfColumns = ret
End Property

Public Property Get GD_NbrOfRows() As Long

    Dim ret As Long

    ' A 1D grid means only have one row of data_
    If arrayDimensions(data_) = 1 Then
        ret = 1
    ElseIf arrayDimensions(data_) <> 0 Then
        ret = dimensionSize(data_, YDimension)
    Else
        ret = 0
    End If

    GD_NbrOfRows = ret

End Property

Public Function GD_GetDictionary( _
    ByVal keyColumn As Long, _
    ByVal valueColumn As Long) As Dictionary

    Dim ret As New Dictionary

    Dim r As Long
    For r = 1 To Me.GD_NbrOfRows
        Dim key As Variant
        Dim val As Variant

        key = Me.GD_Value(keyColumn, r)
        val = Me.GD_Value(valueColumn, r)

        If ret.Exists(key) = False Then
            ret(key) = val
        End If
    Next r

    Set GD_GetDictionary = ret
End Function

' GD_Impl specific

Private Sub setValue( _
    ByVal clm As Long, _
    ByVal row As Long, _
    ByRef val As Variant)

    Dim ii As Long: ii = clm - 1 + LBound(data_, XDimension)
    Dim jj As Long: jj = row - 1 + LBound(data_, YDimension)

    If IsNull(val) Then
        data_(ii, jj) = ""
        Exit Sub
    ElseIf IsEmpty(val) Then
        data_(ii, jj) = ""
        Exit Sub
    ElseIf IsError(val) Then
        data_(ii, jj) = ""
        Exit Sub
    ElseIf IsObject(val) = True Then
        Set data_(ii, jj) = val
        Exit Sub
    End If

    Dim typ As ZPType
    If columnTypes_.Exists(clm) = True Then
        If row = 1 And hasHeader_ = TristateTrue Then
            typ = ZPType.ZPString
        Else
            typ = columnTypes_(clm)
        End If
    Else
        typ = ZPType.ZPVariant
    End If

    Select Case typ

    Case ZPType.ZPBool
        If TypeName(val) = "Boolean" Then
            data_(ii, jj) = val
        ElseIf UCase(Trim(CStr(val))) = "TRUE" Then
            data_(ii, jj) = True
        ElseIf UCase(Trim(CStr(val))) = "FALSE" Then
            data_(ii, jj) = False
        ElseIf val > 0 Then
            data_(ii, jj) = True
        ElseIf val <= 0 Then
            data_(ii, jj) = False
        Else
            Err.Raise ExceptionCode.Argument, _
                "GD_Impl.setValue", _
                "Could not assign value '" & val & "' to cell [" & clm & _
                "," & row & "] as it is not a valid boolean"
        End If

    Case ZPType.ZPString
        data_(ii, jj) = Trim(CStr(val))

    Case ZPType.ZPDate

        If IsDate(val) And TypeName(val) <> "String" Then
            data_(ii, jj) = val
        ElseIf val = "" Then
            data_(ii, jj) = CDate(0)
        ElseIf columnStringToDateConvertor_.Exists(clm) = True Then
            data_(ii, jj) = columnStringToDateConvertor_(clm).Convert(Trim(CStr(val)))
        Else
            data_(ii, jj) = CDate(val)
        End If

    Case ZPType.ZPFloat

        If IsNumeric(val) Then
            data_(ii, jj) = CSng(val)
        ElseIf val = "" Then
            data_(ii, jj) = 0#
        ElseIf IsDate(val) Then
            data_(ii, jj) = CSng(val)
        ElseIf Trim(val) = "-" Then
            data_(ii, jj) = 0#
        End If

    Case ZPType.ZPDouble

        If IsNumeric(val) Then
            data_(ii, jj) = CDbl(val)
        ElseIf val = "" Then
            data_(ii, jj) = 0#
        ElseIf IsDate(val) Then
            data_(ii, jj) = CDbl(val)
        ElseIf Trim(val) = "-" Then
            data_(ii, jj) = 0#
        End If

    Case ZPType.ZPLong

        If IsNumeric(val) Then
            data_(ii, jj) = CLng(val)
        ElseIf val = "" Then
            data_(ii, jj) = 0
        ElseIf IsDate(val) Then
            data_(ii, jj) = CLng(val)
        ElseIf Trim(val) = "-" Then
            data_(ii, jj) = 0
        End If

    Case ZPType.ZPInteger

        If IsNumeric(val) Then
            data_(ii, jj) = CInt(val)
        ElseIf val = "" Then
            data_(ii, jj) = 0
        ElseIf IsDate(val) Then
            data_(ii, jj) = CInt(val)
        ElseIf Trim(val) = "-" Then
            data_(ii, jj) = 0
        End If

    Case ZPType.ZPVariant
        data_(ii, jj) = val

    End Select

    If IsEmpty(data_(ii, jj)) = True Then
        Err.Raise ExceptionCode.Argument, _
            "GD_Impl.setValue", _
            "Could not assign value '" & val & "' to cell [" & clm & _
            "," & row & "] as it is not of the correct ZPType: " & typ
    End If

End Sub

Private Function arrayDimensions( _
    ByRef ar() As Variant) As Long

    If IsArray(ar) = False Then
        Err.Raise ExceptionCode.InvalidObject, "GD_Impl.ArrayDimensions", "ar is not an Array"
    End If

    Dim dims As Long: dims = 0

    On Error GoTo EXITFUNCTION

    While (1)
        dims = dims + 1
        Dim upper As Long
        upper = UBound(ar, dims)
        arrayDimensions = dims
    Wend

    Exit Function

EXITFUNCTION:
    If Err.Number <> 9 Then
        Err.Raise Err.Number, "GD_Impl.ArrayDimensions", Err.Description
    End If

    Exit Function

End Function

Private Property Get dimensionSize( _
    ar() As Variant, _
    ByVal dimension As Long) As Long

    Dim totalDimensions As Long: totalDimensions = arrayDimensions(ar)

    If dimension > totalDimensions Then
        Err.Raise ExceptionCode.IndexOutOfRange, "GD_Impl.dimensionSize", _
            "Indexed=" & dimension & ", total=" & totalDimensions
    End If

    Dim upper As Long
    Dim lower As Long

    On Error GoTo ERRHANDLER

    upper = UBound(ar, dimension)
    lower = LBound(ar, dimension)
    dimensionSize = upper - lower + 1

EXITPROPERTY:
    Exit Property

ERRHANDLER:
    dimensionSize = 0

End Property

Private Property Get varArrayLength(v As Variant)

    On Error GoTo ERRHANDLER
    varArrayLength = UBound(v) - LBound(v) + 1
    Exit Property

ERRHANDLER:
    varArrayLength = 0
End Property

Public Property Let Grid(ByRef var() As Variant)

    Dim totalDimensions As Long: totalDimensions = arrayDimensions(var)

    If totalDimensions > 2 Then
        Err.Raise ExceptionCode.IndexOutOfRange, "GD_Impl.Grid", _
        "GridData is used for 2D data but got " & totalDimensions & " dimensions"
    End If

    ' Special case where we are passed in a 1D array ( ie a single row of data )
    If totalDimensions = 1 Then
        Me.GD_Initialize dimensionSize(var, 1), 1
        'Me.GD_UpdateRow var, 1
        Err.Raise ExceptionCode.NotImplemented, "GD_Impl.Grid", _
            "Initialising with a 1D array not yet implemented"
    Else
        data_ = var
    End If

End Property

Public Property Get Grid() As Variant()
    Grid = data_
End Property


' Diagnostic

Public Sub GD_DebugPrint()

    Dim s As String

    If Me.GD_HasHeader = TristateTrue And Me.GD_NbrOfRows > 0 Then
        s = "[Header Row 1]={" & Me.GD_RowAsString(1) & "}"
        Debug.Print s
    End If

    Dim r As Long
    For r = Me.GD_StartIndex To Me.GD_NbrOfRows
        s = "[Row " & r & "]=" & "{"
        s = s & Me.GD_RowAsString(r) & "}"
        Debug.Print s
    Next r

End Sub





















