VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DbClause_Values1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements DBClause

Private fieldTypes_ As GD
Private fieldValues_ As GD
Private dbh_ As DBH
'

Public Sub Initialize( _
    ByRef FieldTypes As GD, _
    ByRef fieldValues As GD, _
    ByRef dbHelper As DBH)

    Set fieldTypes_ = FieldTypes
    Set fieldValues_ = fieldValues
    Set dbh_ = dbHelper
    
End Sub

Public Sub Class_Terminate()
    If Err.Number <> 0 Then
    AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "DbClause_Values1.Class_Terminate"
    End If
End Sub

Public Function DBClause_GenerateString() As String

    ' Eg :  "Values('Src1', 'Ttl1', 1) "
    
    Dim sql As String
    Dim val As Variant
    
    sql = "Values("
    
    Dim c As Long
    For c = 1 To fieldTypes_.NbrOfColumns
        val = clauseValue(fieldTypes_.Value(c, 1), fieldValues_(c, 1))
        sql = sql & val & ", "
    Next c
    
    sql = Left(sql, Len(sql) - Len(", ")) ' remove trailing ", "
    sql = sql & ")"
    
    
    DBClause_GenerateString = sql
    
End Function

Private Property Get clauseValue( _
    ByVal FieldType As Long, _
    ByVal fieldVal As Variant) As String

    Dim ret As String

    If IsNumeric(fieldVal) Then
        ret = CStr(fieldVal)
    ElseIf IsEmpty(fieldVal) Then
        ret = "NULL"
    ElseIf IsNull(fieldVal) Then
        ret = "NULL"
    ElseIf (dbh_.IsTypeDate(FieldType) Or dbh_.IsTypeNumeric(FieldType)) Then
        If fieldVal = "" Then
            ret = "NULL"
        Else
            ret = fieldVal
        End If
    ElseIf IsDate(fieldVal) = False Then
        ' Not Numeric, Not Empty, Not Null, Not Date -> leaves pretty much Text
        ' Escape any single quotes in the value
        ret = Replace(fieldVal, "'", "''")
    End If
    
    If ret <> "NULL" Then
        If dbh_.IsTypeText(FieldType) = True Then
            ret = "'" & ret & "'"
        ElseIf dbh_.IsTypeDate(FieldType) = True Then
            'ret = "#" & ret & "#" ' Stupid Access effs up the dates !
            ret = "'" & Format(ret, "DD MMM YYYY HH:MM:SS") & "'"
        End If
    End If
    
    clauseValue = ret

End Property









