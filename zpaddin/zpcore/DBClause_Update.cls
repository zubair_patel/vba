VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DBClause_Update"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements DBClause

Private fields_ As GD
Private fieldTypes_ As GD
Private values_ As GD
Private tableName_ As String
Private dbh_ As DBH
'

Public Sub Initialize( _
    ByRef Fields As GD, _
    ByRef FieldTypes As GD, _
    ByRef values As GD, _
    tableName As String, _
    ByRef dbHelper As DBH)

    Set fields_ = Fields
    Set fieldTypes_ = FieldTypes
    Set values_ = values
    tableName_ = tableName
    Set dbh_ = dbHelper
End Sub

Public Sub Class_Terminate()
    If Err.Number <> 0 Then
    AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "DBClause_Update.Class_Terminate"
    End If
End Sub

Public Function DBClause_GenerateString() As String

    ' Eg :  "Update t_test Set([Source]='src1', [Title]='Ttl1' , [Val]=1) "
    
    Dim sql As String
    
    sql = "Update " & tableName_ & " Set "
    
    Dim c As Long
    
    For c = 1 To fields_.NbrOfColumns
        
        Dim Field As String
        Dim FieldType As Long
        Dim fieldVal As Variant
        
        Field = fields_(c, 1)
        If dbh_.IsAutoInrement(tableName_, Field) = False Then
            FieldType = fieldTypes_(c, 1)
            fieldVal = values_(c, 1)
        
            sql = sql & "[" & Field & "]=" & clauseValue(FieldType, fieldVal) & ", "
        End If
    Next c
    
    sql = Left(sql, Len(sql) - Len(", "))
    sql = sql & " "
    
    DBClause_GenerateString = sql
    
End Function

Private Property Get clauseValue( _
    ByVal FieldType As Long, _
    ByVal fieldVal As Variant) As String

    Dim ret As String

    If IsNumeric(fieldVal) Then
        ret = CStr(fieldVal)
    ElseIf IsEmpty(fieldVal) Then
        ret = "NULL"
    ElseIf IsNull(fieldVal) Then
        ret = "NULL"
    ElseIf (dbh_.IsTypeDate(FieldType) Or dbh_.IsTypeNumeric(FieldType)) Then
        If fieldVal = "" Then
            ret = "NULL"
        Else
            ret = fieldVal
        End If
    ElseIf IsDate(fieldVal) = False Then
        ' Not Numeric, Not Empty, Not Null, Not Date -> leaves pretty much Text
        ' Escape any single quotes in the value
        ret = Replace(fieldVal, "'", "''")
    Else
        ret = fieldVal
    End If
    
    If ret <> "NULL" Then
        If dbh_.IsTypeText(FieldType) = True Then
            ret = "'" & ret & "'"
        ElseIf dbh_.IsTypeDate(FieldType) = True Then
            'ret = "#" & ret & "#" ' Stupid Access effs up the dates !
            ret = "'" & Format(ret, "DD MMM YYYY HH:MM:SS") & "'"
        End If
    End If
    
    clauseValue = ret

End Property







