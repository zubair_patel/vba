VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Email_Outlook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Email

Private outlook_ As Outlook.Application

Private attachments_ As GD
'

Public Sub Initialize( _
    ByRef attachments As GD)
End Sub

Public Sub Class_Initialize()
    Set outlook_ = New Outlook.Application
End Sub

Public Sub Class_Terminate()
    Set outlook_ = Nothing
    Set attachments_ = Nothing
End Sub

Public Sub Email_SendEmail( _
    ByVal toList As String, _
    ByVal subjectText As String, _
    ByVal messageBody As String, _
    Optional autoSend As Boolean = False, _
    Optional ccList As String = "", _
    Optional bccList As String = "", _
    Optional bodyIsHtml As Boolean = False)
    
    Dim newMail As Outlook.MailItem
    Set newMail = outlook_.CreateItem(olMailItem)
    
    If bodyIsHtml = True Then
        newMail.BodyFormat = olFormatHTML
        newMail.HTMLBody = messageBody
    Else
        newMail.BodyFormat = olFormatPlain
    End If
    
    newMail.To = toList
    newMail.CC = ccList
    newMail.BCC = bccList
    
    newMail.subject = subjectText
    
    Dim a As Long
    For a = attachments_.StartIndex To attachments_.NbrOfRows
        newMail.attachments.Add attachments_(1, a)
    Next a
    
    If autoSend = True Then
        newMail.Send
    Else
        newMail.Display
    End If
    
    ' Clear out the attachements list after sending the email
    attachments_.Initialize 0, 0
    
    Set newMail = Nothing
    
End Sub

Public Sub Email_AttachFile(ByVal filePath As String)
    attachments_.AppendStringRow filePath
End Sub

