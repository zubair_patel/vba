VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "GDBinaryProcess_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements GDBinaryProcess
'

Public Sub Initialize()
End Sub

Public Function GDBinaryProcess_ProcessTwoGrids( _
    ByRef fn As BinaryOp, _
    ByRef gd1 As GD, _
    ByRef gd2 As GD, _
    ByRef checkFields As GD, _
    ByRef operateFields As GD) As GD ' {<gd1CheckFields>,<gd1OperatedFields>}
    
    ' checkFieldsMap -> {GD1|GD2}
    ' operateFieldsMap -> {GD1|GD2}
    
    ' Iterates gd1
    ' For Each field in checkFields, validates the data is the same
    ' For Each field in operateFields, operates on the data using the operand
    
    validate gd1, gd2, checkFields
    
    Dim ret As GD
    Set ret = CoreFactory.CreateGD( _
        checkFields.ColumnAsString(checkFields.ColumnIndex("GD1")) & "|" & _
        operateFields.ColumnAsString(operateFields.ColumnIndex("GD1")), _
        , , gd1.NbrOfRows)
    
    Dim ro As Long
        For ro = operateFields.StartIndex To operateFields.NbrOfRows
            
            Dim gd1Field As String: gd1Field = ""
            Dim gd2Field As String: gd2Field = ""
        
            gd1Field = operateFields.FieldValue("GD1", ro)
            gd2Field = operateFields.FieldValue("GD2", ro)
            
            Dim r As Long
            For r = gd1.StartIndex To gd1.NbrOfRows
                Dim val1 As Variant: val1 = Empty
                Dim val2 As Variant: val2 = Empty
                
                val1 = gd1.FieldValue(gd1Field, r)
                val2 = gd2.FieldValue(gd2Field, r)
                
                Dim rc As Long
                For rc = checkFields.StartIndex To checkFields.NbrOfRows
                    Dim chkFld As String: chkFld = ""
                    chkFld = checkFields.FieldValue("GD1", rc)
                    ret.FieldValue(chkFld, r) = gd1.FieldValue(chkFld, r)
                Next rc
                
                If val1 = Empty And val2 = Empty Then
                    ' do nothing ie ok
                Else
                    ret.FieldValue(gd1Field, r) = fn.Operate(val1, val2)
                End If
            Next r
                
        Next ro
    
    Set GDBinaryProcess_ProcessTwoGrids = ret
End Function

Private Sub validate( _
    ByRef gd1 As GD, _
    ByRef gd2 As GD, _
    ByRef checkFields As GD)
    
    Dim errMsg As String: errMsg = ""
    
    If gd1.HasHeader <> TristateTrue Then
        errMsg = "Grid1 doesn't have a header"
    ElseIf gd2.HasHeader <> TristateTrue Then
        errMsg = "Grid2 doesn't have a header"
    ElseIf gd1.NbrOfRows <> gd2.NbrOfRows Then
        errMsg = "Grid1 and Grid2 don't have the same number of rows"
    Else
        Dim rc As Long
        For rc = checkFields.StartIndex To checkFields.NbrOfRows
            
            Dim gd1Field As String: gd1Field = ""
            Dim gd2Field As String: gd2Field = ""
        
            gd1Field = checkFields.FieldValue("GD1", rc)
            gd2Field = checkFields.FieldValue("GD2", rc)
            
            Dim r As Long
            For r = gd1.StartIndex To gd1.NbrOfRows
                Dim val1 As Variant: val1 = Empty
                Dim val2 As Variant: val2 = Empty
                
                val1 = gd1.FieldValue(gd1Field, r)
                val2 = gd2.FieldValue(gd2Field, r)
                
                If val1 = Empty And val2 = Empty Then
                    ' do nothing ie ok
                ElseIf val1 <> val2 Then
                    errMsg = _
                        "Grid1(" & gd1Field & "," & r & ")=" & val1 & " does not match " & _
                        "Grid2(" & gd2Field & "," & r & ")=" & val2
                    GoTo THROWIFERR
                End If
            Next r
                
        Next rc
    End If
    
THROWIFERR:
    If errMsg <> "" Then
        Err.Raise ExceptionCode.InvalidObject, "GDBinaryProcess_Impl.validate", _
            errMsg
    End If
    
    Exit Sub
    
End Sub
