VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "LG_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements LG

Private pauseDebug_ As Boolean
Private forceDebug_ As Boolean
Private debugList_ As Dictionary ' Store a list of strings

Private start_ As String

Private csr_ As CSR
'


Public Sub Initialize(ByRef crsr As CSR)
    
    Set csr_ = crsr
    start_ = csr_.RefNameOrAddress
    
    pauseDebug_ = False
    forceDebug_ = False
    
    Set debugList_ = New Dictionary
    ' Place a dash at front so we don't debug by default
    
    debugList_.Add "LG_Impl.Initialize", True
    
End Sub

Public Sub Class_Terminate()
    On Error Resume Next
    Set csr_ = Nothing
    Set debugList_ = Nothing
End Sub

Public Sub LG_CloseLog()
    Set csr_ = Nothing
    Set debugList_ = Nothing
End Sub

Public Sub LG_AddToDebugList( _
    ByVal source As String)
    If debugList_.Exists(source) = False Then
        debugList_.Add source, True
    End If
End Sub


Private Property Get logDebug(ByVal source As String) As Boolean
  
    Dim ret As Boolean
    
    If forceDebug_ = True Then
        ret = True
    ElseIf pauseDebug_ = True Then
        ret = False
    ElseIf Environ("ENV") = "PRD" Then
        ret = False
    Else
        ' We have not forced debugging
        ' nor have we paused debugging
        ' and we are not in a PRD env
        
        ret = False
        
        Dim v As Variant
        For Each v In debugList_
            Dim listItem As String
            listItem = LCase(CStr(v))
            
            If InStr(1, LCase(source), listItem) <> 0 Then
                ret = True
            End If
        Next
    
    End If
    
    logDebug = ret
    
End Property

Public Sub LG_PauseDebug()
    If forceDebug_ = False Then
        pauseDebug_ = True
    End If
End Sub

Public Sub LG_ResumeDebug()
    pauseDebug_ = False
End Sub

Public Sub LG_ForceDebug()
    pauseDebug_ = False
    forceDebug_ = True
End Sub

Public Sub LG_ResetDebug()
    forceDebug_ = False
    pauseDebug_ = False
End Sub

Public Property Get LG_Console(lgType As LogType) As Worksheet
    Set LG_Console = csr_.Sheet
End Property


Public Sub LG_Clear()
    
    csr_.LocateNameData start_
    
    While csr_.Ref <> ""
        csr_.Ref = ""
        csr_.Down
    Wend
    
    csr_.LocateNameData start_
End Sub

Public Sub LG_LogI(ByVal s As String)
    LG_Log "INFO: " & s
End Sub

Public Sub LG_LogW(ByVal s As String)
    LG_Log "WARNING: " & s
End Sub

Public Sub LG_LogE(ByVal s As String)
    LG_Log "ERROR: " & s
End Sub

Public Sub LG_LogD(s As String, source As String)
    If logDebug(source) = True Then
        LG_Log "DEBUG " & source & "(): " & s
    End If
End Sub

Public Sub LG_LogProgress() ' Used for showing progress
    
    csr_.LocateNameData start_
    While csr_.Ref <> ""
        csr_.Down
    Wend
    
    On Error Resume Next
    csr_.Up
    On Error GoTo 0
    
    Dim s As String
    s = csr_.Ref
    
    Dim logmsgprefix As String
    logmsgprefix = "DDMmmYY HH:MM:SS :  "
    
    Application.ScreenUpdating = True
    If Mid(csr_.Ref, Len(logmsgprefix), Len("INFO")) = "INFO" Or _
       Mid(csr_.Ref, Len(logmsgprefix), 3) = " . " Then
        s = Mid(csr_.Ref, Len(logmsgprefix)) & " . "
        csr_.Ref = ""
        LG_Log s
    Else
        LG_Log " . "
    End If
    Application.ScreenUpdating = False
    
End Sub

Public Sub LG_Log(ByVal s As String)
   
    ' We set this to true below if we are already on the log sheet
    Application.ScreenUpdating = False
    
    Dim finalSheet As Worksheet
    
    If IsErrorMessage(s) = True Then
        ' Not sure what is better.  We can show the log screen
        ' but that could be annoying as user will have to click
        ' to go back to orig 'run' screen. But at least errors are
        ' not missed out even if msgbox is suppressed.
        Set finalSheet = ActiveSheet
        'Set finalSheet = csr.sheet
    Else
        Set finalSheet = ActiveSheet
    End If
    
    csr_.Sheet.Activate
    csr_.LocateNameData start_
    While csr_.Ref <> ""
        csr_.Down
    Wend
    
    If csr_.Sheet.Name = finalSheet.Name Then
        ' This will cause the log window to scroll
        Application.ScreenUpdating = True
        csr_.GetRelativeRange(0, 1).Select
    End If
    
    Dim timestampPrefix As String
    timestampPrefix = Format(Now, "ddmmmyy hh:mm:ss") & " : "
    
    Dim msgColor As Long: msgColor = messageColor(s)
    Dim Remaining As String: Remaining = timestampPrefix & s
    
    ' We break the msg down to max 255 chars due to xl limitation
    While Remaining <> indent
        
        s = Left(Remaining, maxCharactersPerLine(""))
        Remaining = indent & Trim(Mid(Remaining, maxCharactersPerLine("") + 1))
        
        csr_.Ref.Value2 = s
        csr_.Ref.Font.ColorIndex = msgColor
        csr_.Ref.WrapText = False ' Need to reset Wrap in case msg contains a linefeed
        
        csr_.Down
    
    Wend
    
    finalSheet.Activate
    Application.ScreenUpdating = False
    
End Sub

Private Property Get indent() As String
    indent = "                                        "
End Property
Private Property Get maxCharactersPerLine(fixedPrefix As String) As Long
    maxCharactersPerLine = 255 - Len(fixedPrefix)
End Property

Private Property Get IsErrorMessage(s As String) As Boolean
    IsErrorMessage = (Left(s, 5) = "ERROR")
End Property

Private Function messageColor(s As String) As Long

    If Left(s, 5) = "ERROR" Then
        messageColor = RED_COLOR
    ElseIf Left(s, 7) = "WARNING" Then
        messageColor = ORANGE_COLOR
    ElseIf Left(s, 4) = "TODO" Then
        messageColor = ORANGE_COLOR
    ElseIf Left(s, 5) = "DEBUG" Then
        messageColor = BLUE_COLOR
    ElseIf s = ">" Then
        messageColor = BLACK_COLOR
    Else
        messageColor = GREEN_COLOR
    End If
    
End Function


