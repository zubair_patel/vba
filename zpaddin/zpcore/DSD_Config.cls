VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DSD_Config"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements DSD
'

Public Sub Initialize()
End Sub

Public Sub Class_Terminate()
    If Err.Number <> 0 Then
        AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "DSD_Config.Class_Terminate"
    End If
End Sub

Public Property Get DSD_Keys() As GD
    Set DSD_Keys = CreateGD(2, 1, TristateFalse)
    DSD_Keys.Value(1, 1) = "Section"
    DSD_Keys.Value(2, 1) = "Field"
End Property

Public Property Get DSD_Fields() As GD
    Set DSD_Fields = CreateGD(5, 1, TristateFalse)
    DSD_Fields.Value(1, 1) = "Section"
    DSD_Fields.Value(2, 1) = "Field"
    DSD_Fields.Value(3, 1) = "Detail"
    DSD_Fields.Value(4, 1) = "Description"
    DSD_Fields.Value(5, 1) = "DisplayOrder"
End Property

Public Property Get DSD_Table() As String
    DSD_Table = "T_Config"
End Property

Public Property Get DSD_DefaultOrderBy() As String
    DSD_DefaultOrderBy = "DisplayOrder"
End Property

Private Function CreateGD(clms As Long, rows As Long, HasHeader As Tristate) As GD
    Dim ret As GD
    Set ret = CoreFactory.CreateGD
    ret.Initialize clms, rows, HasHeader
    Set CreateGD = ret
End Function





