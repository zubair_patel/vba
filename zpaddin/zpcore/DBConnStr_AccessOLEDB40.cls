VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DBConnStr_AccessOLEDB40"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements DBConnStr

Private filePath_ As String
'

Public Sub Class_Terminate()
    If Err.Number <> 0 Then
        AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "DBConnStr_AccessOLEDB40.Class_Terminate"
    End If
End Sub

Public Sub DBConnStr_Initialize(filePath As String)
    filePath_ = filePath
End Sub

Public Property Get DBConnStr_ConnectionString() As String

    Dim fso As New FileSystemObject
    If fso.FileExists(filePath_) = False Then
        Err.Raise ExceptionCode.FileNotFound, _
            "DBConnStr_AccessOLEDB40.ConnectionString", _
            "File='" & filePath_ & "'"
    End If
    Set fso = Nothing

    Dim connstr As String
    
    connstr = connstr & "Provider=Microsoft.Jet.OLEDB.4.0;"
    connstr = connstr & "Data Source=" & filePath_ & "; "

    DBConnStr_ConnectionString = connstr

End Property







