VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Feed_ExcelActiveRange"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Feed

Private sourcePath_ As String
Private sourceSheetName_ As String

Private sourceWorkbook_ As Workbook
'


Public Sub Initialize(workbookPath As String, sheetName As String)
    sourcePath_ = workbookPath
    sourceSheetName_ = sheetName
End Sub

Public Sub Feed_CloseFeed()
    On Error Resume Next
    If Not (sourceWorkbook_ Is Nothing) Then
        sourceWorkbook_.Close
    End If
    Set sourceWorkbook_ = Nothing
End Sub

Public Function Feed_Load() As GD
        
    Application.ScreenUpdating = False
    Application.DisplayAlerts = False
    
    On Error GoTo ERRHANDLER
    Feed_CloseFeed
    
    Set sourceWorkbook_ = Workbooks.Open(sourcePath_, False, True, , , , , , , , , , False)
    
    Dim WS As Worksheet
    Set WS = sourceWorkbook_.Sheets(sourceSheetName_)
    
    Set Feed_Load = CoreFactory.CreateGD
    Feed_Load.InitializeWithRange WS.UsedRange
    
    Feed_CloseFeed
    
    Exit Function
    
ERRHANDLER:
    Dim errnbr As Long: errnbr = Err.Number
    Dim errdesc As String: errdesc = Err.Description
    Feed_CloseFeed
    
    Err.Raise ExceptionCode.FailedOperation, "Feed_ExcelActiveRange.Load", _
        "Source data from '" & sourcePath_ & "!" & sourceSheetName_ & "' not loaded"

End Function













