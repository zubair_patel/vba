VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ProcessInterrupt_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements ProcessInterrupt

Private stopProcess_ As Boolean
Private batchSize_ As Long
Private pauseSeconds_ As Long
Private count_ As Long
'

Public Sub Initialize( _
    ByVal batchSz As Long, _
    ByVal pauseSeconds As Long)
    
    batchSize_ = batchSz
    pauseSeconds_ = pauseSeconds
    Me.ProcessInterrupt_Reset
End Sub

Public Sub Class_Terminate()
    AppLog.LogD "Terminating interruptor", "ProcessInterrupt_Impl.Class_Terminate"
End Sub

Public Sub ProcessInterrupt_StopRun()
    stopProcess_ = True
End Sub

Public Sub ProcessInterrupt_Reset()
    stopProcess_ = False
    count_ = 0
End Sub

Public Property Get ProcessInterrupt_BatchSize() As Long
    Dim ret As Long
    ret = batchSize_
    ProcessInterrupt_BatchSize = batchSize_
End Property

Public Function ProcessInterrupt_ShouldProcessStop() As Boolean

    On Error Resume Next
    count_ = count_ + 1
    
    If count_ Mod batchSize_ = 0 Then

        Dim future As Date
        
        Dim origCursor As Variant
        origCursor = Application.Cursor
        
        AppLog.LogD "Pausing for " & pauseSeconds_ & " seconds ...", _
            "ProcessInterrupt_Impl.ShouldProcessStop"
        
        Application.Cursor = xlNorthwestArrow
        
        future = DateAdd("s", pauseSeconds_, Now)
        
        While Now < future
            DoEvents
        Wend
        
        AppLog.LogD "StopProcess=" & stopProcess_, "ProcessInterrupt_Impl.ShouldProcessStop"
        AppLog.LogD "... Resuming again", "ProcessInterrupt_Impl.ShouldProcessStop"
        
        Application.Cursor = origCursor
        
    End If
    
    ProcessInterrupt_ShouldProcessStop = stopProcess_
End Function


