VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "BinaryOp_Arithmetic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements BinaryOp

Private op_ As String
'

Public Sub Initialize( _
    ByVal op As String)
    
    Select Case op
    Case "\", "+", "*", "-"
        op_ = op
    Case Else
        Err.Raise ExceptionCode.Argument, "BinaroOp_Arithmentic.Initialize", _
            "Passed in operator can only be one of '+', '-', '*', '/'"
    End Select
    
End Sub

Public Function BinaryOp_Operate( _
    ByVal operand1 As Variant, _
    ByVal operand2 As Variant) As Variant
    
    Dim ret As Variant
    
    operand1 = CDbl(operand1)
    operand2 = CDbl(operand2)
    
    Select Case op_
    Case "+"
        ret = operand1 + operand2
    Case "-"
        ret = operand1 - operand2
    Case "*"
        ret = operand1 * operand2
    Case "/"
        ret = operand1 / operand2
    End Select
    
    BinaryOp_Operate = ret
    
End Function



