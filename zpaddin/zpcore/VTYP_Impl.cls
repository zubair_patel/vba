VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "VTYP_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements VTYP

Private stringToDateConvertor_ As Str2Dt
'

Public Sub Initialize( _
    Optional ByRef stringToDateConvertor As Str2Dt)
    
    Set stringToDateConvertor_ = stringToDateConvertor
End Sub

Public Function VTYP_TypeToString( _
    ByVal typ As ZPType) As String
    
    Dim ret As String
    
    Select Case typ
    Case ZPBool
        ret = "ZPBool"
    Case ZPString
        ret = "ZPString"
    Case ZPFloat
        ret = "ZPFloat"
    Case ZPDouble
        ret = "ZPDouble"
    Case ZPInteger
        ret = "ZPInteger"
    Case ZPLong
        ret = "ZPLong"
    Case ZPDate
        ret = "ZPDate"
    Case ZPVariant
        ret = "ZPVariant"
    Case ZPObject
        ret = "ZPObject"
    End Select
    
    VTYP_TypeToString = ret
End Function

Public Function VTYP_GetTypedValue( _
    ByVal orig As Variant, _
    ByVal typ As ZPType) As Variant
    
    Dim ret As Variant: ret = Empty
    
    If IsNull(orig) Then
        ret = ""
        Exit Function
    ElseIf IsEmpty(orig) Then
        ret = ""
        Exit Function
    ElseIf IsError(orig) Then
        ret = ""
        Exit Function
    ElseIf IsObject(orig) = True Then
        Set ret = orig
        Exit Function
    End If
        
    Select Case typ
    
    Case ZPType.ZPBool
        If TypeName(orig) = "Boolean" Then
            ret = orig
        ElseIf UCase(Trim(CStr(orig))) = "TRUE" Then
            ret = True
        ElseIf UCase(Trim(CStr(orig))) = "FALSE" Then
            ret = False
        ElseIf UCase(Trim(CStr(orig))) = "YES" Then
            ret = True
        ElseIf UCase(Trim(CStr(orig))) = "NO" Then
            ret = False
        ElseIf orig > 0 Then
            ret = True
        ElseIf orig <= 0 Then
            ret = False
        End If
        
    Case ZPType.ZPString
        ret = Trim(CStr(orig))
        
    Case ZPType.ZPDate
        
        If Not (stringToDateConvertor_ Is Nothing) Then
            ret = stringToDateConvertor_.Convert(Trim(CStr(orig)))
        ElseIf IsDate(orig) Then
            ret = orig
        ElseIf orig = "" Then
            ret = CDate(0)
        Else
            ret = CDate(orig)
        End If
        
    Case ZPType.ZPFloat
    
        If IsNumeric(orig) Then
            ret = CSng(orig)
        ElseIf orig = "" Then
            ret = 0#
        ElseIf IsDate(orig) Then
            ret = CSng(orig)
        ElseIf Trim(orig) = "-" Then
            ret = 0#
        End If
        
    Case ZPType.ZPDouble
    
        If IsNumeric(orig) Then
            ret = CDbl(orig)
        ElseIf orig = "" Then
            ret = 0#
        ElseIf IsDate(orig) Then
            ret = CDbl(orig)
        ElseIf Trim(orig) = "-" Then
            ret = 0#
        End If
        
    Case ZPType.ZPLong
    
        If IsNumeric(orig) Then
            ret = CLng(orig)
        ElseIf orig = "" Then
            ret = 0
        ElseIf IsDate(orig) Then
            ret = CLng(orig)
        ElseIf Trim(orig) = "-" Then
            ret = 0
        End If
        
    Case ZPType.ZPInteger
    
        If IsNumeric(orig) Then
            ret = CInt(orig)
        ElseIf orig = "" Then
            ret = 0
        ElseIf IsDate(orig) Then
            ret = CInt(orig)
        ElseIf Trim(orig) = "-" Then
            ret = 0
        End If
        
    Case ZPType.ZPVariant
        ret = orig
        
    End Select
    
    If IsEmpty(ret) = True Then
        Err.Raise ExceptionCode.Argument, _
            "VarType.GetTypedValue", _
            "Could not get typed value for '" & orig & _
                "' as it is not of the correct ZPType: " & Me.VTYP_TypeToString(orig)
    End If
    
    
    VTYP_GetTypedValue = ret
    
End Function
