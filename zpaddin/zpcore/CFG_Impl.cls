VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CFG_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements CFG

Private dat_ As GD ' "Section|Field|Detail|Description|DisplayOrder"
Private defaultDat_ As GD ' "Section|Field|Detail"
Private substituter_ As VariableSubstituter

Private varStartChar_ As String
Private varEndChar_ As String
Private dateStartChar_ As String
Private dateEndChar_ As String

Private obsvrId_ As String
'

Public Sub Initialize( _
    ByRef dat As GD, _
    Optional varStartChar As String = "%", _
    Optional varEndChar As String = "%", _
    Optional dateStartChar As String = "<", _
    Optional dateEndChar As String = ">")
    
    If dat.NbrOfColumns = 0 Then
        Set dat_ = CoreFactory.CreateGD("Section|Field|Detail|Description|DisplayOrder")
    Else
        Set dat_ = dat
    End If
    
    Set defaultDat_ = CoreFactory.CreateGD("Section|Field|Detail")
    
    Set substituter_ = New VariableSubstituter
    substituter_.Initialize
    
    varStartChar_ = varStartChar
    varEndChar_ = varEndChar
    dateStartChar_ = dateStartChar
    dateEndChar_ = dateEndChar
    
End Sub

Friend Sub setData(ByRef dat As GD)
    Set dat_ = dat
End Sub

Private Property Get this() As CFG
    Set this = Me
End Property

Public Property Get CFG_IsProd() As Boolean
    Dim ret As Boolean
    Select Case UCase(this.Value("Environment", "ENV"))
    Case "PRD", "PROD", "PRODUCTION"
        ret = True
    Case Else
        ret = False
    End Select
    CFG_IsProd = ret
End Property

Public Sub CFG_AddDefaultItem( _
    ByVal section As String, _
    ByVal Field As String, _
    ByVal val As Variant)
    
    If this.FieldExists(section, Field) = False Then
        defaultDat_.AppendStringRow section & "|" & Field & "|" & val
    End If
    
End Sub

Public Property Get CFG_FieldExists( _
    ByVal section As String, _
    ByVal Field As String) As Boolean
    
    Dim ret As Boolean
    
    Dim allCfg As GD
    Set allCfg = this.data
    
    Dim fltr As DF
    allCfg.AddFilterCriteria "Section", Equal, section, fltr
    allCfg.AddFilterCriteria "Field", Equal, Field, fltr
        
    ret = (allCfg.Filtered(fltr).NbrOfRows > 1)
    
    CFG_FieldExists = ret
    
End Property

Public Property Get CFG_Value( _
    ByVal section As String, _
    ByVal Field As String) As String
    
    Dim ret As String: ret = ""
    
    If section = "Environment" Then
        ret = Environ(Field)
    End If
    
    If ret = "" Then
    
        Dim allCfg As GD
        Set allCfg = this.data
        
        Dim fltr As DF
        allCfg.AddFilterCriteria "Section", Equal, section, fltr
        allCfg.AddFilterCriteria "Field", Equal, Field, fltr
        
        Dim fltrd As GD
        Set fltrd = allCfg.Filtered(fltr)
        
        If fltrd.NbrOfRows = 2 Then
            ret = fltrd.FieldValue("Detail", 2)
            ret = substituteEnvVars(ret)
        Else
            Err.Raise ExceptionCode.InvalidObject, "CFG_Impl.Value", _
                "Section and Field do not uniquely identify a record in the config. " & _
                "Section='" & section & "', Field='" & Field & "'"
        End If
    
    End If
    
    CFG_Value = ret
    
End Property

Private Function substituteEnvVars( _
    ByVal origFull As String) As String

    ' Substitute env vars
    
    Dim ret As String
    ret = origFull
    
    Dim variables As GD
    Set variables = extractVariablesFromString(ret, varStartChar_, varEndChar_)

    Dim r As Long
    For r = variables.StartIndex To variables.NbrOfRows
        
        Dim varName As String
        varName = variables(1, r)

        ret = substituter_.Substitute( _
            ret, _
            varStartChar_ & varName & varEndChar_, _
            Me.CFG_Value("Environment", varName))
    Next r
    
    substituteEnvVars = ret
    
End Function

Private Function extractVariablesFromString( _
    ByVal detail As String, _
    startChar As String, _
    endChar As String) As GD

    Dim ret As GD
    Set ret = CoreFactory.CreateGD

    ' Eg. detail = %APPROOT%%APPLICATION%\Feed\ag.xls
    ' 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34
    ' % A P P R O O T %  %  A  P  P  L  I  C  A  T  I  O  N  %  \  F  e  e  d  \  a  g  .  x  l  s


    Dim strt As Long
    strt = InStr(1, detail, startChar)
    Dim fin As Long

    While strt > 0
        fin = InStr(strt + 1, detail, endChar)

        If fin < strt Then
            Err.Raise ExceptionCode.InvalidResult, _
                "CFG_Impl.extractVariablesFromString", _
                "Terminating variable char not found, Detail='" & detail & "', " & _
                    "Beginning char='" & startChar & "', " & _
                    "Terminating char='" & endChar & "'"
        End If
        ret.AppendStringRow Mid(detail, strt + 1, fin - strt - 1)

        strt = InStr(fin + 1, detail, startChar)
    Wend

    Set extractVariablesFromString = ret

End Function

Public Property Get CFG_DatedValue( _
    ByVal section As String, _
    ByVal Field As String, _
    ByVal dt As Date) As String
    
    Dim ret As String
    ret = Me.CFG_Value(section, Field)
    ret = substituter_.SubstituteDate(ret, dt)
    
    CFG_DatedValue = ret
    
End Property

Public Property Get CFG_Data() As GD  ' {Section|Field|Detail|Description|DisplayOrder}
    Dim ret As GD
    Set ret = dat_.Clone
    
    Dim r As Long
    For r = defaultDat_.StartIndex To defaultDat_.NbrOfRows
        
        Dim section As String
        Dim Field As String
        Dim val As Variant
        
        section = defaultDat_.FieldValue("Section", r)
        Field = defaultDat_.FieldValue("Field", r)
        
        If mainConfigItemExists(section, Field) = False Then
            val = defaultDat_.FieldValue("Detail", r)
            'ret.AppendStringRow section & "|" & field & "|" & val & "||"
            Dim newRow As Long: newRow = 0
            newRow = ret.NbrOfRows + 1
            ret.ExpandRows newRow
            
            ret.FieldValue("Section", newRow) = section
            ret.FieldValue("Field", newRow) = Field
            ret.FieldValue("Detail", newRow) = val
        End If
    Next r
    
    Set CFG_Data = ret
End Property

Public Function CFG_ExpandVariable( _
    ByVal origStr As String, _
    ByVal var As String, _
    ByVal val As String) As String
    
    Dim ret As String
    ret = substituter_.Substitute(origStr, var, val)
    CFG_ExpandVariable = ret
    
End Function

Private Property Get mainConfigItemExists( _
    ByVal section As String, _
    ByVal Field As String) As Boolean
    
    Dim ret As Boolean
    
    Dim fltr As DF
    dat_.AddFilterCriteria "Section", Equal, section, fltr
    dat_.AddFilterCriteria "Field", Equal, Field, fltr
    
    ret = (dat_.Filtered(fltr).NbrOfRows > 1)
    
    mainConfigItemExists = ret

End Property

