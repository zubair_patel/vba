VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DS_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements DS   ' DataService
Implements OBSVBL


Private def_ As DSD
Private fieldsAndTypes_ As GD  ' Header plus their types
Private dbh_ As DBH

Private obs_ As OBSVBL

' State of loaded cache for efficiency
Private cachedData_ As GD
Private cacheRowsCount_ As Long
Private cacheFilter_ As DF
Private cacheOrderByString_ As String
Private cacheSortAscending_ As Boolean
Private cacheSelectFields_ As GD
        

Const FIELDSROWINDEX = 1 ' Row 1 in fieldsAndTypes_ gives field names
Const TYPESROWINDEX = 2 ' Row 2 in fieldsAndTypes_ gives field types
'

' With ADO recordsets, it seems that Table Key information doesn't
' get updated. So, this class requires the user to pass in that info
Public Sub Initialize( _
    ByRef def As DSD, _
    ByRef dbHelper As DBH)
        
    Set def_ = def
    Set dbh_ = dbHelper
    Set fieldsAndTypes_ = dbh_.FieldTypes(def_.Table, def.Fields)
    
    Set obs_ = CoreFactory.CreateObservableImpl(def_.Table, Me)
    
    Me.DS_ClearCache
End Sub

Public Sub Terminate()
    Set obs_ = Nothing
End Sub

Public Sub Class_Terminate()
    Me.Terminate
        
    If Err.Number <> 0 Then
    AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "DS_Impl.Class_Terminate"
    End If
    
End Sub

' DS

Public Property Get DS_Definition() As DSD
    Dim ret As DSD
    Set ret = def_
    Set DS_Definition = ret
End Property

Public Property Set DS_Definition(ByRef def As DSD)
    Set def_ = def
End Property

Public Sub DS_ClearCache()
    Set cachedData_ = Nothing
End Sub

Private Sub SetCache( _
    ByRef cache As GD, _
    ByRef fltr As DF, _
    ByVal orderBy As String, _
    ByVal sortAscending As Boolean, _
    ByVal count As Long, _
    ByRef selectFields As GD)
    
    If cache.NbrOfRows > 0 Then
        Set cachedData_ = cache
    Else
        Set cachedData_ = def_.Fields
    End If
    
    cacheRowsCount_ = cachedData_.NbrOfRows - 1
    Set fltr = cacheFilter_
    cacheOrderByString_ = orderBy
    cacheSortAscending_ = sortAscending
    cacheRowsCount_ = count
    Set cacheSelectFields_ = selectFields
    
    Me.OBSVBL_Notify
End Sub

Public Sub DS_Synchronize(ByRef toSynch As GD)
    dbh_.Conn.BeginTrans
    dbh_.Run "Delete From " & def_.Table, False
    On Error GoTo ROLLBACK
    Me.DS_Save toSynch
    On Error GoTo 0
    dbh_.Conn.CommitTrans
    Me.DS_ClearCache
    
    Exit Sub
    
ROLLBACK:
    dbh_.Conn.RollbackTrans
    AppEx.RecordException
    AppEx.RethrowRecorded
End Sub

Public Sub DS_Save( _
    ByRef toSave As GD, _
    Optional ByVal updateIfInsertFails As Boolean = True)

    
    Dim sqlInsert As String
    sqlInsert = insertPreString()
    
    Dim sql As String
    
    Dim r As Long
    For r = 2 To toSave.NbrOfRows
    
        ' Lets attempt an Insert first
        
        sql = sqlInsert & insertValuesString(toSave, r)
        
        On Error Resume Next
        dbh_.Run sql, False
        
        If Err.Number <> 0 Then
        
            If updateIfInsertFails = True Then
                ' Insert failed so lets try Update
                On Error GoTo 0
                sql = updateValuesString(toSave, r)
                dbh_.Run sql, False
            Else
                AppEx.RecordException
                On Error GoTo 0
                AppEx.RethrowRecorded
            End If
            
        End If
        
    Next r
    
    Me.DS_ClearCache
    
End Sub

Public Sub DS_Delete(ByRef toDelete As GD)
    Dim r As Long
    
    Dim delSql As String
    delSql = "Delete from " & def_.Table & " "
    
    For r = 2 To toDelete.NbrOfRows
        dbh_.Run delSql & whereString(toDelete, r), False
    Next r
    
    Me.DS_ClearCache
    
End Sub

Private Property Get shouldReloadCache( _
        ByRef fltr As DF, _
        ByVal orderBy As String, _
        ByVal sortAscending As Boolean, _
        ByVal count As Long, _
        ByRef selectFields As GD) As Boolean

    Dim reload As Boolean
    reload = False
    
    If cachedData_ Is Nothing Then
        reload = True
    End If
    
    ' if filter is different from cacheFilter then don't load cache
    If Not (fltr Is Nothing) Then
        If fltr.Equals(cacheFilter_) = False Then
            reload = True
        End If
    End If
    
    ' if orderBy is different from cacheOrderBy then don't load cache
    If orderBy <> "" Then
        If orderBy <> cacheOrderByString_ Then
            reload = True
        End If
    End If
    
    ' sort ascending
    If sortAscending <> True Then
        If sortAscending <> cacheSortAscending_ Then
            reload = True
        End If
    End If
    
    ' count
    If count <> -1 Then
        If count <> cacheRowsCount_ Then
            reload = True
        End If
    End If
    
    ' selectFields
    If Not (selectFields Is Nothing) Then
        If selectFields.Equal(cacheSelectFields_) Then
            reload = True
        End If
    End If
    
    shouldReloadCache = reload

End Property


Public Function DS_Load( _
        Optional ByRef fltr As DF, _
        Optional ByVal orderBy As String = "", _
        Optional ByVal sortAscending As Boolean = True, _
        Optional ByVal count As Long = -1, _
        Optional ByRef selectFields As GD) As GD    ' Contains Table Header
        
    If shouldReloadCache(fltr, orderBy, sortAscending, count, selectFields) = True Then
    
        If selectFields Is Nothing Then
            Set selectFields = fieldsAndTypes_.HeaderRow
        End If
    
        Dim selectClause As DBClause
        Set selectClause = DataLoadFactory.CreateSelectClause( _
            selectFields, count)
        
        Dim sql As String
        
        Dim SelectString As String
        SelectString = selectClause.GenerateString
        
        sql = SelectString
        sql = sql & "From " & def_.Table
        
        If Not (fltr Is Nothing) Then
            
            Dim whereClause As DBClause
            Set whereClause = DataLoadFactory.CreateWhereClause( _
                selectFields, fltr, def_.Table, dbh_)
                
            sql = sql & " " & whereClause.GenerateString
        End If
        
        If orderBy = "" Then
            orderBy = def_.DefaultOrderBy
        End If
        
        If orderBy <> "" Then
            orderBy = "[" & orderBy & "]"
            If InStr(1, SelectString, orderBy) > 0 Then
                sql = sql & " Order By " & orderBy
            End If
        End If
        
        If sortAscending = False Then
            sql = sql & " Desc"
        End If
        
        SetCache dbh_.RunGD(sql, False), fltr, orderBy, sortAscending, count, selectFields
        
    End If
        
    Set DS_Load = cachedData_
End Function


' Internal stuff

Private Property Get updateValuesString( _
    ByRef dat As GD, _
    ByVal datRow As Long) As String
    
    Dim updateClause As DBClause
    Set updateClause = DataLoadFactory.CreateUpdateClause( _
        fieldsAndTypes_.EntireRow(FIELDSROWINDEX), _
        fieldsAndTypes_.EntireRow(TYPESROWINDEX), _
        dat.EntireRow(datRow), _
        def_.Table, _
        dbh_)
        
    Dim ret As String
    ret = updateClause.GenerateString
    Set updateClause = Nothing
    
    ret = ret & whereString(dat, datRow)
    
    updateValuesString = ret
End Property

Private Property Get whereString( _
    ByRef dat As GD, _
    ByVal datRow As Long) As String

    Dim ret As String

    Dim ks As GD
    Set ks = def_.Keys
    
    Dim filter As DF
    Dim c As Long
    For c = 1 To ks.NbrOfColumns
        Dim keyName As String: keyName = ""
        keyName = ks(c, 1)
        ks.AddFilterCriteria keyName, Equal, dat.FieldValue(keyName, datRow), filter
    Next c

    Dim whereClause As DBClause
    Set whereClause = DataLoadFactory.CreateWhereClause( _
        def_.Keys, _
        filter, _
        def_.Table, _
        dbh_)
        
    ret = whereClause.GenerateString
    
    Set filter = Nothing
    Set whereClause = Nothing
    
    whereString = ret
End Property

Private Property Get insertPreString()
    Dim insertClause As DBClause
    Set insertClause = DataLoadFactory.CreateInsertClause( _
        fieldsAndTypes_.EntireRow(FIELDSROWINDEX), _
        def_.Table)
        
    Dim ret As String
    
    ret = insertClause.GenerateString
    Set insertClause = Nothing
    
    insertPreString = ret
End Property

Private Property Get insertValuesString( _
    ByRef dat As GD, _
    ByVal datRow As Long) As String

    Dim valuesClause As DBClause
    
    Set valuesClause = DataLoadFactory.CreateValuesClause( _
        fieldsAndTypes_.EntireRow(TYPESROWINDEX), _
        dat.EntireRow(datRow), _
        dbh_)

    Dim ret As String
    
    ret = valuesClause.GenerateString
    Set valuesClause = Nothing
    
    insertValuesString = ret
    
End Property

Private Sub validateData( _
    ByRef gdata As GD)

    If gdata.EntireRow(1).Equal(fieldsAndTypes_.EntireRow(1)) = False Then
        Dim msg As String
        msg = "Fields validated against='" & fieldsAndTypes_.RowAsString(1) & _
            "', Fields in data provided='" & gdata.RowAsString(1) & "'"
        Err.Raise ExceptionCode.MissingField, "DS_Generic.validateData", msg
    End If

End Sub


' OBSVBL

Public Property Get OBSVBL_Identifier() As String
    Dim ret As String
    ret = obs_.Identifier
    OBSVBL_Identifier = ret
End Property

Public Sub OBSVBL_Attach(obsrvr As OBSVR)
    obs_.Attach obsrvr
End Sub

Public Sub OBSVBL_Detach(obsrvr As OBSVR)
    obs_.Detach obsrvr
End Sub

Public Sub OBSVBL_Notify()
    obs_.Notify
End Sub

Public Property Get OBSVBL_State() As Object
    Dim ret As Object
    Set ret = obs_.State
    Set OBSVBL_State = ret
End Property












