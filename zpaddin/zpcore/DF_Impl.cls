VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DF_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private fieldIndices_ As Dictionary ' map<String fieldName, Long index>
Private allfilters_ As Dictionary   ' map<Long columnIndex, GD columnFilters{Operator|Value}>

Implements DF
'

Friend Property Get pvt_fieldIdx( _
    ByVal fieldName As String) As Long
    
    Dim ret As Long
    If fieldIndices_.Exists(fieldName) Then
        ret = fieldIndices_.Item(fieldName)
    ElseIf IsNumeric(fieldName) Then
        ret = CLng(fieldName)
    Else
        Err.Raise ExceptionCode.MissingField, "DF_Impl.fieldIdx", _
            "Couldn't get column index for field '" & fieldName & "'"
    End If
    
    pvt_fieldIdx = ret
    
End Property

Friend Property Get pvt_newColumnFilter() As GD
    Dim ret As GD
    Set ret = CoreFactory.CreateGD("Operator|Value")
    Set pvt_newColumnFilter = ret
End Property

Public Sub Initialize( _
    Optional ByRef Fields As GD)
    
    Set fieldIndices_ = New Dictionary
    Set allfilters_ = New Dictionary
    
    If Not (Fields Is Nothing) Then
        Me.DF_SetFields Fields
    End If
    
End Sub

Public Sub DF_SetFields( _
    ByRef Fields As GD)

    Dim c As Long
    For c = 1 To Fields.NbrOfColumns
        fieldIndices_.Add Fields(c, 1), c
    Next c
    
End Sub

Public Function DF_GetFields() As GD
    Dim ret As GD
    Set ret = CoreFactory.CreateGD
    ret.HasHeader = TristateTrue
    
    Dim v As Variant
    For Each v In fieldIndices_
        Dim clm As Long
        clm = fieldIndices_.Item(v)
        
        ret.InsertExpand clm, 1, CStr(v)
        
    Next
    
    Set DF_GetFields = ret
    
End Function


Public Property Get DF_AllCriteria() As Dictionary
    Dim ret As Dictionary
    Set ret = allfilters_
    Set DF_AllCriteria = ret
End Property

Public Property Get DF_ColumnCriteria( _
    ByVal clmIndex As Long) As GD ' {Operator|Value}
    
    Dim ret As GD
    If allfilters_.Exists(clmIndex) Then
        Set ret = allfilters_.Item(clmIndex)
    Else
        Set ret = pvt_newColumnFilter
    End If
    
    Set DF_ColumnCriteria = ret
    
End Property

Public Property Get DF_FieldCriteria( _
    ByVal fieldName As String) As GD ' {Operator|Value}
    
    Dim ret As GD
    Set ret = Me.DF_ColumnCriteria(pvt_fieldIdx(fieldName))
    Set DF_FieldCriteria = ret
    
End Property

Public Sub DF_RemoveColumnCriteria( _
    ByVal clmIndex As Long)
    
    If allfilters_.Exists(clmIndex) Then
        Set allfilters_.Item(clmIndex) = Nothing
        allfilters_.Remove clmIndex
    End If
End Sub

Public Sub DF_RemoveFieldCriteria( _
    ByVal fieldName As String)
    
    Me.DF_RemoveColumnCriteria Me.pvt_fieldIdx(fieldName)
    
End Sub

Public Sub DF_ClearAllCriteria()
    allfilters_.RemoveAll
End Sub

Public Sub DF_AddColumnCriteria( _
    ByVal clmIndex As Long, _
    ByVal op As Operator, _
    ByVal val As Variant)
    
    If allfilters_.Exists(clmIndex) = False Then
        allfilters_.Add clmIndex, Me.pvt_newColumnFilter
    End If
    
    Dim criteria As GD
    Set criteria = allfilters_.Item(clmIndex)
    
    Dim rows As Long
    rows = criteria.NbrOfRows + 1
     
    criteria.ExpandRows rows
    criteria.FieldValue("Operator", rows) = op
    criteria.FieldValue("Value", rows) = val
    
End Sub

Public Sub DF_AddFieldCriteria( _
    ByVal fieldName As String, _
    ByVal op As Operator, _
    ByVal val As Variant)
    
    Me.DF_AddColumnCriteria Me.pvt_fieldIdx(fieldName), op, val
    
End Sub

Public Sub DF_AddStringCriteria( _
    ByVal clause As String, _
    Optional valType As ZPType = ZPVariant)
    
    Dim fieldName As String
    Dim opStr As String
    Dim valStr As String
    
    Dim startPos As Long
    Dim endPos As Long: endPos = 0
    Dim nbrOfCharacters As Long
    
    startPos = endPos + 1
    endPos = InStr(startPos, clause, " ")
    nbrOfCharacters = endPos - startPos
    fieldName = Mid(clause, startPos, nbrOfCharacters)
    
    startPos = endPos + 1
    endPos = InStr(startPos, clause, " ")
    nbrOfCharacters = endPos - startPos
    opStr = Mid(clause, startPos, nbrOfCharacters)
    
    startPos = endPos + 1
    endPos = InStr(startPos, clause, " ")
    nbrOfCharacters = endPos - startPos
    valStr = Mid(clause, startPos)
    
    Me.DF_AddFieldCriteria _
        fieldName, _
        Me.COMP_StringToOperator(opStr), _
        AppType.GetTypedValue(valStr, valType)
    
End Sub

Public Function DF_Equals(ByRef compareWith As DF) As Boolean

    Dim ret As Boolean
    ret = True

    If compareWith Is Nothing Then
        ret = False
        
    Else
    
        Dim thisData As Dictionary
        Set thisData = Me.DF_AllCriteria

        Dim compareData As Dictionary
        Set compareData = compareWith.AllCriteria
        
        If thisData.count <> compareData.count Then
            ret = False
            GoTo EXITFUNCTION
        End If

        Dim v As Variant
        For Each v In thisData
            If compareData.Exists(v) = False Then
                ret = False
                GoTo EXITFUNCTION
            Else
                Dim thisClmFilter As GD
                Set thisClmFilter = thisData(v)

                Dim compareClmFilter As GD
                Set compareClmFilter = compareData(v)

                If thisClmFilter.Equal(compareClmFilter) = False Then
                    ret = False
                    GoTo EXITFUNCTION
                End If
            End If
        Next v
        
    End If

EXITFUNCTION:
    DF_Equals = ret
    Exit Function
    
End Function


Public Function DF_MatchesCriteria( _
    ByVal sourceValue As Variant, _
    ByVal op As Operator, _
    ByVal filterValue As Variant) As Boolean
    
    Dim ret As Boolean
    
    Dim sourceValueStr As String
    Dim filterValueStr As String
    
    sourceValueStr = CStr(sourceValue)
    filterValueStr = CStr(filterValue)
    
    If (op = Operator.Equal And sourceValue = filterValue) Or _
        (op = Operator.NotEqual And sourceValue <> filterValue) Or _
        (op = Operator.LessThan And sourceValue < filterValue) Or _
        (op = Operator.GreaterThan And sourceValue > filterValue) Or _
        (op = Operator.BeginsWith And Left(sourceValueStr, Len(filterValueStr)) = filterValueStr) Or _
        (op = Operator.EndsWith And Right(sourceValueStr, Len(filterValueStr)) = filterValueStr) Or _
        (op = Operator.Contains And InStr(1, sourceValue, filterValueStr) <> 0) Or _
        (op = Operator.None) _
    Then
        ret = True
    Else
        ret = False
    End If
    
    DF_MatchesCriteria = ret
    
End Function

Public Sub DF_DebugPrint()

    Debug.Print "Fieldname|ClmIdx|Operator|Value"

    Dim v As Variant
    For Each v In allfilters_
        
        Dim clmFilter As GD
        Set clmFilter = allfilters_.Item(v)
        
        Dim fieldName As String
        Dim clmIdx As Long
        Dim opStr As String
        Dim val As Variant
        
        clmIdx = v
        fieldName = pvt_IdxField(clmIdx)
        
        Dim r As Long
        For r = clmFilter.StartIndex To clmFilter.NbrOfRows
            opStr = Me.COMP_OperatorToString(clmFilter.FieldValue("Operator", r))
            val = clmFilter.FieldValue("Value", r)
            
            Debug.Print fieldName & "|" & clmIdx & "|" & opStr & "|" & val
        Next r
        
    Next v
    
End Sub


Private Property Get pvt_IdxField( _
    ByVal idx As Long) As String

    Dim ret As String
    ret = ""
    
    Dim v As Variant
    For Each v In fieldIndices_
        If fieldIndices_(v) = idx Then
            ret = v
            Exit For
        End If
    Next

    pvt_IdxField = ret
End Property

Public Function COMP_OperatorToString( _
    ByVal op As Operator) As String
    
    Dim ret As String
    
    Select Case op
    Case None
        ret = "None"
    Case Equal
        ret = "Equal"
    Case NotEqual
        ret = "NotEqual"
    Case LessThan
        ret = "LessThan"
    Case GreaterThan
        ret = "GreaterThan"
    Case BeginsWith
        ret = "BeginsWith"
    Case EndsWith
        ret = "EndsWith"
    Case Contains
        ret = "Contains"
    End Select
    
    COMP_OperatorToString = ret
End Function

Public Function COMP_StringToOperator( _
    ByVal strOp As String) As Operator
    
    Dim ret As Operator
    
    Select Case UCase(strOp)
    Case "EQUAL", "==", "="
        ret = Equal
    Case "NOTEQUAL", "<>", "!="
        ret = NotEqual
    Case "LESSTHAN", "<"
        ret = LessThan
    Case "GREATERTHAN", ">"
        ret = GreaterThan
    Case "BEGINSWITH", "^"
        ret = BeginsWith
    Case "ENDSWITH", "$"
        ret = EndsWith
    Case "CONTAINS", "*"
        ret = Contains
    
    Case Else
        ret = None
    End Select
    
    COMP_StringToOperator = ret
    
End Function












