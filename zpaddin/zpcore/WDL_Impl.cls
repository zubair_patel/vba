VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WDL_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements WDL ' Interface WebDownload

' Ref Microsoft WinHTTP Services

Public Sub WDL_Download( _
    ByVal sourceUrl As String, _
    ByVal destinationFile As String)

    On Error GoTo ERRHANDLER
    
    Dim WinHttpReq As Object
    Set WinHttpReq = CreateObject("Microsoft.XMLHTTP")
    WinHttpReq.Open "GET", sourceUrl, False
    WinHttpReq.Send
    
    Dim oStream As Object
    
    Dim srcUrlCopy As String
    srcUrlCopy = WinHttpReq.ResponseBody
    
    If WinHttpReq.Status = 200 Then
        Set oStream = CreateObject("ADODB.Stream")
        oStream.Open
        oStream.Type = 1
        oStream.Write WinHttpReq.ResponseBody
        oStream.SaveToFile destinationFile, 2  'adSaveCreateOverWrite
        oStream.Close
    End If
    
    Exit Sub
    
ERRHANDLER:
    'WinHttpReq.Close
    
    Dim desc As String
    Dim nbr As Long
    desc = Err.Description
    nbr = Err.Number
    
    On Error Resume Next
    oStream.Close
    On Error GoTo 0
    Err.Raise _
        ExceptionCode.GeneralSystem, _
        "WDL_Impl.Download", _
        desc
    
End Sub

