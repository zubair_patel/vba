VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "LG_Plus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements LG

Private infoSheet_ As String
Private warningSheet_ As String
Private debugSheet_ As String
Private errorSheet_ As String

Private infoStart_ As String
Private warningStart_ As String
Private debugStart_ As String
Private errorStart_ As String

Private logDebug_ As Boolean
Private pauseDebug_ As Boolean
Private forceDebug_ As Boolean
Private debugList_ As Dictionary

Private infoCsr_ As CSR
Private warningCsr_ As CSR
Private debugCsr_ As CSR
Private errorCsr_ As CSR

' We use these to avoid publishing repeated warnings/errors
Private cachedWarnings_ As Dictionary
Private cachedErrors_ As Dictionary

Private showErrorsOnce_ As Boolean
Private showWarningsOnce_ As Boolean

Private logFile_ As Scripting.TextStream
'

Public Sub Initialize( _
    ByRef infoCsr As CSR, _
    ByRef warningCsr As CSR, _
    ByRef errorCsr As CSR, _
    ByRef debugCsr As CSR, _
    ByVal filePath As String, _
    Optional showErrorsOnce As Boolean = False, _
    Optional showWarningsOnce As Boolean = False)
        
    Dim fso As New FileSystemObject
    Set logFile_ = fso.OpenTextFile(filePath, ForAppending, True)
    
    
    infoStart_ = infoCsr.RefNameOrAddress
    warningStart_ = warningCsr.RefNameOrAddress
    debugStart_ = debugCsr.RefNameOrAddress
    errorStart_ = errorCsr.RefNameOrAddress
    
    Set infoCsr_ = infoCsr
    Set warningCsr_ = warningCsr
    Set debugCsr_ = debugCsr
    Set errorCsr_ = errorCsr
    
    pauseDebug_ = False
    forceDebug_ = False
    
    Set debugList_ = New Dictionary
    
    showErrorsOnce_ = showErrorsOnce
    showWarningsOnce_ = showWarningsOnce

    Set cachedWarnings_ = New Dictionary
    Set cachedErrors_ = New Dictionary
    
End Sub

Private Sub Class_Terminate()
    Me.LG_CloseLog
End Sub

Public Sub LG_CloseLog()
    If Not (logFile_ Is Nothing) Then
        logFile_.Close
        Set logFile_ = Nothing
    End If
    
    Set logFile_ = Nothing
    Set debugList_ = Nothing
    Set infoCsr_ = Nothing
    Set warningCsr_ = Nothing
    Set errorCsr_ = Nothing
    Set debugCsr_ = Nothing
    Set cachedErrors_ = Nothing
    Set cachedWarnings_ = Nothing
    
End Sub

Public Sub LG_AddToDebugList( _
    ByVal source As String)
    If debugList_.Exists(source) = False Then
        debugList_.Add source, True
    End If
End Sub

Private Property Get logDebug(ByVal source As String) As Boolean

    Dim ret As Boolean
    
    If forceDebug_ = True Then
        ret = True
    ElseIf pauseDebug_ = True Then
        ret = False
    ElseIf Environ("ENV") = "PRD" Then
        ret = False
    Else
        ' We have not forced debugging
        ' nor have we paused debugging
        ' and we are not in a PRD env
        
        ret = False
        
        Dim v As Variant
        For Each v In debugList_
            Dim listItem As String
            listItem = LCase(CStr(v))
            
            If InStr(1, LCase(source), listItem) <> 0 Then
                ret = True
            End If
        Next
    
    End If
    
    logDebug = ret
    
End Property


Public Sub LG_PauseDebug()
    If forceDebug_ = False Then
        pauseDebug_ = True
    End If
End Sub

Public Sub LG_ResumeDebug()
    pauseDebug_ = False
End Sub

Public Sub LG_ForceDebug()
    pauseDebug_ = False
    forceDebug_ = True
End Sub

Public Sub LG_ResetDebug()
    forceDebug_ = False
    pauseDebug_ = False
End Sub

Public Property Get LG_Console(lgType As LogType) As Worksheet
    
    Dim ret As Worksheet
    
    Select Case lgType
    Case LogType.LG_DEBUG
        Set ret = debugCsr_.Sheet
    Case LogType.LG_ERROR
        Set ret = errorCsr_.Sheet
    Case LogType.LG_INFO
        Set ret = infoCsr_.Sheet
    Case LogType.LG_WARNING
        Set ret = warningCsr_.Sheet
    End Select
    
    Set LG_Console = ret
        
End Property

Public Sub LG_Clear()
    
    infoCsr_.LocateNameData infoStart_
    AppGui.Clear infoCsr_
    
    warningCsr_.LocateNameData warningStart_
    AppGui.Clear warningCsr_
    
    errorCsr_.LocateNameData errorStart_
    AppGui.Clear errorCsr_
    
    debugCsr_.LocateNameData debugStart_
    AppGui.Clear debugCsr_

    cachedErrors_.RemoveAll
    cachedWarnings_.RemoveAll
    
End Sub

Public Sub LG_Log(ByVal s As String)
    
    If IsErrorMessage(s) = True Then
        LG_LogE Right(s, Len(s) - Len("ERROR: "))
    ElseIf IsWarningMessage(s) = True Then
        LG_LogW Right(s, Len(s) - Len("WARNING: "))
    ElseIf IsInfoMessage(s) = True Then
        LG_LogI Right(s, Len(s) - Len("INFO: "))
    Else
        log s, infoCsr_, GREEN_COLOR
    End If
    
End Sub

Public Sub LG_LogI(ByVal s As String)
    log "INFO: " & s, infoCsr_, GREEN_COLOR
End Sub

Public Sub LG_LogW(ByVal s As String)
    
    If cachedWarnings_.Exists(s) = False Then
        
        log "WARNING: " & s, warningCsr_, ORANGE_COLOR
        
        If showWarningsOnce_ = True Then
            cachedWarnings_.Add s, Nothing
        End If
        
    End If
    
End Sub

Public Sub LG_LogE(ByVal s As String)
    
    If cachedErrors_.Exists(s) = False Then
    
        log "ERROR: " & s, errorCsr_, RED_COLOR
        
        If showErrorsOnce_ = True Then
            cachedErrors_.Add s, Nothing
        End If
        
    End If
    
End Sub

Public Sub LG_LogD(s As String, source As String)
    If logDebug(source) = True Then
        log "DEBUG " & source & "(): " & s, debugCsr_, BLUE_COLOR
    End If
End Sub

Private Sub log(s As String, ByRef crsr As CSR, TextColor As Long)

    Dim timestampPrefix As String
    timestampPrefix = Format(Now, "ddmmmyy hh:mm:ss") & " : "
    
    If Not (logFile_ Is Nothing) Then
        logFile_.WriteLine timestampPrefix & s
    End If

    Dim origScreenUpdating As Boolean
    origScreenUpdating = Application.ScreenUpdating
    
    If crsr.Sheet.Name = ActiveSheet.Name Then
        ' This will cause the log window to scroll
        Application.ScreenUpdating = True
        crsr.GetRelativeRange(0, 1).Select
    Else
        Application.ScreenUpdating = False
    End If
    
    Dim Remaining As String: Remaining = timestampPrefix & s
    
    ' We break the msg down to max 255 chars due to xl limitation
    While Remaining <> indent
        
        s = Left(Remaining, maxCharactersPerLine(""))
        Remaining = indent & Trim(Mid(Remaining, maxCharactersPerLine("") + 1))
        
        crsr.Ref.Value2 = s
        crsr.Ref.Font.ColorIndex = TextColor
        crsr.Ref.WrapText = False ' Need to reset Wrap in case msg contains a linefeed
        
        crsr.Down
    
    Wend
    
    Application.ScreenUpdating = origScreenUpdating
    
End Sub

Public Sub LG_LogProgress() ' Used for showing progress
    
    Dim Cursor As CSR
    Set Cursor = infoCsr_
    
    On Error Resume Next
    Cursor.Up
    On Error GoTo 0
    
    Dim logmsgprefix As String
    logmsgprefix = "DDMmmYY HH:MM:SS :  "
    
    Dim s As String
    s = Mid(Cursor.Ref, Len(logmsgprefix))
    
    If Left(s, Len("INFO")) = "INFO" Or _
       Left(s, Len(" . ")) = " . " Then
        
        's = Mid(csr.ref, Len(logmsgprefix)) & " . "
        'log s, csr, GREEN_COLOR
        
        Cursor.Ref.Value2 = ""
        s = s & " . "
        log s, Cursor, GREEN_COLOR
        
        'csr.down
    Else
        Cursor.Down
        log " . ", Cursor, GREEN_COLOR
    End If
    
End Sub

Private Property Get indent() As String
    indent = "        "
End Property
Private Property Get maxCharactersPerLine(fixedPrefix As String) As Long
    maxCharactersPerLine = 255 - Len(fixedPrefix)
End Property

Private Property Get IsInfoMessage(s As String) As Boolean
    IsInfoMessage = (Left(s, 4) = "INFO")
End Property

Private Property Get IsErrorMessage(s As String) As Boolean
    IsErrorMessage = (Left(s, 5) = "ERROR")
End Property

Private Property Get IsWarningMessage(s As String) As Boolean
    IsWarningMessage = (Left(s, 7) = "WARNING")
End Property

Private Property Get IsDebugMessage(s As String) As Boolean
    IsDebugMessage = (Left(s, 5) = "DEBUG")
End Property





















