VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "EXCPT_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements EXCPT ' Implements Exception

Private recordedErrNbr_ As Long
Private recordedErrSrc_ As String
Private recordedErrDesc_ As String
'

Public Sub Initialize()
    recordedErrNbr_ = 0
    recordedErrSrc_ = ""
    recordedErrDesc_ = ""
End Sub

Public Sub Class_Terminate()
End Sub

Public Sub EXCPT_HandleException( _
    Optional caughtLocation As String = "", _
    Optional showmsg As Boolean = True)
    
    Dim msg As String
    
    msg = Me.EXCPT_ExceptionName(Err.Number) & " (" & _
        Err.Number & ") - " & Err.Description
    
    If showmsg = True Then
        MsgBox "ERROR - " & Err.Description
    End If
    
    msg = msg & ", " & "Raised from " & Err.source & "()"
    
    If caughtLocation <> "" Then
        msg = msg & ", Caught in " & caughtLocation & "()"
    End If
    
    If AppLog Is Nothing Then
        Debug.Print "ERROR: " & msg
    Else
        AppLog.LogE msg
    End If
    
End Sub

Public Property Get EXCPT_ExceptionName(code As Long) As String
    
    Dim ret As String
    
    Select Case code
        Case ExceptionCode.FileNotFound: ret = "FileNotFound"
        Case ExceptionCode.FileAlreadyExists: ret = "FileAlreadyExists"
        Case ExceptionCode.FilePermissionDenied: ret = "FilePermissionDenied"
        Case ExceptionCode.PathNotFound: ret = "PathNotFound"
        Case ExceptionCode.NoRecordsFound: ret = "NoRecordsFound"
        Case ExceptionCode.ObjectNotSet: ret = "ObjectNotSet"
        Case ExceptionCode.AccessOdbcTableDoesntExist: ret = "AccessOdbcTableDoesntExist"
        Case ExceptionCode.AccessOdbcConstraintDoesntExist: ret = "AccessOdbcConstraintDoesntExist"
        Case ExceptionCode.AccessOdbcFieldDoesntExist: ret = "AccessOdbcFieldDoesntExist"
        Case ExceptionCode.AccessOdbcUniqueKeyIndexViolation: ret = "AccessOdbcUniqueKeyIndexViolation"
        Case ExceptionCode.AccessOdbcReferentialIntegrityViolation: ret = "AccessOdbcReferentialIntegrityViolation"
        
        Case ExceptionCode.FilePermissionDenied: ret = "FilePermissionDenied"
        Case ExceptionCode.GeneralSystem: ret = "GeneralSystemException"
        Case ExceptionCode.Argument: ret = "ArgumentException"
        Case ExceptionCode.ArgumentOutOfRange: ret = "ArgumentOutOfRangeException"
        Case ExceptionCode.ArrayTypeMismatch: ret = "ArrayTypeMismatchException"
        Case ExceptionCode.FieldAccess: ret = "FieldAccessException"
        Case ExceptionCode.IndexOutOfRange: ret = "IndexOutOfRangeException"
        Case ExceptionCode.InvalidOperation: ret = "InvalidOperationException"
        Case ExceptionCode.MissingField: ret = "MissingFieldException"
        Case ExceptionCode.NotImplemented: ret = "NotImplementedException"
        Case ExceptionCode.NotSupported: ret = "NotSupportedException"
        Case ExceptionCode.NullReference: ret = "NullReferenceException"
        Case ExceptionCode.InvalidObject: ret = "InvalidObjectException"
        Case ExceptionCode.ObjectNotInitialised: ret = "ObjectNotInitialisedException"
        Case ExceptionCode.MissingValue: ret = "MissingValueException"
        Case ExceptionCode.InvalidResult: ret = "InvalidResultException"
        Case ExceptionCode.FailedOperation: ret = "FailedOperationException"
        Case ExceptionCode.Business: ret = "BusinessException"
        Case Else: ret = "GeneralSystemException"
    End Select
    
    EXCPT_ExceptionName = ret
End Property

Public Sub EXCPT_RecordException()
    recordedErrNbr_ = Err.Number
    recordedErrSrc_ = Err.source
    recordedErrDesc_ = Err.Description
End Sub

Public Sub EXCPT_ClearRecorded()
    recordedErrNbr_ = 0
    recordedErrSrc_ = ""
    recordedErrDesc_ = ""
End Sub

Public Sub EXCPT_RethrowRecorded()
    Dim errnbr As Long
    Dim errSrc As String
    Dim errdesc As String
    
    errnbr = recordedErrNbr_
    errSrc = recordedErrSrc_
    errdesc = recordedErrDesc_
    
    Me.EXCPT_ClearRecorded
    
    On Error GoTo 0
    
    Err.Raise errnbr, errSrc, errdesc
End Sub
















