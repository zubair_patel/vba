VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "LoadFactory_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements LoadFactory
'

Public Sub Initialize()
End Sub

' DB Schema

Public Function LoadFactory_CreateDBSchema( _
    ByRef db As DBH) As DbSchema
    
    Dim ret As New DbSchema_Impl
    ret.Initialize db
    Set LoadFactory_CreateDBSchema = ret
    
End Function


Public Function LoadFactory_CreateDBAccess( _
    ByVal connString As String, _
    Optional ByVal userId As String = "", _
    Optional ByVal password As String = "") As DbAccess

    Dim ret As New DbAccess_Impl
    ret.Initialize connString, userId, password

    Set LoadFactory_CreateDBAccess = ret
End Function


' DB Clause

Public Function LoadFactory_CreateInsertClause( _
    ByRef Fields As GD, _
    ByVal tableName As String) As DBClause
    
    Dim ret As New DBClause_Insert
    ret.Initialize Fields, tableName
    
    Set LoadFactory_CreateInsertClause = ret
End Function

Public Function LoadFactory_CreateSelectClause( _
    ByRef Fields As GD, _
    Optional ByVal count As Long = -1) As DBClause
    
    Dim ret As New DBClause_Select
    ret.Initialize Fields, count
    
    Set LoadFactory_CreateSelectClause = ret
End Function

Public Function LoadFactory_CreateUpdateClause( _
    ByRef Fields As GD, _
    ByRef FieldTypes As GD, _
    ByRef values As GD, _
    ByVal tableName As String, _
    ByRef dbHelper As DBH) As DBClause
    
    Dim ret As New DBClause_Update
    ret.Initialize Fields, FieldTypes, values, tableName, dbHelper
    
    Set LoadFactory_CreateUpdateClause = ret
End Function

Public Function LoadFactory_CreateValuesClause( _
    ByRef FieldTypes As GD, _
    ByRef fieldValues As GD, _
    ByRef dbHelper As DBH) As DBClause
    
    Dim ret As New DbClause_Values1
    ret.Initialize FieldTypes, fieldValues, dbHelper
    
    Set LoadFactory_CreateValuesClause = ret
End Function

Public Function LoadFactory_CreateWhereClause( _
    ByRef Fields As GD, _
    ByRef filter As DF, _
    ByVal tblName As String, _
    ByRef db As DBH) As DBClause
    
    Dim ret As New DBClause_Where
    ret.Initialize Fields, filter, tblName, db
    
    Set LoadFactory_CreateWhereClause = ret
End Function


' DB Connection Strings

Public Function LoadFactory_CreateCSVConnStr( _
    ByVal filePath As String) As DBConnStr
    Dim ret As New DBConnStr_Csv
    ret.DBConnStr_Initialize filePath
    Set LoadFactory_CreateCSVConnStr = ret
End Function

Public Function LoadFactory_CreateXLSConnStr( _
    ByVal filePath As String) As DBConnStr
    Dim ret As New DBConnStr_Xls
    ret.DBConnStr_Initialize filePath
    Set LoadFactory_CreateXLSConnStr = ret
End Function

Public Function LoadFactory_CreateAccessConnStr( _
    ByVal filePath As String) As DBConnStr
    
    Dim ret As New DBConnStr_Access
    ret.DBConnStr_Initialize filePath
    Set LoadFactory_CreateAccessConnStr = ret
    
End Function

Public Function LoadFactory_CreateAccessOLEDB40ConnStr( _
    ByVal filePath As String) As DBConnStr
    
    Dim ret As New DBConnStr_AccessOLEDB40
    ret.DBConnStr_Initialize filePath
    Set LoadFactory_CreateAccessOLEDB40ConnStr = ret
    
End Function

Public Function LoadFactory_CreateODBCConnStr( _
    ByVal dsnName As String, _
    Optional ByVal userId As String = "", _
    Optional ByVal password As String = "") As DBConnStr
    
    Dim ret As New DBConnStr_ODBC
    ret.DBConnStr_Initialize dsnName
    ret.userId = userId
    ret.password = password
    
    Set LoadFactory_CreateODBCConnStr = ret
End Function


' DB Helper

Public Function LoadFactory_CreateDBHelper( _
    ByVal dbPath As String, _
    Optional ByVal userId As String = "", _
    Optional ByVal password As String = "", _
    Optional ByRef auditlog As LG = Nothing, _
    Optional ByVal dbType As String = "") As DBH
    
    If dbType = "" Then
        dbType = Mid(dbPath, InStrRev(dbPath, "."))
    End If
    
    Dim connStringGenerator As DBConnStr
    
    Select Case LCase(dbType)
    Case "access", ".mdb"
        Set connStringGenerator = Me.LoadFactory_CreateAccessConnStr(dbPath)
    Case "accessoledb40"
        Set connStringGenerator = Me.LoadFactory_CreateAccessOLEDB40ConnStr(dbPath)
    Case "xls", ".xls", "excel"
        Set connStringGenerator = Me.LoadFactory_CreateXLSConnStr(dbPath)
    Case "csv", ".csv"
        Set connStringGenerator = Me.LoadFactory_CreateCSVConnStr(dbPath)
    Case Else
        Set connStringGenerator = Me.LoadFactory_CreateODBCConnStr(dbPath, userId, password)
    End Select
    
    Dim ret As New DBH_Impl
    ret.Initialize connStringGenerator.ConnectionString, userId, password, auditlog
    
    Set LoadFactory_CreateDBHelper = ret
End Function


' Data Source Definitions

Public Function LoadFactory_CreateDataService( _
    ByVal tableName As String, _
    ByRef db As DBH, _
    ByRef allTablesConfig As GD) As DS
    
    Dim def As DSD
    Set def = Me.LoadFactory_CreateDataDefinition( _
        tableName, allTablesConfig)
    
    Dim ret As New DS_Impl
    ret.Initialize def, db
    
    Set LoadFactory_CreateDataService = ret
End Function

Public Function LoadFactory_CreateDSDConfig() As DSD
    
    Dim ret As New DSD_Config
    ret.Initialize
    
    Set LoadFactory_CreateDSDConfig = ret
End Function

Public Function LoadFactory_CreateDataDefinition( _
    ByVal tableName As String, _
    ByRef allTablesConfig As GD) As DSD
    
    Dim ret As New DSD_Generic
    ret.Initialize tableName, allTablesConfig
    
    Set LoadFactory_CreateDataDefinition = ret
    
End Function

Public Function LoadFactory_CreateDBAccessImpl( _
    ByVal connString As String, _
    Optional ByVal userId As String = "", _
    Optional ByVal password As String = "") As DbAccess

    Dim ret As New DbAccess_Impl
    ret.Initialize connString, userId, password
    
    Set LoadFactory_CreateDBAccessImpl = ret
End Function
    
    
' Query Creator
    
Public Function LoadFactory_CreateQuery( _
    ByVal qryName As String, _
    ByRef db As DBH) As Query
    
    Dim ret As New Query_Impl
    ret.Initialize qryName, db
    Set LoadFactory_CreateQuery = ret
    
End Function



' File Feeds

Public Function LoadFactory_CreateFeedActiveRange( _
    ByVal workbookPath As String, _
    ByVal sheetName As String) As Feed

    Dim ret As New Feed_ExcelActiveRange
    ret.Initialize workbookPath, sheetName
    
    Set LoadFactory_CreateFeedActiveRange = ret
End Function

Public Function LoadFactory_CreateFeedImpl( _
    ByVal tableName As String, _
    ByRef dbHelper As DBH, _
    ByRef selectClause As DBClause, _
    ByRef whereClause As DBClause) As Feed
    
    Dim ret As New Feed_Impl
    ret.Initialize tableName, dbHelper, selectClause, whereClause
    
End Function

Public Function LoadFactory_CreateFeedTrimData( _
    ByVal workbookPath As String, _
    ByVal sheetName As String) As Feed
    
    Dim ret As New Feed_TrimData
    ret.Initialize workbookPath, sheetName
    Set LoadFactory_CreateFeedTrimData = ret
End Function




