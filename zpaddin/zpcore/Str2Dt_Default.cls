VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Str2Dt_Default"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Str2Dt ' Interface StringToDate Convertor
'


Public Sub Class_Initialize()
End Sub

Public Sub Initialize()
End Sub

Public Sub Class_Terminate()
End Sub

Public Function Str2Dt_Convert(s As String) As Date
    Dim ret As Date
    ret = CDate(s)
        
    Str2Dt_Convert = ret
End Function

