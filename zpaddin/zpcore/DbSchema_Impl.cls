VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DbSchema_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements DbSchema

Private db_ As DBH
Private accessRoot_ As String
Private accessFile_ As String
'

Public Sub Initialize( _
    ByRef db As DBH)
    Set db_ = db
End Sub

Public Sub DbSchema_DropTable( _
    ByVal tableName As String)
    db_.Run "DROP TABLE " & tableName, True
End Sub

Public Sub DbSchema_AddFieldDefinition( _
    ByVal fldName As String, _
    ByVal fldType As ZPType, _
    Optional ByVal fldSize As Long = 50, _
    Optional ByVal isKey As Boolean = False, _
    Optional ByVal isRqd As Boolean = False, _
    Optional ByRef inOutDefn As GD = Nothing)
    
    'inOutDefn-> {Field|FieldType|FieldSize|IsKeyField|IsRequired}
    If inOutDefn Is Nothing Then
        Set inOutDefn = CoreFactory.CreateGD("Field|FieldType|FieldSize|IsKeyField|IsRequired|DefaultValue")
        inOutDefn.FieldType("FieldType") = ZPInteger
        inOutDefn.FieldType("IsKeyField") = ZPBool
        inOutDefn.FieldType("IsRequired") = ZPBool
    End If
    
    inOutDefn.AppendStringRow fldName & "|" & fldType & "|" & fldSize & "|" & _
        isKey & "|" & isRqd
    
End Sub

Public Sub DbSchema_CreateTable( _
    ByVal tableName As String, _
    ByRef tableDef As GD, _
    Optional ByVal identityField As String = "")
    
    'tableDef -> {Field|FieldType|FieldSize|IsKeyField|IsRequired}
    
    ' http://support.microsoft.com/kb/q275561/
    
    Dim keyFields As GD ' Single row
    Set keyFields = CoreFactory.CreateGD
    
    Dim sqlCreateTable As String: sqlCreateTable = ""
    If identityField <> "" Then
        sqlCreateTable = identityField & " " & Me.DbSchema_GetDbType(ZPLong, , True) & ", "
        keyFields.InsertExpand keyFields.NbrOfColumns + 1, 1, identityField
    End If
        
    Dim r As Long
    For r = tableDef.StartIndex To tableDef.NbrOfRows
        Dim fldName As String: fldName = ""
        Dim fldType As String: fldType = ""
        Dim isKey As Boolean: isKey = False
        Dim isRqd As Boolean: isRqd = False
        
        fldName = "[" & tableDef.FieldValue("Field", r) & "]"
        fldType = Me.DbSchema_GetDbType( _
            tableDef.FieldValue("FieldType", r), _
            tableDef.FieldValue("FieldSize", r))
        isKey = tableDef.FieldValue("IsKeyField", r)
        isRqd = tableDef.FieldValue("IsRequired", r)
        
        If isKey Then
            keyFields.InsertExpand keyFields.NbrOfColumns + 1, 1, fldName
            isRqd = True
        End If
        
        sqlCreateTable = sqlCreateTable & fldName & " "
        sqlCreateTable = sqlCreateTable & fldType
        If isRqd = True Then
            sqlCreateTable = sqlCreateTable & " NOT NULL"
        End If
        
        sqlCreateTable = sqlCreateTable & ", "
        
    Next r
        
    If sqlCreateTable <> "" Then
        sqlCreateTable = Left(sqlCreateTable, Len(sqlCreateTable) - Len(", "))
        sqlCreateTable = "CREATE TABLE " & tableName & "(" & sqlCreateTable & ") "
    End If
    
    db_.Run sqlCreateTable, True
    
    Me.DbSchema_CreateKey tableName, keyFields
    
End Sub

Public Sub DbSchema_DeleteField( _
    ByVal tableName As String, _
    ByVal fieldName As String)
    
    Dim sql As String
    sql = "ALTER TABLE " & tableName & _
        " DROP COLUMN [" & fieldName & "] "
        
    db_.Run sql, True
    
End Sub

Public Sub DbSchema_AddField( _
    ByVal tableName As String, _
    ByVal fieldName As String, _
    ByVal typ As ZPType, _
    Optional ByVal fieldSize As Long = 50, _
    Optional ByVal isRqd As Boolean = False)
    
    Dim sql As String
    sql = "ALTER TABLE " & tableName & _
        " ADD [" & fieldName & "]" & _
        Me.DbSchema_GetDbType(typ, fieldSize)
        
    If isRqd Then
        sql = sql & " NOT NULL "
    End If
    
    db_.Run sql, True
End Sub
    
Public Sub DbSchema_DropKey( _
    ByVal tableName As String)
    
    Dim sql As String
    sql = "ALTER TABLE " & tableName & _
        "DROP PRIMARY KEY "
        
    db_.Run sql, True
End Sub

Public Sub DbSchema_CreateKey( _
    ByVal tableName As String, _
    ByVal keyFlds As GD)
    
    ' keyflds is single header row
    
    ' http://www.w3schools.com/sql/sql_primarykey.asp
    
    Dim sql As String
    sql = "ALTER TABLE " & tableName & _
        " ADD PRIMARY KEY (" & keyFlds.RowAsString(1, ",") & ") "
        
    db_.Run sql, True
    
End Sub

Public Sub DbSchema_DropIndex( _
    ByVal tableName As String, _
    ByVal indexName As String)
    
    Dim sql As String
    sql = "DROP INDEX " & indexName & _
        " ON " & tableName
        
    db_.Run sql, True
End Sub

Public Sub DbSchema_CreateIndex( _
    ByVal tableName As String, _
    ByVal indexName As String, _
    ByRef indexFields As GD, _
    ByVal dupsAllowed As Boolean)
    
    ' http://www.w3schools.com/sql/sql_create_index.asp
    
    ' indexFields -> single row of fields
    
    Dim sql As String
    sql = "CREATE"
    If dupsAllowed = False Then
        sql = sql & " UNIQUE"
    End If
    sql = sql & " INDEX " & indexName & _
        " ON " & tableName & _
        " (" & indexFields.RowAsString(1, ",") & ") "
    
    db_.Run sql, True
    
End Sub
    
Public Function DbSchema_GetTableSchema( _
    ByVal tableName As String) As GD
    
    ' http://support.microsoft.com/kb/186246#appliesto
    
    Dim ret As GD
    Set ret = CoreFactory.CreateGD( _
        "FieldOrder|Field|FieldType|FieldSize|IsRequired|DefaultValue|IsPrimayKey|IndexName|IsUniqueIndex")
        
    ret.FieldType("FieldOrder") = ZPLong
    ret.FieldType("FieldType") = ZPLong
    ret.FieldType("FieldSize") = ZPLong
    ret.FieldType("IsPrimayKey") = ZPBool
    ret.FieldType("IsUniqueIndex") = ZPBool
    ret.FieldType("IsRequired") = ZPBool
    
    Dim Conn As ADODB.Connection
    Set Conn = db_.Conn
    
    Dim rsClms As ADODB.Recordset
    Dim rsIdxs As ADODB.Recordset
    
    Dim clmsData As GD
    Dim idxsData As GD ' -> {INDEX_NAME|PRIMARY_KEY|UNIQUE|COLUMN_NAME}
    
    Set rsClms = Conn.OpenSchema(adSchemaColumns, Array(Empty, Empty, tableName))
    Set clmsData = CoreFactory.CreateGD
    clmsData.InitializeWithADORS rsClms
    rsClms.Close
    
    Set idxsData = CoreFactory.CreateGD
    Set rsIdxs = Conn.OpenSchema(adSchemaIndexes, Array(Empty, Empty, Empty, Empty, tableName))
    idxsData.InitializeWithADORS rsIdxs
    rsIdxs.Close
    
    Dim r As Long
    For r = clmsData.StartIndex To clmsData.NbrOfRows
    
        Dim schemaOrdinalPosition As Long: schemaOrdinalPosition = 0
        Dim schemaColumnName As String: schemaColumnName = ""
        Dim schemaDataType As Long: schemaDataType = -1
        Dim schemaCharacterMaximumLength As Variant: schemaCharacterMaximumLength = 0
        Dim schemaIsNullable As Boolean: schemaIsNullable = True
        Dim schemaColumnHasdefault As Boolean: schemaColumnHasdefault = False
        Dim schemaColumnDefault As Variant: schemaColumnDefault = ""
        
        Dim schemaPrimaryKey As Boolean: schemaPrimaryKey = False
        Dim schemaIndexName As String: schemaIndexName = ""
        Dim schemaUnique As Boolean: schemaUnique = False
        
        schemaOrdinalPosition = clmsData.FieldValue("ORDINAL_POSITION", r)
        schemaColumnName = clmsData.FieldValue("COLUMN_NAME", r)
        schemaDataType = clmsData.FieldValue("DATA_TYPE", r)
        schemaCharacterMaximumLength = clmsData.FieldValue("CHARACTER_MAXIMUM_LENGTH", r)
        If Len(schemaCharacterMaximumLength) = 0 Then
            schemaCharacterMaximumLength = -1
        End If
        schemaIsNullable = clmsData.FieldValue("IS_NULLABLE", r)
        schemaColumnHasdefault = clmsData.FieldValue("COLUMN_HASDEFAULT", r)
        If schemaColumnHasdefault Then
            schemaColumnDefault = clmsData.FieldValue("", r)
        End If
        
        Dim foundRow As Long: foundRow = 0
        If idxsData.FindFieldValue(schemaColumnName, "COLUMN_NAME", , foundRow) = True Then
            schemaPrimaryKey = idxsData.FieldValue("PRIMARY_KEY", foundRow)
            schemaIndexName = idxsData.FieldValue("INDEX_NAME", foundRow)
            schemaUnique = idxsData.FieldValue("UNIQUE", foundRow)
        End If
        
        ret.AppendStringRow _
            schemaOrdinalPosition & "|" & _
            schemaColumnName & "|" & _
            schemaDataType & "|" & _
            schemaCharacterMaximumLength & "|" & _
            CStr(Not (schemaIsNullable)) & "|" & _
            schemaColumnDefault & "|" & _
            schemaPrimaryKey & "|" & _
            schemaIndexName & "|" & _
            schemaUnique
            
    Next r
    
    Set DbSchema_GetTableSchema = ret
End Function

Public Function DbSchema_GetDbType( _
    ByVal typ As ZPType, _
    Optional ByVal sz As Long = 50, _
    Optional autoIncr As Boolean = False) As String
    
    'http://www.w3schools.com/ado/ado_datatypes.asp
    
    Dim ret As String
    
    If autoIncr = True Then
         ret = "AutoIncrement"
    Else
        
        Select Case typ
        Case ZPBool
            ret = "YesNo"
        Case ZPFloat
            ret = "Single"
        Case ZPDouble
            ret = "Double"
        Case ZPInteger
            ret = "Integer"
        Case ZPLong
            ret = "Long"
        Case ZPString
            If sz <= 1 Or sz > 255 Then
                ret = "Memo"
            Else
                ret = "Text(" & sz & ")"
            End If
        Case ZPDate
            ret = "Date"
        End Select
    End If
    
    DbSchema_GetDbType = ret
End Function


Private Sub test()
    
    
    Dim db As DBH
    'Terminate
    'Initialize
    'Set db = r3.R3Db
    
    Dim tableDef As GD
    
    Dim tableName As String: tableName = "ZZ_T_Blah"
    Dim typ As ZPType: typ = ZPString
    Dim isRqd As Boolean: isRqd = False
    Dim txtLen As Long: txtLen = 50
    
    Dim fld1 As String: fld1 = "Working"
    Dim fld2 As String: fld2 = "Working2"
    
    Dim isKey As Boolean: isKey = False
    
    Dim t As DbSchema
    Dim tmp As New DbSchema_Impl
    tmp.Initialize db
    Set t = tmp
    t.AddFieldDefinition fld1, typ, txtLen, isKey, isRqd, tableDef
    t.AddFieldDefinition fld2, typ, txtLen, isKey, isRqd, tableDef
    
    On Error Resume Next
    t.DropTable tableName
    On Error GoTo 0
    t.CreateTable tableName, tableDef, "Id"
    
    Dim addedField As String: addedField = "addedFld"
    t.AddField tableName, addedField, ZPDate, 0, True ' not null field
    t.DeleteField tableName, addedField
    t.AddField tableName, addedField, ZPBool, , False ' null allowed
    
    Dim idxName As String: idxName = "Idx1"
    Dim idxFields As GD
    Set idxFields = CoreFactory.CreateGD(addedField)
    
    t.CreateIndex tableName, idxName, idxFields, True ' not uniq idx
    t.DropIndex tableName, idxName
    Set idxFields = CoreFactory.CreateGD(addedField & "|" & fld1)
    t.CreateIndex tableName, idxName, idxFields, False ' uniq idx
    
End Sub
