VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DBClause_Where"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements DBClause

Private filter_ As DF
Private fields_ As GD
Private tableName_ As String
Private db_ As DBH
'

Public Sub Initialize( _
    ByRef keyFields As GD, _
    ByRef filter As DF, _
    ByVal tblName As String, _
    ByRef db As DBH)
    
    Set fields_ = keyFields
    Set filter_ = filter
    
    If fields_.Equal(filter_.GetFields) = False Then
        Err.Raise ExceptionCode.InvalidObject, "DBClause_Where.Initialize", _
            "Filter Fields {" & filter_.GetFields.RowAsString(1) & _
             "} are not synchronised with clause field {" & fields_.RowAsString(1) & "}"
    End If
    
    tableName_ = tblName
    
    Set db_ = db
    
End Sub

Public Sub Class_Terminate()
    If Err.Number <> 0 Then
        AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "DBClause_Where.Class_Terminate"
    End If
End Sub

Public Function DBClause_GenerateString() As String
    Dim ret As String
    
    If fields_ Is Nothing Then
        ret = ""
    ElseIf filter_ Is Nothing Then
        ret = ""
    ElseIf filter_.AllCriteria.count = 0 Then
        ret = ""
    Else
        Dim AllFilters As Dictionary
        Set AllFilters = filter_.AllCriteria
        
        
        Dim c As Long
        For c = 1 To fields_.NbrOfColumns
            If AllFilters.Exists(c) Then
                Dim fieldFilter As GD
                Set fieldFilter = AllFilters(c)
                Dim r As Long
                
                Dim orClause As String
                orClause = ""
                
                For r = fieldFilter.StartIndex To fieldFilter.NbrOfRows
                
                    Dim op As Operator
                    op = fieldFilter.FieldValue("Operator", r)
                    
                    Dim Field As String
                    Field = fields_(c, 1)
                    
                    Dim filterValue As Variant
                    filterValue = fieldFilter.FieldValue("Value", r)
                    
                    ' NB: We 'quote' dates and strings but not numerics!
                    
                    ' TODO - Break each of the cases below into their own functions
                    
                    Dim adoTyp As ADODB.DataTypeEnum
                    adoTyp = db_.FieldTypes(tableName_, fields_).FieldValue(Field, 2)
                    
                    If db_.IsTypeText(adoTyp) Then
                        Select Case op
                        
                        Case Operator.Equal
                            orClause = orClause & "[" & Field & "]='" & filterValue & "' Or "
                        Case Operator.NotEqual
                            orClause = orClause & "[" & Field & "]<>'" & filterValue & "' Or "
                        Case Operator.BeginsWith
                            orClause = orClause & "[" & Field & "] Like '" & filterValue & "%' Or "
                        Case Operator.EndsWith
                            orClause = orClause & "[" & Field & "] Like '%" & filterValue & "' Or "
                        Case Operator.Contains
                            orClause = orClause & "[" & Field & "] Like '%" & filterValue & "%' Or "
                        End Select
                    ElseIf db_.IsTypeNumeric(adoTyp) Then
                        Select Case op
                        Case Operator.Equal
                            orClause = orClause & "[" & Field & "]=" & filterValue & " Or "
                        Case Operator.NotEqual
                            orClause = orClause & "[" & Field & "]<>" & filterValue & " Or "
                        Case Operator.GreaterThan
                            orClause = orClause & "[" & Field & "]>" & filterValue & " Or "
                        Case Operator.LessThan
                            orClause = orClause & "[" & Field & "]<" & filterValue & " Or "
                        End Select
                    ElseIf db_.IsTypeDate(adoTyp) Then
                        Select Case op
                        
                        Case Operator.Equal
                            orClause = orClause & "[" & Field & "]=#" & filterValue & "# Or "
                        Case Operator.NotEqual
                            orClause = orClause & "[" & Field & "]<>#" & filterValue & "# Or "
                        Case Operator.GreaterThan
                            orClause = orClause & "[" & Field & "]>#" & filterValue & "# Or "
                        Case Operator.LessThan
                            orClause = orClause & "[" & Field & "]<#" & filterValue & "# Or "
                        End Select
                        
                    Else ' Default - treat as string
                        Select Case op
                        
                        Case Operator.Equal
                            orClause = orClause & "[" & Field & "]='" & filterValue & "' Or "
                        Case Operator.NotEqual
                            orClause = orClause & "[" & Field & "]<>'" & filterValue & "' Or "
                        Case Operator.BeginsWith
                            orClause = orClause & "[" & Field & "] Like '" & filterValue & "%' Or "
                        Case Operator.EndsWith
                            orClause = orClause & "[" & Field & "] Like '%" & filterValue & "' Or "
                        Case Operator.Contains
                            orClause = orClause & "[" & Field & "] Like '%" & filterValue & "%' Or "
                        End Select
                    End If
                    
                Next r
                
                ' Circle with parentheses and remove trailing 'Or '
                If Len(orClause) > Len("Or ") Then
                    orClause = Left(orClause, Len(orClause) - Len("Or "))
                    orClause = "( " & orClause & ") "
                End If
                
                ' Add an ' And ' at the end for the next field
                ret = ret & orClause & "And "
                
            End If
        Next c
        
        ' Remove trailing 'And '
        If Len(ret) > Len("And ") Then
            ret = Left(ret, Len(ret) - Len("And "))
        End If
        
        Dim v As Variant
        For Each v In AllFilters
        Next
        
    End If
    
    If ret <> "" Then ret = "Where " & ret
    DBClause_GenerateString = ret
End Function
