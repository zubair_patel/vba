VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Feed_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements Feed

Private tableName_ As String
Private selectClause_ As DBClause
Private whereClause_ As DBClause
Private dbHelper_ As DBH
'


Public Sub Initialize( _
    tableName As String, _
    ByRef dbHelper As DBH, _
    ByRef selectClause As DBClause, _
    ByRef whereClause As DBClause)
    
    tableName_ = tableName
    
    Set dbHelper_ = dbHelper
    Set selectClause_ = selectClause
    Set whereClause_ = whereClause
    
    
End Sub

Public Sub Class_Terminate()
    Feed_CloseFeed
    Set selectClause_ = Nothing
    Set whereClause_ = Nothing
End Sub

Public Sub Feed_CloseFeed()
    If Not (dbHelper_ Is Nothing) Then
        dbHelper_.CloseDb
        Set dbHelper_ = Nothing
    End If
End Sub

Private Property Get dbHelper() As DBH
    Set dbHelper = dbHelper_
End Property

Public Function Feed_Load() As GD
    
    On Error GoTo ERRHANDLER
    Dim sql As String
    sql = pvt_Sql
    Set Feed_Load = dbHelper.RunGD(sql)
    
EXITFUNCTION:
    On Error GoTo 0
    Feed_CloseFeed
    Exit Function
'
ERRHANDLER:
    Dim errnbr As Long: errnbr = Err.Number
    Dim errdesc As String: errdesc = Err.Description
    MsgBox errdesc
    Feed_CloseFeed
    ' Rethrow using the RaiseError function doesn't work !
    Err.Raise errnbr, "Feed_Impl.Load", _
        errdesc & " sql='" & sql & "'"
    
End Function

Public Property Get pvt_Sql() As String
    Dim sql As String
        
    Dim SelectString As String
    SelectString = pvt_SelectString
    
    Dim fromString As String
    fromString = pvt_FromString
    
    Dim whereString As String
    whereString = pvt_WhereString
    
    sql = SelectString & fromString
    If whereString <> "" Then
        sql = sql & whereString
    End If
    
    pvt_Sql = sql
    
    'pvt_Sql = "Select * from " & pvt_FromString(sourceDate)
End Property
 
Private Property Get pvt_FromString() As String
    pvt_FromString = "From [" & tableName_ & "] "
End Property

Public Property Get pvt_SelectString() As String
    pvt_SelectString = selectClause_.GenerateString
End Property

Public Property Get pvt_WhereString() As String
    pvt_WhereString = whereClause_.GenerateString
End Property




























