VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WkClr_Blocks"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements WkClr

Private crsr_ As CSR
Private protector_ As WkProtect
Private topLeftNames_ As GD ' Single column of data
Private deleteRows_ As Boolean
'

Public Sub Initialize( _
    ByVal sheetName As String, _
    ByRef topLeftNames As GD, _
    Optional ByVal deleteRows As Boolean = True)
    
    Set topLeftNames_ = topLeftNames
    Set crsr_ = CoreFactory.CreateCursor(sheetName)
    Set protector_ = WorksheetFactory.CreateWkProtect(crsr_)
    deleteRows_ = deleteRows
    
End Sub

Public Sub Class_Terminate()
    If Err.Number <> 0 Then
        AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "WkClr_Blocks.Class_Terminate"
    End If
End Sub

Public Sub WkClr_Clear()

    On Error GoTo ERRHANDLER
    
    protector_.Mark
    protector_.ForceUnProtect
    
    If crsr_.Sheet.FilterMode = True Then
        crsr_.Sheet.ShowAllData
    End If
    
    Dim r As Long
    For r = 1 To topLeftNames_.NbrOfRows
        
        Dim topLeft As String
        topLeft = topLeftNames_(1, r)
        crsr_.LocateNameData topLeft
        
        Dim toClear As Range
        If deleteRows_ = True Then
            AppGui.Clear crsr_, , 1
            crsr_.Down
            AppGui.BlockRange(crsr_).Delete xlUp
        Else
            AppGui.Clear crsr_
        End If
        
    Next r
    
    protector_.Restore
    Exit Sub
    
ERRHANDLER:
    protector_.Restore
    Err.Raise Err.Number, Err.source, Err.Description
    
End Sub





