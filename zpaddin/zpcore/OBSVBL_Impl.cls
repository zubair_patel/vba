VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "OBSVBL_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements OBSVBL

Private observers_ As Dictionary
Private parent_ As OBSVBL
Private parentId_ As String
'

Public Sub Initialize( _
    parentId As String, _
    ByRef parent As OBSVBL)
    
    parentId_ = parentId
    Set parent_ = parent
    
    Set observers_ = New Dictionary
End Sub

Public Sub Class_Terminate()

    On Error Resume Next
    
    If observers_.count > 0 Then
        MsgBox "NOT ALL OBSERVERS ARE DETACHED"
    End If
    
    If Err.Number <> 0 Then
        AppLog.LogD _
            "Abnormal termination - " & Err.Description, _
            "OBSVBL_Impl.Class_Terminate"
    End If
End Sub

Public Property Get OBSVBL_Identifier() As String
    OBSVBL_Identifier = parentId_
End Property

Public Sub OBSVBL_Attach(obsrvr As OBSVR)
    If observers_.Exists(obsrvr.Identifier) = False Then
        AppLog.LogD "Attaching '" & obsrvr.Identifier & "' " & _
            "to '" & parent_.Identifier & "'", _
            "OBSVBL_Impl.Attach"
        'observers_.Add obsrvr.Identifier, obsrvr
    End If
End Sub

Public Sub OBSVBL_Detach(obsrvr As OBSVR)
    If observers_.Exists(obsrvr.Identifier) = True Then
        AppLog.LogD "Detaching '" & obsrvr.Identifier & "' " & _
        "from '" & parent_.Identifier & "'", _
            "OBSVBL_Impl.Detach"
        'observers_.Remove obsrvr.Identifier
    Else
        AppLog.LogW "OBSVBL_Impl.Detach() - observer '" & _
            obsrvr.Identifier & "' is not a registered observer"
    End If
End Sub

Public Sub OBSVBL_Notify()
    Dim v As Variant
    For Each v In observers_
        Dim OBSVR As OBSVR
        Set OBSVR = observers_(v)
        OBSVR.Update parent_
    Next
End Sub

Public Property Get OBSVBL_State() As Object
    Set OBSVBL_State = parent_
End Property


