VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WkFactory_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements WkFactory
'

Public Sub Initialize()
End Sub

Public Function WkFactory_CreateWkDispGD( _
    ByVal sheetName As String, _
    ByVal topLeft As String, _
    ByRef dat As GD) As WkDisp

    Dim ret As WkDisp

    Dim dic As New Dictionary
    dic.Add topLeft, dat
    
    Set ret = Me.WkFactory_CreateWkDisp(sheetName, dic)

    Set WkFactory_CreateWkDispGD = ret

End Function


Public Function WkFactory_CreateWkDisp( _
    ByVal sheetName As String, _
    ByRef data As Dictionary) As WkDisp
    
    Dim ret As New WkDisp_MultipleGDs
    ret.Initialize sheetName, data
        
    Set WkFactory_CreateWkDisp = ret
    
End Function
    

' WkProtect

Public Function WkFactory_CreateWkProtect( _
    ByRef crsr As CSR) As WkProtect
    
    Dim ret As New WkProtect_Impl
    ret.Initalize crsr
    Set WkFactory_CreateWkProtect = ret
    
End Function

' WkClr

Public Function WkFactory_CreateWkClrDeleteRows( _
    ByVal sheetName As String, _
    Optional ByVal startRow As Long = 1, _
    Optional ByVal clearButDontDeleteFirstRow As Boolean = False) As WkClr
    
    Dim ret As New WkClr_DeleteRows
    ret.Initialize sheetName, startRow, clearButDontDeleteFirstRow
    Set WkFactory_CreateWkClrDeleteRows = ret
    
End Function

Public Function WkFactory_CreateWkClrBlocks( _
    ByVal sheetName As String, _
    ByRef topLeftNames As GD, _
    Optional ByVal deleteRows As Boolean = True) As WkClr
    
    Dim ret As New WkClr_Blocks
    ret.Initialize sheetName, topLeftNames, deleteRows
    Set WkFactory_CreateWkClrBlocks = ret
    
End Function


' WkExport

Public Function WkFactory_CreateWkExportValues() As WkExport
    Dim ret As New WkExport_FormatValues
    ret.Initialize
    Set WkFactory_CreateWkExportValues = ret
End Function

Public Function WkFactory_CreateWkExportPivot() As WkExport
    Dim ret As New WkExport_PivotTable
    ret.Initialize
    Set WkFactory_CreateWkExportPivot = ret
End Function

' WkSheet

Public Function WkFactory_CreateWkSheet( _
    ByRef wks As Worksheet, _
    Optional ByRef clr As WkClr, _
    Optional ByRef disp As WkDisp, _
    Optional ByRef dataservices As Dictionary = Nothing) As WkSheet
    
    Dim ret As WkSheet
    
    If dataservices Is Nothing Then
        
        Dim simple As New WkSheet_Simple
        
        If disp Is Nothing Then
            Set disp = WkFactory_CreateWkDispGD(wks.Name, "A1", CoreFactory.CreateGD)
        End If
        
        If clr Is Nothing Then
            Set clr = WkFactory_CreateWkClrBlocks(wks.Name, CoreFactory.CreateGD, False)
        End If
        
        simple.Initialize wks, disp, clr
        Set ret = simple
        
    Else
        Dim dbTables As New WkSheet_DbTables
        dbTables.Initialize wks, dataservices
        Set ret = dbTables
    End If
    
    Set WkFactory_CreateWkSheet = ret
    
End Function





