VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CSR_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements CSR

Private start_ As String
Private sheet_ As Worksheet
Private ref_ As Range
Private markRef As Range
'


Public Sub Initialize( _
    Optional sheetName As String = "", _
    Optional start As String = "", _
    Optional dx As Long = 0, _
    Optional dy As Long = 0)
        
    Set markRef = Nothing
    Set sheet_ = Nothing
    Set ref_ = Nothing
    
    If sheetName = "" Then
        Set sheet_ = ActiveSheet
    Else
        Set sheet_ = Worksheets(sheetName)
    End If
    
    If start <> "" Then
        Me.CSR_LocateNameData start, dx, dy
        start_ = start
    End If
End Sub

Public Sub InitializeWithRef( _
    ByRef wk As Worksheet, _
    ByRef start As Range, _
    Optional dx As Long = 0, _
    Optional dy As Long = 0)
        
    Set markRef = Nothing
    Set sheet_ = Nothing
    Set ref_ = Nothing
    
    Set sheet_ = wk
    Set ref_ = start
    start_ = Me.CSR_RefNameOrAddress

    Me.CSR_Move dx, dy

End Sub


Public Sub Class_Terminate()
    Set sheet_ = Nothing
    Set ref_ = Nothing
    Set markRef = Nothing
End Sub

Public Property Get CSR_StartAddress() As String
    Dim ret As String
    ret = start_
    CSR_StartAddress = ret
End Property

Public Sub CSR_Reset()
    If start_ <> "" Then
        CSR.LocateNameData start_
    End If
End Sub

Public Property Get CSR_RefNameOrAddress() As String
    Dim ret As String: ret = ""
    
    On Error Resume Next
    
    If Not (Me.CSR_Ref.Name Is Nothing) Then
        ret = Me.CSR_Ref.Name
    End If
    
    If ret = "" Then ret = Me.CSR_Ref.Address
    
    ret = sheet_.Name & "!" & ret
    
    CSR_RefNameOrAddress = ret
    
    On Error GoTo 0
    
End Property

Function CSR_LocateNameData(Name As String, Optional dx As Long = 0, Optional dy As Long = 0) As Range
    Set Me.CSR_Ref = sheet_.Range(Name)
    Me.CSR_Move dx, dy

    Set CSR_LocateNameData = Me.CSR_Ref
End Function

Public Sub CSR_Left(Optional dx As Long = 1)
    Me.CSR_Move -dx, 0
End Sub

Public Sub CSR_Right(Optional dx As Long = 1)
    Me.CSR_Move dx, 0
End Sub

Public Sub CSR_Up(Optional dy As Long = 1)
    Me.CSR_Move 0, -dy
End Sub

Public Sub CSR_Down(Optional dy As Long = 1)
    Me.CSR_Move 0, dy
End Sub

Public Sub CSR_Move(Optional dx As Long = 0, Optional dy As Long = 0)
    
    Dim csrCol As Long
    Dim csrRow As Long

    csrCol = Me.CSR_Ref.Column + dx
    csrRow = Me.CSR_Ref.row + dy

    Set Me.CSR_Ref = sheet_.Cells(csrRow, csrCol)

End Sub

Public Function CSR_GetRelativeRange(Optional dx As Long = 0, Optional dy As Long = 0) As Range
    
    Dim csrCol As Long
    Dim csrRow As Long
    
    csrCol = Me.CSR_Ref.Column + dx
    csrRow = Me.CSR_Ref.row + dy

    Set CSR_GetRelativeRange = sheet_.Cells(csrRow, csrCol)

End Function

Public Sub CSR_Mark()
    Set markRef = Me.CSR_Ref
End Sub

Public Sub CSR_Restore()
    If Not markRef Is Nothing Then
        Set Me.CSR_Ref = markRef
    End If
End Sub

Public Property Get CSR_Ref() As Range
    Set CSR_Ref = ref_
End Property

Public Property Get CSR_Sheet() As Worksheet
    Set CSR_Sheet = sheet_
End Property

Public Property Set CSR_Ref(ByRef r As Range)
    Set ref_ = r
End Property

Public Property Set CSR_Sheet(ByRef wk As Worksheet)
    Set sheet_ = wk
End Property

