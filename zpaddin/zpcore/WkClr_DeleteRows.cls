VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WkClr_DeleteRows"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements WkClr

Private crsr_ As CSR
Private protector_ As WkProtect
Private startRow_ As Long
Private clearStartRow_ As Boolean
'

Public Sub Initialize( _
    sheetName As String, _
    Optional startRow As Long = 1, _
    Optional clearStartRowContents As Boolean = False)
    
    startRow_ = startRow
    clearStartRow_ = clearStartRowContents
    
    Set crsr_ = CoreFactory.CreateCursor(sheetName)
    Set protector_ = WorksheetFactory.CreateWkProtect(crsr_)
End Sub

Public Sub Class_Terminate()
End Sub

Public Sub WkClr_Clear()

    On Error GoTo ERRHANDLER
    
    protector_.Mark
    protector_.ForceUnProtect
    
    If crsr_.Sheet.FilterMode = True Then
        crsr_.Sheet.ShowAllData
    End If
    
    If startRow_ = 1 And clearStartRow_ = False Then
        crsr_.Sheet.Cells.Delete
    Else
    
        Dim startRange As Range
        Dim endRange As Range
        Dim deleteRange As Range
        
        If clearStartRow_ = True Then
            crsr_.Sheet.rows(startRow_).ClearContents
            Set startRange = crsr_.Sheet.rows(startRow_ + 1)
        Else
            Set startRange = crsr_.Sheet.rows(startRow_)
        End If
        
        Set endRange = crsr_.Sheet.rows(crsr_.Sheet.rows.count)
        Set deleteRange = Range(startRange, endRange)
        
        deleteRange.Delete xlUp
        
    End If
    
    protector_.Restore
    Exit Sub
    
ERRHANDLER:
    protector_.Restore
    Err.Raise Err.Number, Err.source, Err.Description
End Sub





