VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WkProtect_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements WkProtect ' Implements inteface for sheet protection

Private crsr_ As CSR
Private state_ As Boolean
'


Public Sub Initalize(ByRef crsr As CSR)
    Set crsr_ = crsr
    state_ = crsr_.Sheet.ProtectContents
End Sub

Public Sub Class_Terminate()
    On Error Resume Next
    Set crsr_ = Nothing
End Sub

Public Sub WkProtect_Mark()
    state_ = crsr_.Sheet.ProtectContents
End Sub

Public Sub WkProtect_Restore()
    If state_ = True Then
        Me.WkProtect_ForceProtect
    Else
        Me.WkProtect_ForceUnProtect
    End If
End Sub

Public Sub WkProtect_ForceProtect()
    'crsr_.sheet.Protect , , , False, , True, True, True, , , , , , True, True, True
    crsr_.Sheet.Protect _
        DrawingObjects:=False, _
        Contents:=True, _
        Scenarios:=False, _
        AllowFormattingCells:=True, _
        AllowFormattingColumns:=True, _
        AllowFormattingRows:=True, _
        AllowSorting:=True, _
        AllowFiltering:=True, _
        AllowUsingPivotTables:=True
End Sub

Public Sub WkProtect_ForceUnProtect()
    crsr_.Sheet.Unprotect
End Sub


