VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Str2Dt_ISO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements Str2Dt ' Interface StringToDate Convertor
'


Public Sub Class_Initialize()
End Sub

Public Sub Initialize()
End Sub

Public Sub Class_Terminate()
End Sub

Public Function Str2Dt_Convert(s As String) As Date
    Dim ret As Date
    
    If Len(s) = 8 Then
        ret = DateSerial( _
            Left(s, 4), _
            Mid(s, 5, 2), _
            Right(s, 2))
    Else
        Err.Raise ExceptionCode.Argument, _
            "Str2Dt_ISO.Convert", _
            "Argument '" & s & "' is not in ISO date format 'YYYYMMDD'"
    End If
 
    Str2Dt_Convert = ret
End Function


