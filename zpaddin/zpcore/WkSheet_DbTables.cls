VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WkSheet_DbTables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements WkSheet

Private dataservices_ As Dictionary ' map<String tablename, DS svc>

Private wks_ As Worksheet

Private crsr_ As CSR

Private autoRefresh_ As Boolean
'

Public Sub Class_Terminate()
    Me.WkSheet_Terminate
End Sub

Private Property Get this() As WkSheet
    Set this = Me
End Property

Public Sub Initialize( _
    ByVal wks As Worksheet, _
    ByRef dataservices As Dictionary)
    
    Set wks_ = wks
    this.AutoRefresh = False
    
    Set crsr_ = CoreFactory.CreateCursor(wks_.Name)
    
    Set dataservices_ = dataservices
           
    this.AutoRefresh = True
    
End Sub

Public Property Get WkSheet_Sheet() As Worksheet
    Dim ret As Worksheet
    Set ret = wks_
    Set WkSheet_Sheet = ret
End Property

Public Sub WkSheet_Terminate()
        
    Me.WkSheet_Clear
    
    Me.WkSheet_Sheet.Visible = xlSheetHidden
    
End Sub

Public Property Get WkSheet_AutoRefresh() As Boolean
    Dim ret As Boolean
    ret = autoRefresh_
    WkSheet_AutoRefresh = ret
End Property

Public Property Let WkSheet_AutoRefresh( _
    ByVal b As Boolean)
    autoRefresh_ = b
End Property

Public Sub WkSheet_Clear()
    
    On Error GoTo ERRHANDLER
    
    If UsedRangeEndCell(wks_).row > 1 Then
        Range("A2:" & UsedRangeEndCell(wks_).Address).Delete
    End If
    
    Exit Sub
    
ERRHANDLER:
    AppEx.HandleException "WkSheet_DbTables.Clear"
End Sub

Public Sub WkSheet_Refresh()
    On Error GoTo ERRHANDLER
    
    this.Clear
    
    Dim v As Variant
    For Each v In dataservices_
        Dim tblName As String
        tblName = v
        
        Dim dsvc As DS
        Set dsvc = dataservices_.Item(tblName)
        crsr_.LocateNameData tblName
        
        dsvc.ClearCache
        AppGui.OutputGrid crsr_, dsvc.Load
    Next
    
    Exit Sub
    
ERRHANDLER:
    AppEx.HandleException "WkSheet_DbTables.Refresh"
    
End Sub

Private Sub btnSave_Click()

    On Error GoTo ERRHANDLER
    
    
    this.AutoRefresh = False
    
    Dim actioned As Boolean
    
    Dim v As Variant
    For Each v In dataservices_
    
        Dim tblName As String: tblName = ""
        tblName = v
        
        crsr_.LocateNameData tblName
        
        Dim toSave As GD: Set toSave = Nothing
        Set toSave = AppGui.LoadTable(crsr_)
        
        Dim dsvc As DS: Set dsvc = Nothing
        Set dsvc = dataservices_.Item(tblName)
        dsvc.Save toSave
        dsvc.Delete toDelete(toSave, dsvc.Load)
    Next
    
    If actioned = True Then
        Me.WkSheet_Refresh
    End If
    
    this.AutoRefresh = True
    
    Exit Sub
    
ERRHANDLER:
    AppEx.HandleException "WkSheet_DbTables.btnSave_Click"

End Sub

Private Property Get toDelete( _
    ByRef sheetData As GD, _
    ByRef dbData As GD) As GD
    
    Dim ret As GD
    Set ret = dbData.HeaderRow
    
    Dim sheetDic As New Dictionary
    Dim nullObject As Object
    
    Dim r As Long
    For r = 2 To sheetData.NbrOfRows
        Dim rowStr As String
        rowStr = sheetData.RowAsString(r)
        If sheetDic.Exists(rowStr) = False Then
            sheetDic.Add rowStr, nullObject
        End If
    Next r
    
    For r = 2 To dbData.NbrOfRows
        If sheetDic.Exists(dbData.RowAsString(r)) = False Then
            ret.Append dbData.EntireRow(r)
        End If
    Next r
    
    Set toDelete = ret
    
End Property

Public Sub WkSheet_RunEvent( _
    ByVal subName As String, _
    Optional ByRef triggerComponent As Object = Nothing, _
    Optional ByVal Target As Range = Nothing, _
    Optional ByRef Cancel As Boolean = False)
    
    On Error GoTo ERRHANDLER
    
    Select Case subName
    Case "btnSave_Click"
        btnSave_Click
    Case Else
        Err.Raise ExceptionCode.Argument, _
            "WkSheet_DbTable.RunEvent", _
            "Unknown sub '" & subName & "'"
    End Select
    
    Exit Sub
    
ERRHANDLER:
    AppEx.HandleException "WkSheet_DbTables.RunEvent"
End Sub
