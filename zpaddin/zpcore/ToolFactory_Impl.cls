VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ToolFactory_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements ToolFactory
'

Public Sub Initialize()
End Sub


' Shell

Public Function ToolFactory_CreateShell( _
    executablePath As String, _
    arguments As String) As RSH
    
    Dim tmp As New RSH_Wait
    tmp.Initialize executablePath, arguments
    
    Set ToolFactory_CreateShell = tmp
End Function


' Web Downloader

Public Function ToolFactory_CreateWebDownload() As WDL
    Dim ret As New WDL_Impl
    
    Set ToolFactory_CreateWebDownload = ret
End Function


' ProcesssInterrupt

Public Function ToolFactory_CreateProcessInterrupt( _
    ByVal batchSz As Long, _
    ByVal pauseSeconds As Long) As ProcessInterrupt
    
    Dim ret As New ProcessInterrupt_Impl
    ret.Initialize batchSz, pauseSeconds
    Set ToolFactory_CreateProcessInterrupt = ret
    
End Function

' ProcessKill

Public Function ToolFactory_CreateProcessKiller( _
    ByVal windowCaption As String) As ProcessKill
    Dim ret As New ProcessKill_Impl
    ret.Initialize
    Set ToolFactory_CreateProcessKiller = ret
End Function


' Email

Public Function ToolFactory_CreateEmail( _
    ByRef attachments As GD) As Email
    
    Dim ret As New Email_Outlook
    ret.Initialize attachments
    Set ToolFactory_CreateEmail = ret
End Function


' Zip / Unzip tool

Public Function ToolFactory_CreateZipper( _
    ByVal pathToExe As String) As Zip
    
    Dim ret As New Zip_7za
    ret.Initialize pathToExe
    Set ToolFactory_CreateZipper = ret
End Function






