VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CoreFactory_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements CoreFactory
'

Public Sub Initialize()
End Sub

Public Function CoreFactory_CreateType( _
    Optional ByRef str2DtConv As Str2Dt = Nothing) As VTYP
    Dim ret As New VTYP_Impl
    ret.Initialize str2DtConv
    Set CoreFactory_CreateType = ret
End Function


' UI

Public Function CoreFactory_CreateUI() As UI
    Dim ret As New UI_Impl
    ret.Initialize
    Set CoreFactory_CreateUI = ret
End Function
    
    
' GridData

Public Function CoreFactory_CreateGD( _
    Optional HeaderRow As String = "", _
    Optional delim As String = "|", _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1) As GD
    
    Dim ret As GD
    Set ret = New GD_Impl
    If HeaderRow <> "" Then
        ret.InitializeWithString HeaderRow, delim, False, clms, rows, TristateTrue
    ElseIf clms > 0 And rows > 0 Then
        ret.Initialize clms, rows
    End If
    
    Set CoreFactory_CreateGD = ret
    
End Function

' Data Filter

Public Function CoreFactory_CreateDataFilter( _
    Optional Fields As GD = Nothing) As DF
    Dim ret As New DF_Impl
    ret.Initialize Fields
    Set CoreFactory_CreateDataFilter = ret
End Function

Public Function CoreFactory_CreateArithmeticOp( _
    ByVal op As String) As BinaryOp

    Dim ret As New BinaryOp_Arithmetic
    ret.Initialize op
    Set CoreFactory_CreateArithmeticOp = ret

End Function

Public Function CoreFactory_CreateGridBinaryProcess( _
    ) As GDBinaryProcess
    
    Dim ret As New GDBinaryProcess_Impl
    ret.Initialize
    Set CoreFactory_CreateGridBinaryProcess = ret

End Function

' Log

Public Function CoreFactory_CreateLogger( _
    infoSheet As String, _
    Optional ByRef warningSheet As String = "", _
    Optional ByRef errorSheet As String = "", _
    Optional ByRef debugSheet As String = "", _
    Optional ByRef logFilePath As String = "", _
    Optional showErrorsOnce As Boolean = False, _
    Optional showWarningsOnce As Boolean = False) As LG
    
    Dim ret As LG
    
    Dim infoCsr As CSR
    Set infoCsr = Me.CoreFactory_CreateCursor(infoSheet, "$A$1")
                    
    If logFilePath = "" Then
        Dim impl As New LG_Impl
        impl.Initialize infoCsr
        Set ret = impl
    Else
        Dim warningCsr As CSR
        Dim errorCsr As CSR
        Dim debugCsr As CSR
        
        If warningSheet <> "" Then Set warningCsr = Me.CoreFactory_CreateCursor(warningSheet, "$A$1")
        If errorSheet <> "" Then Set errorCsr = Me.CoreFactory_CreateCursor(errorSheet, "$A$1")
        If debugSheet <> "" Then Set debugCsr = Me.CoreFactory_CreateCursor(debugSheet, "$A$1")
    
        If warningCsr Is Nothing Then Set warningCsr = infoCsr
        If errorCsr Is Nothing Then Set errorCsr = infoCsr
        If debugCsr Is Nothing Then Set debugCsr = infoCsr
        
        Dim plus As New LG_Plus
        plus.Initialize _
            infoCsr, warningCsr, errorCsr, debugCsr, _
            logFilePath, _
            showErrorsOnce, showWarningsOnce
        Set ret = plus
    End If
    
    Set CoreFactory_CreateLogger = ret

End Function



' Cursor

Public Function CoreFactory_CreateCursor( _
    ByVal sheetName As String, _
    Optional ByVal topLeft As String, _
    Optional ByRef wbk As Workbook = Nothing) As CSR
    
    Dim ret As New CSR_Impl
    
    If wbk Is Nothing Then
        ret.Initialize sheetName, topLeft
    Else
        ret.InitializeWithRef _
            wbk.Sheets(sheetName), _
            wbk.Sheets(sheetName).Range(topLeft)
    End If
    
    Set CoreFactory_CreateCursor = ret
    
End Function


' Exception

Public Function CoreFactory_CreateExceptionObject() As EXCPT
    
    Dim ret As EXCPT
    Dim impl As New EXCPT_Impl
    impl.Initialize
    Set ret = impl
    
    Set CoreFactory_CreateExceptionObject = ret
End Function


' Observable implementation


Public Function CoreFactory_CreateObservableImpl( _
    parentId As String, _
    parent As OBSVBL) As OBSVBL
    
    Dim ret As New OBSVBL_Impl
    ret.Initialize parentId, parent
    
    Set CoreFactory_CreateObservableImpl = ret
End Function


Public Function CoreFactory_CreateConfig( _
    Optional ByVal configSheet As String = "Config", _
    Optional ByVal topLeft As String = "T_Config") As CFG
    
    Dim ret As New CFG_XLSheet
    ret.Initialize configSheet, topLeft
    Set CoreFactory_CreateConfig = ret
    
End Function

Public Function CoreFactory_CreateConfigGD( _
    ByRef dat As GD) As CFG
    
    Dim ret As New CFG_Impl
    ret.Initialize dat
    Set CoreFactory_CreateConfigGD = ret
    
End Function


