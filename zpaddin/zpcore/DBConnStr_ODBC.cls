VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DBConnStr_ODBC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements DBConnStr

Private dsnName_ As String
Private userId_ As String
Private password_ As String
'

Public Sub Class_Terminate()
    If Err.Number <> 0 Then
        AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "DBConnStr_ODBC.Class_Terminate"
    End If
End Sub

Public Sub DBConnStr_Initialize(dsnName As String)
    dsnName_ = dsnName
End Sub

Public Property Let userId(s As String)
    userId_ = s
End Property

Public Property Let password(s As String)
    password_ = s
End Property

Public Property Get DBConnStr_ConnectionString() As String

    Dim fso As New FileSystemObject

    Dim connstr As String
    connstr = connstr & "DSN=" & dsnName_ & ";"
    
    If userId_ <> "" Then
        connstr = connstr & " Uid=" & userId_ & ";"
        If password_ <> "" Then
            connstr = connstr & " Pwd=" & password_ & ";"
        End If
    End If
          
    DBConnStr_ConnectionString = connstr

End Property



