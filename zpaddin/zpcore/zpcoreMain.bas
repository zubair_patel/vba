Attribute VB_Name = "zpcoreMain"
Option Explicit

Private Const ZPLIB_NAME = "zpcore"

Public Const BLACK_COLOR = 1
Public Const EIGHTY_GRAY_COLOR = 56
Public Const FIFTY_GRAY_COLOR = 16
Public Const FORTY_GRAY_COLOR = 48
Public Const TWENTYFIVE_GRAY_COLOR = 15
Public Const WHITE_COLOR = 2
Public Const RED_COLOR = 3
Public Const GREEN_COLOR = 4
Public Const ORANGE_COLOR = 45
Public Const BLUE_COLOR = 33
Public Const YELLOW_COLOR = 36
Public Const DARKTEAL_COLOR = 49

Private appvtype_ As VTYP
Private appgui_ As UI
Private applog_ As LG
Private appex_ As EXCPT
Private appcomp_ As COMP

Private dataLoadFactory_ As LoadFactory
Private coreFactory_ As CoreFactory
Private toolsFactory_ As ToolFactory
Private worksheetFactory_ As WkFactory
'

Public Sub ProcessComponents()
    zpcorei.ExportComponents xlbookThis, ZPLIB_NAME
    'zpcorei.RemoveComponents xlbookThis
    'zpcorei.AddZPReferences xlbookThis
    'zpcorei.RemoveZPReferences xlbookThis
End Sub

Public Sub Initialize()
    Set coreFactory_ = createCoreFactory
    Set dataLoadFactory_ = createLoadFactory
    Set toolsFactory_ = createToolsFactory
    Set worksheetFactory_ = createWorksheetFactory
End Sub

Public Sub Terminate()
    On Error Resume Next
    AppLog.CloseLog
    
    Set appvtype_ = Nothing
    Set appgui_ = Nothing
    Set applog_ = Nothing
    Set appex_ = Nothing
    Set appcomp_ = Nothing
    
    Set dataLoadFactory_ = Nothing
    Set coreFactory_ = Nothing
    Set toolsFactory_ = Nothing

End Sub

Private Function createCoreFactory() As CoreFactory
    Dim ret As New CoreFactory_Impl
    ret.Initialize
    Set createCoreFactory = ret
End Function

Private Function createLoadFactory() As LoadFactory
    Dim ret As New LoadFactory_Impl
    ret.Initialize
    Set createLoadFactory = ret
End Function

Private Function createToolsFactory() As ToolFactory
    Dim ret As New ToolFactory_Impl
    ret.Initialize
    Set createToolsFactory = ret
End Function

Private Function createWorksheetFactory() As WkFactory
    Dim ret As New WkFactory_Impl
    ret.Initialize
    Set createWorksheetFactory = ret
End Function

' Global Objects

Public Property Get DataLoadFactory() As LoadFactory
    Set DataLoadFactory = dataLoadFactory_
End Property

Public Property Get CoreFactory() As CoreFactory
    Set CoreFactory = coreFactory_
End Property

Public Property Get ToolsFactory() As ToolFactory
    Set ToolsFactory = toolsFactory_
End Property

Public Property Get WorksheetFactory() As WkFactory
    Set WorksheetFactory = worksheetFactory_
End Property

Public Property Set AppType( _
    ByRef typ As VTYP)
    Set appvtype_ = typ
End Property


Public Property Get AppType() As VTYP
    If appvtype_ Is Nothing Then
        Set appvtype_ = CoreFactory.CreateType
    End If
    Set AppType = appvtype_
End Property

Public Property Set AppGui( _
    ByRef u As UI)
    Set appgui_ = u
End Property

Public Property Get AppGui() As UI
    If appgui_ Is Nothing Then
        Set appgui_ = CoreFactory.CreateUI
    End If
    Set AppGui = appgui_
End Property

Public Property Set AppLog( _
    ByRef alog As LG)
    Set applog_ = alog
End Property

Public Property Get AppLog() As LG
    If applog_ Is Nothing Then
        Set applog_ = CreateDefaultLogger
    End If
    Set AppLog = applog_
End Property

Public Property Set AppEx( _
    ByRef aex As EXCPT)
    Set appex_ = aex
End Property

Public Property Get AppEx() As EXCPT
    If appex_ Is Nothing Then
        Set appex_ = CoreFactory.CreateExceptionObject
    End If
    Set AppEx = appex_
End Property

Public Property Set AppCompOp( _
    ByRef compOp As COMP)
    Set appcomp_ = compOp
End Property

Public Property Get AppCompOp() As COMP
    Dim ret As COMP
    If appcomp_ Is Nothing Then
        Set appcomp_ = CoreFactory.CreateDataFilter
    End If
    Set ret = appcomp_
    Set AppCompOp = ret
End Property


' Public Functions

Public Function Min(ByVal a As Double, ByVal b As Double) As Double
    ' Apparently this is quicker than calling Application.WorksheetFunction.Min(a, b)
    Dim ret As Double

    If a <= b Then
        ret = a
    Else
        ret = b
    End If

    Min = ret
End Function

Public Function Max(ByVal a As Double, ByVal b As Double) As Double
    ' Apparently this is quicker than calling Application.WorksheetFunction.Max(a, b)
    Dim ret As Double

    If a >= b Then
        ret = a
    Else
        ret = b
    End If

    Max = ret
End Function

Public Property Get PreviousDate( _
    ByVal dt As Date) As Date
    
    Dim ret As Date
    Select Case Weekday(dt)
    Case vbMonday
        ret = dt - 3
    Case vbSunday
        ret = dt - 2
    Case Else
        ret = dt - 1
    End Select
    
    ret = DateSerial(Year(ret), Month(ret), Day(ret)) ' remove time portion
    PreviousDate = ret
    
End Property

Public Property Get NextDate( _
    ByVal dt As Date) As Date
    
    Dim ret As Date
    Select Case Weekday(dt)
    Case vbFriday
        ret = dt + 3
    Case vbSaturday
        ret = dt + 2
    Case Else
        ret = dt + 1
    End Select
    
    ret = DateSerial(Year(ret), Month(ret), Day(ret)) ' remove time portion
    NextDate = ret
    
End Property

Public Property Get UsedRangeEndCell( _
    ByRef wk As Worksheet) As Range
    
    Dim ret As Range
    Dim addr As String
    addr = wk.UsedRange.Address
    
    Dim colonLoc As Long
    colonLoc = InStr(1, addr, ":")
    
    If colonLoc > 0 Then
        Set ret = wk.Range(Mid(addr, colonLoc + 1))
    Else
        Set ret = wk.Range(addr)
    End If
    
    Set UsedRangeEndCell = ret
End Property


