VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WkSheet_Simple"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements WkSheet

Private wks_ As Worksheet
Private disp_ As WkDisp
Private clr_ As WkClr

Private autoRefresh_ As Boolean

'

Public Sub Class_Terminate()
    Me.WkSheet_Terminate
End Sub

Public Sub Initialize( _
    ByRef wks As Worksheet, _
    disp As WkDisp, _
    clr As WkClr)
    
    autoRefresh_ = False
    
    Set wks_ = wks
    
    Set disp_ = disp
    Set clr_ = clr
    
    autoRefresh_ = True
        
End Sub

Public Property Get WkSheet_Sheet() As Worksheet
    Dim ret As Worksheet
    Set ret = wks_
    Set WkSheet_Sheet = ret
End Property

Public Sub WkSheet_Terminate()

    On Error Resume Next
    
    Me.WkSheet_Clear
    
    Me.WkSheet_Sheet.Visible = xlSheetHidden
End Sub

Public Property Get WkSheet_AutoRefresh() As Boolean
    Dim ret As Boolean
    ret = autoRefresh_
    WkSheet_AutoRefresh = ret
End Property

Public Property Let WkSheet_AutoRefresh( _
    ByVal b As Boolean)
    autoRefresh_ = b
End Property

Public Sub WkSheet_Clear()
    On Error GoTo ERRHANDLER
    clr_.Clear
    Exit Sub
    
ERRHANDLER:
    AppEx.HandleException "WkSheet_Simple.Clear"
End Sub

Public Sub WkSheet_Refresh()
    On Error GoTo ERRHANDLER
    clr_.Clear
    disp_.Display
    Exit Sub
    
ERRHANDLER:
    AppEx.HandleException "WkSheet_Simple.Refresh"
    
End Sub

Public Sub WkSheet_RunEvent( _
    ByVal subName As String, _
    Optional ByRef triggerComponent As Object = Nothing, _
    Optional ByVal Target As Range = Nothing, _
    Optional ByRef Cancel As Boolean = False)
    
    On Error GoTo ERRHANDLER
    
    Select Case subName
    
    Case Else
        Err.Raise ExceptionCode.Argument, _
            "WkSheet_DbTable.RunEvent", _
            "Unknown sub '" & subName & "'"
    End Select
    
    Exit Sub
    
ERRHANDLER:
    AppEx.HandleException "WkSheet_DbTable.RunEvent"
End Sub
