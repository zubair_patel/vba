VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "UI_Impl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements UI

Private csrSheet_ As String
Private csrRef_ As String
Dim evtOn_ As Boolean
'

Public Sub Initialize()
End Sub

Private Sub markCsr( _
    ByRef crsr As CSR)
    
    csrSheet_ = crsr.Sheet.Name
    csrRef_ = crsr.Ref.Address
End Sub

Private Sub restoreCsr( _
    ByRef crsr As CSR)
    If crsr.Sheet.Name <> csrSheet_ And csrSheet_ <> "" Then
        Set crsr.Sheet = Sheets(csrSheet_)
    End If
    
    If crsr.Ref.Address <> csrRef_ And csrRef_ <> "" Then
        crsr.LocateNameData csrRef_
    End If
    
End Sub

Public Property Get UI_ColumnsInRow( _
    ByRef crsr As CSR) As Long
    If crsr.Ref = "" Then
        UI_ColumnsInRow = 0
        Exit Property
    End If
    
    If crsr.GetRelativeRange(1) = "" Then
        UI_ColumnsInRow = 1
    Else
        UI_ColumnsInRow = crsr.Ref.End(xlToRight).Column - crsr.Ref.Column + 1
    End If
End Property

Public Property Get UI_RowsInColumn( _
    ByRef crsr As CSR) As Long
    If crsr.Ref = "" Then
        UI_RowsInColumn = 0
        Exit Property
    End If
    
    If crsr.GetRelativeRange(0, 1) = "" Then
        UI_RowsInColumn = 1
    Else
        removeOverlappingFilter crsr.Sheet, crsr.Ref
        UI_RowsInColumn = crsr.Ref.End(xlDown).row - crsr.Ref.row + 1
    End If
End Property

Public Property Get UI_BlockRange( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1 _
    ) As Range

    markCsr crsr
    
    If crsr.Ref = "" Then
        Set UI_BlockRange = crsr.Ref
        GoTo EXITPROP
    End If
    
    If clms = -1 Then
        clms = Me.UI_ColumnsInRow(crsr)
    Else
        clms = Application.WorksheetFunction.Min(clms, _
            crsr.Sheet.Columns.count - crsr.Ref.Column + 1)
    End If
    
    If rows = -1 Then
        rows = Me.UI_RowsInColumn(crsr)
    Else
        rows = Application.WorksheetFunction.Min(rows, _
            crsr.Sheet.rows.count - crsr.Ref.row + 1)
    End If
    
    Set UI_BlockRange = Range(crsr.Ref, crsr.GetRelativeRange(clms - 1, rows - 1))
    
EXITPROP:
    restoreCsr crsr

End Property

Public Property Get UI_MaxColumnsInStaggeredBlock( _
    ByRef crsr As CSR) As Long
    markCsr crsr
    
    While crsr.Ref <> ""
        UI_MaxColumnsInStaggeredBlock = Application.WorksheetFunction.Max _
            (UI_MaxColumnsInStaggeredBlock, Me.UI_ColumnsInRow(crsr))
        crsr.Down
    Wend
    
    restoreCsr crsr
End Property

Public Property Get UI_MaxRowsInStaggeredBlock( _
    ByRef crsr As CSR) As Long
    markCsr crsr
    
    While crsr.Ref <> ""
        UI_MaxRowsInStaggeredBlock = Application.WorksheetFunction.Max _
            (UI_MaxRowsInStaggeredBlock, Me.UI_RowsInColumn(crsr))
        crsr.Right
    Wend
    
    restoreCsr crsr
End Property

Public Property Get UI_ColumnOffset( _
    ByRef crsr As CSR, _
    ByVal heading As String) As Long
    markCsr crsr
    
    UI_ColumnOffset = -1
    
    Dim ret As Long: ret = 0
    Dim found As Boolean: found = False
    While crsr.Ref <> "" And found = False
        If crsr.Ref.Value2 = heading Then
            found = True
        Else
            ret = ret + 1
        End If
        crsr.Right
    Wend
    
    If found = True Then
        UI_ColumnOffset = ret
    End If
    
    restoreCsr crsr
End Property

Public Function UI_LoadGrid( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1) As GD
    
    Dim rge As Range
    Set rge = Me.UI_BlockRange(crsr, clms, rows)
    
    Set UI_LoadGrid = CoreFactory.CreateGD
    UI_LoadGrid.InitializeWithRange rge
    
End Function

Public Function UI_LoadTable( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1, _
    Optional ByVal fieldName As String, _
    Optional ByVal op As Operator, _
    Optional ByVal val As Variant) As GD
    
    Dim ret As GD
    Set ret = Me.UI_LoadGrid(crsr, clms, rows)
    ret.HasHeader = TristateTrue
    
    If fieldName <> "" And op <> None And (IsMissing(val) = False) Then
        Dim fltr As DF
        ret.AddFilterCriteria fieldName, op, val, fltr
        Set ret = ret.Filtered(fltr)
    End If
    
    Set UI_LoadTable = ret
    
End Function

Private Sub removeOverlappingFilter( _
    ByRef wk As Worksheet, _
    ByRef selectedRange As Range)
    
    Dim autoFilt As AutoFilter
    Set autoFilt = wk.AutoFilter
    
    If Not (autoFilt Is Nothing) Then
        
        Dim filtRange As Range
        Set filtRange = wk.AutoFilter.Range
    
        If Not (Intersect(selectedRange, filtRange) Is Nothing) Then
            wk.AutoFilterMode = False
        End If
        
    End If
End Sub


Public Sub UI_OutputGrid( _
    ByRef crsr As CSR, _
    ByRef data As GD, _
    Optional ByVal gridStartColumn As Long = 1, _
    Optional ByVal gridStartRow As Long = 1, _
    Optional ByVal gridColumns As Long = -1, _
    Optional ByVal gridRows As Long = -1 _
    )
    
    If gridColumns = -1 Then
        gridColumns = crsr.Sheet.Columns.count - crsr.Ref.Column
    End If
    
    If gridRows = -1 Then
        gridRows = crsr.Sheet.rows.count - crsr.Ref.row
    End If
    
    ' Fix in case sb is stupid enough to pass in zeros or less
    gridStartColumn = Application.WorksheetFunction.Max(1, gridStartColumn)
    gridStartRow = Application.WorksheetFunction.Max(1, gridStartRow)
    
    Dim clmsToOutput As Long
    clmsToOutput = Application.WorksheetFunction.Min( _
        data.NbrOfColumns - gridStartColumn + 1, gridColumns)
    
    Dim rowsToOutput As Long
    rowsToOutput = Application.WorksheetFunction.Min( _
        data.NbrOfRows - gridStartRow + 1, gridRows)
        
    If rowsToOutput < 1 Or clmsToOutput < 1 Then
        Exit Sub
    End If
    
    Dim rge As Range
    Set rge = crsr.Sheet.Range( _
        crsr.Ref, _
        crsr.GetRelativeRange(clmsToOutput - 1, rowsToOutput - 1) _
    )
    
    Dim v() As Variant
    ReDim v(1 To rowsToOutput, 1 To clmsToOutput)
    Dim r As Long
    Dim c As Long
    For r = 1 To rowsToOutput
        For c = 1 To clmsToOutput
            ' Note: We have to transpose the arrays to display to sheet
            v(r, c) = data(c + gridStartColumn - 1, r + gridStartRow - 1)
        Next c
    Next r
    
    rge.Value = v
End Sub

Public Sub UI_Clear( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1 _
    )
    
    Dim rge As Range
    Set rge = Me.UI_BlockRange(crsr, clms, rows)
    rge.ClearContents
End Sub


Public Sub UI_InsertRowsInBlock( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = 1, _
    Optional ByVal rows As Long = -1)
    
    markCsr crsr
    
    crsr.Mark
    If clms = -1 Then
        While crsr.Ref <> ""
            clms = clms + 1
            crsr.Right
        Wend
    End If
    crsr.Restore
    
    Range(crsr.Ref, crsr.GetRelativeRange(clms - 1, rows - 1)) _
        .Insert xlShiftDown
    
    restoreCsr crsr
    
End Sub

Public Sub UI_DeleteRowsFromBlock( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = 1, _
    Optional ByVal rows As Long = -1)
    
    markCsr crsr
    
    rows = Application.WorksheetFunction.Min(rows, _
        crsr.Sheet.rows.count - crsr.Ref.row + 1)
        
    clms = Application.WorksheetFunction.Min(clms, _
        crsr.Sheet.Columns.count - crsr.Ref.Column + 1)
    
    crsr.Mark
    If clms = -1 Then
        clms = 0
        While crsr.Ref <> ""
            clms = clms + 1
            crsr.Right
        Wend
    End If
    crsr.Restore
    
    Range(crsr.Ref, crsr.GetRelativeRange(clms - 1, rows - 1)) _
        .Delete xlShiftUp
    
    restoreCsr crsr
End Sub

Public Sub UI_DeleteBlock( _
    ByRef crsr As CSR)
    markCsr crsr
    
    Dim clms As Long: clms = 0
    Dim rows As Long: rows = 0
    
    crsr.Mark
    While crsr.Ref <> ""
        rows = rows + 1
        crsr.Down
    Wend
    
    If rows = 0 Then GoTo EXITSUB
    
    crsr.Restore
    While crsr.Ref <> ""
        clms = clms + 1
        crsr.Right
    Wend
    
    crsr.Restore
    Me.UI_DeleteRowsFromBlock crsr, rows, clms
    
EXITSUB:
    restoreCsr crsr
    Exit Sub
    
End Sub

Public Sub UI_SetBlockName( _
    ByRef crsr As CSR, _
    ByVal RangeName As String, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1)
End Sub

Public Sub UI_SetDataForAutocomplete( _
    ByRef crsr As CSR, _
    ByRef gdata As GD)
End Sub

Public Function UI_Overlaps( _
    ByRef crsr As CSR, _
    ByRef cellSelected As Range, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1) As Boolean
    
    markCsr crsr
    
    Dim ret As Boolean
    ret = Not (Intersect(cellSelected, AppGui.BlockRange(crsr, clms, rows)) Is Nothing)
    
    UI_Overlaps = ret
    
    restoreCsr crsr
End Function

Public Function UI_BlockAsHtml( _
    ByRef crsr As CSR, _
    Optional ByVal clms As Long = -1, _
    Optional ByVal rows As Long = -1) As String
    
    Dim rge As Range
    Set rge = Me.UI_BlockRange(crsr, clms, rows)
    
    Dim tempFile As String
    tempFile = Environ("TEMP") & "\ab.html"
    
    Dim fso As New Scripting.FileSystemObject
    If fso.FileExists(tempFile) Then
        fso.DeleteFile tempFile, True
    End If
    
    ThisWorkbook.PublishObjects.Add _
        xlSourceRange, _
        tempFile, _
        crsr.Sheet.Name, _
        rge.Address, _
        xlHtmlStatic
    ThisWorkbook.PublishObjects.Publish
    ThisWorkbook.PublishObjects.Delete
    
    Dim ts As TextStream
    Set ts = fso.OpenTextFile(tempFile)
    
    Dim html As String
    html = ts.ReadAll
    
    ts.Close
    Set ts = Nothing
    
    fso.DeleteFile tempFile, True
    Set fso = Nothing
        
    UI_BlockAsHtml = html
    
End Function

Public Property Get UI_ActiveCursor() As CSR
    Dim ret As CSR
    Set ret = CoreFactory.CreateCursor( _
        ActiveSheet.Name, ActiveCell.Address, ActiveWorkbook)
    Set UI_ActiveCursor = ret
End Property

Public Property Get UI_RangeNames( _
    ByRef crsr As CSR) As GD
End Property

Public Property Get UI_TableTopLeft( _
    ByRef crsr As CSR) As String
End Property

Public Property Get UI_FilteredRecords( _
    ByRef crsr As CSR, _
    Optional ByVal topLeftAddr As String = "") As GD
End Property

Public Property Get UI_FilteredRecordsRange( _
    ByRef crsr As CSR, _
    Optional ByVal topLeftAddr As String = "") As Range
End Property

Public Property Get UI_TableColumnData( _
    ByRef crsr As CSR, _
    Optional ByVal topLeftAddr As String = "") As GD
End Property

Public Property Get UI_TableColumnDataRange( _
    ByRef crsr As CSR, _
    Optional ByVal topLeftAddr As String = "") As Range
End Property












