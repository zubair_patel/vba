VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DBConnStr_Access"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements DBConnStr

Private filePath_ As String
'

Public Sub Class_Terminate()
    If Err.Number <> 0 Then
        AppLog.LogD _
        "Abnormal termination - " & Err.Description, _
        "DBConnStr_Access.Class_Terminate"
    End If
End Sub

Public Sub DBConnStr_Initialize(filePath As String)
    filePath_ = filePath
End Sub

Public Property Get DBConnStr_ConnectionString() As String

    Dim fso As New FileSystemObject
    If fso.FileExists(filePath_) = False Then
        Err.Raise ExceptionCode.FileNotFound, _
            "DBConnStr_Access.ConnectionString", _
            "File='" & filePath_ & "'"
    End If
    Set fso = Nothing

    Dim connstr As String
    connstr = connstr & "DRIVER={MicroSoft Access Driver (*.MDB)};"
    connstr = connstr & "DBQ=" & filePath_ & "; "
    
    DBConnStr_ConnectionString = connstr

End Property





